// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuPlayerActor.h"
#include "Components/RegisterableInputComponent.h"


// Sets default values
AMenuPlayerActor::AMenuPlayerActor() {
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create root component
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	RootComponent = CameraComponent;

	// Input configuration 
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	AutoPossessAI = EAutoPossessAI::Disabled;
	AIControllerClass = NULL;
}

// Called to bind functionality to input
void AMenuPlayerActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//check(PlayerInputComponent);
	//RegisterableInputComponent* RegPlayerInputComponent = Cast<URegisterableInputComponent>(PlayerInputComponent);
	//FInputActionBinding& InputActionBinding = RegPlayerInputComponent->RegBindAction(UConstantsProvider::GetInputEventNames()->Action_AnnyPressedKey, IE_Released, this, &AAbstractLoadingActor::OnAnyPressedAction);
}

// Creates an InputComponent that can be used for custom input bindings. Called upon possession by a PlayerController. Return null if you don't want one.
UInputComponent * AMenuPlayerActor::CreatePlayerInputComponent() {
	URegisterableInputComponent* PlayerInputComponent = NewObject<URegisterableInputComponent>(this);
	return PlayerInputComponent;
}

// Called when the game starts or when spawned
void AMenuPlayerActor::BeginPlay() {
	Super::BeginPlay(); 
	// change camera rotation according to actor rotation
	CameraComponent->SetRelativeRotation(GetActorRotation());
}

// Called every frame
void AMenuPlayerActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

