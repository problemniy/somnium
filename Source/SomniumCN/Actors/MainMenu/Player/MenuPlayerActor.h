// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Camera/CameraComponent.h"
#include "MenuPlayerActor.generated.h"

/**
* @descr Player actor class of 'Main menu' map
* @author problemniy
*/
UCLASS(ClassGroup = Player_Actors_)
class SOMNIUMCN_API AMenuPlayerActor : public APawn {

	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMenuPlayerActor();

protected:
	// Creates an InputComponent that can be used for custom input bindings. Called upon possession by a PlayerController. Return null if you don't want one.
	UInputComponent* CreatePlayerInputComponent() override;

	UPROPERTY(EditAnywhere)
		UCameraComponent* CameraComponent;

public:	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
