// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Components/Menu/MenuWidgetComponent.h"
#include "MenuWidgetActor.generated.h"

/**
* @descr Actor class which represents a main menu widget
* @author problemniy
*/
UCLASS(ClassGroup = Static_Actors_)
class SOMNIUMCN_API AMenuWidgetActor : public AActor {

	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMenuWidgetActor();

protected:
	// @todo need assessment 
	/*UPROPERTY(EditAnywhere, Category = "Components", AssetRegistrySearchable, meta = (AssetBundles = ASSET_BUNDLE_MANDATORY))
		TSoftObjectPtr<UWidgetComponent> WidgetComponentPointer;*/

	/** Component which contains a main menu widget */
	UPROPERTY(VisibleDefaultsOnly, BluePrintReadWrite, Category = "Common Configuration")
		UMenuWidgetComponent* WidgetComponent;

public:	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
