// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidgetActor.h"


// Sets default values
AMenuWidgetActor::AMenuWidgetActor() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bCanBeDamaged = false;

	WidgetComponent = CreateDefaultSubobject<UMenuWidgetComponent>(TEXT("MenuWidgetComponent"));
	// Disable physics for widget component
	UPrimitiveComponent* Primitive = Cast<UPrimitiveComponent>(WidgetComponent);
	Primitive->bApplyImpulseOnDamage = false;
	Primitive->bReplicatePhysicsToAutonomousProxy = false;
	Primitive->SetGenerateOverlapEvents(false);

	RootComponent = WidgetComponent;
}

// Called when the game starts or when spawned
void AMenuWidgetActor::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void AMenuWidgetActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

