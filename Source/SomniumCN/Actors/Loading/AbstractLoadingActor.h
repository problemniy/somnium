// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Components/Loading/AssetsLoadingManagerComponent.h"
#include "Components/Loading/StreamLoadingManagerComponent.h"
#include "UMG/Widgets/Abstract/GameLoadingWidget.h"
#include "AbstractLoadingActor.generated.h"

/**
* @descr Abstract actor class for levels loading management  
* @author problemniy
*/
UCLASS(Abstract, ClassGroup = Manager_Actors_)
class SOMNIUMCN_API AAbstractLoadingActor : public AActor {

	GENERATED_BODY()

protected:
	AAbstractLoadingActor();

protected:
	UPROPERTY()
		UGameLoadingWidget* LoadingWidgetObj;

	// Holds loading info to determine actual progress
	struct {
		float ProgressValue;

		int RequestedGroups;

		bool bInProgress, bStreamInProgress;
		bool bAssetsLoaded;
		bool bStreamLoaded;

		void Reset() {
			ProgressValue = 0.f;

			RequestedGroups = 0;

			bInProgress = false;
			bStreamInProgress = false;
			bAssetsLoaded = false;
			bStreamLoaded = false;
		}
	} ActorProgressInfo;

protected:
	/** Component which manages assets loading */
	UPROPERTY(VisibleDefaultsOnly, BluePrintReadOnly, Category = "Loading Components")
		UAssetsLoadingManagerComponent* AssetsLoadingManagerComponent;

	/** Component which manages streamable loading */
	UPROPERTY(VisibleDefaultsOnly, BluePrintReadOnly, Category = "Loading Components")
		UStreamLoadingManagerComponent* StreamLoadingManagerComponent;

	/** Specify loading without a loading widget */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Settings")
		bool bSilentLoad;

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Overridable function called whenever this actor is being removed from a level
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void UpdateWidgetObject(float DeltaTime);

protected:
	/**
	* @descr Overridable function which checks are assets loaded or not
	* @return true if assets loaded otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool CheckAssetsLoaded();

	/**
	* @descr Overridable function which checks are stream levels loaded or not
	* @return true if stream levels loaded otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool CheckStreamLevelsLoaded();

	/**
	* @descr Overridable function which begins to load
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		void Begin();

	/**
	* @descr Overridable function which ends loading
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		void End();

	/**
	* @descr Overridable function which is called when loading has finished
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		void OnLoadingFinished();

	/**
	* @descr Overridable function which checks is loading allowed or not
	* @return true if loading is allowed otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool IsLoadingAllowed();

public:
	/**
	* @descr Returns loading progress
	* @return Loading progress
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		float GetProgress();

	/**
	* @descr Validates preparing to start
	* @return true if loading is ready to start otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool IsReadyToStart();

	/**
	* @descr Checks is loading still in progress or not
	* @return true if loading is in progress otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool IsLoadingInProgress();

	/**
	* @descr Checks is loading has finished or has not
	* @return true if loading has finished otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Actor")
		bool IsLoadingFinished();

};

