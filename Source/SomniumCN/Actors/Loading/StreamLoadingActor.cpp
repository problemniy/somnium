// Fill out your copyright notice in the Description page of Project Settings.

#include "StreamLoadingActor.h"
#include "Utils/GameCommonUtils.h"
#include "Providers/GameInstanceDataProvider.h"

/** Initialize loading streamable levels */
bool AStreamLoadingActor::LoadMap(UWorldLoadingInformation* WorldLoadingInformation) {
	if (!bCheckLoadingData && !IsValid(WorldLoadingInformation)) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::WARNING, TEXT("\n[%s] World loading information is null, please check!"), *GetName());
	}
	else if (IsReadyToStart()) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Loading map:\nCheckLoadingData = %s\nPersistentMap = [%s]\nStreamableLevels = %s"),
			*GetName(),
			bCheckLoadingData ? TEXT("true") : TEXT("false"),
			IsValid(WorldLoadingInformation) ? *WorldLoadingInformation->PersistentMap.GetAssetName() : TEXT("empty"),
			IsValid(WorldLoadingInformation) ? *UGameCommonUtils::ArrayAsString(WorldLoadingInformation->StreamableLevels) : TEXT("empty")
			);

		// check transfer loading data
		bool bTransferLoading = false;
		if (bCheckLoadingData) {
			ULoadingMapData* TransferLoadingData = UGameInstanceDataProvider::GetLoadingMapData(this);
			if (IsValid(TransferLoadingData) && TransferLoadingData->Progress > 0.f) {
				PrepareTransferLoading(TransferLoadingData, WorldLoadingInformation);
				bTransferLoading = true;
			}
		}
		if (!bTransferLoading) {
			if (!IsValid(WorldLoadingInformation)) {
				// -------- logs
				SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::WARNING, TEXT("\n[%s] World loading information is null, please check!"), *GetName());
			} else PrepareSimpleLoading(WorldLoadingInformation);
		}

		Begin();
		return true;
	}
	return !IsLoadingFinished();
}

void AStreamLoadingActor::PrepareTransferLoading(ULoadingMapData* TransferLoadingData, UWorldLoadingInformation* WorldLoadingInformation) {
	FAssetsLoadingManagerComponentInfo AssetsComponentLoadingInfo;
	FStreamLoadingManagerComponentInfo StreamComponentLoadingInfo;

	AssetsComponentLoadingInfo.AssetsList.Append(TransferLoadingData->AssetsList);
	AssetsComponentLoadingInfo.PrimaryAssetsList.Append(TransferLoadingData->PrimaryAssetsList);
	AssetsComponentLoadingInfo.BundleNames.Append(TransferLoadingData->BundleNamesList);
	AssetsComponentLoadingInfo.UnloadPrimaryAssetsList.Append(TransferLoadingData->UnloadPrimaryAssetsList);

	StreamComponentLoadingInfo.StreamableLevelsList.Append(TransferLoadingData->StreamableLevelsList);
	StreamComponentLoadingInfo.UnloadStreamableLevelsList.Append(TransferLoadingData->UnloadStreamableLevelsList);

	AssetsLoadingManagerComponent->AssignLoadingInfo(AssetsComponentLoadingInfo);
	StreamLoadingManagerComponent->AssignLoadingInfo(StreamComponentLoadingInfo);

	TransferProgress = TransferLoadingData->Progress;

	if (IsValid(TransferLoadingData->TransferWidgetObject) && TransferLoadingData->TransferWidgetObject->IsA(UGameLoadingWidget::StaticClass())) {
		LoadingWidgetObj = Cast<UGameLoadingWidget>(TransferLoadingData->TransferWidgetObject);
	} else PrepareLoadingWidget(WorldLoadingInformation);

	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG,
		TEXT("[%s] Prepare transfer loading:\nProgress = %f\nPrepared loading info:\nAssets = %s\nPrimaryAssets = %s\nBundleNames = %s\nPrimaryAssetsToUnload = %s\nStreamableLevels = %s\nStreamableLevelsToInload = %s"),
		*GetName(),
		TransferProgress,
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.AssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.PrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.BundleNames),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.UnloadPrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(StreamComponentLoadingInfo.StreamableLevelsList),
		*UGameCommonUtils::ArrayAsString(StreamComponentLoadingInfo.UnloadStreamableLevelsList)
	);
};

void AStreamLoadingActor::PrepareSimpleLoading(UWorldLoadingInformation* WorldLoadingInformation) {
	TArray<FSoftObjectPath> StreamableAssets;
	TArray<FPrimaryAssetId> PrimaryAssetIds, UnloadPrimaryAssetIds;
	TArray<FName> StreamablePackages, UnloadStreamablePackages;
	StreamableAssets.Add(WorldLoadingInformation->PersistentMap.GetUniqueID());
	PrimaryAssetIds.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*WorldLoadingInformation->PersistentMap.GetUniqueID().GetLongPackageName())));
	for (TAssetPtr<UWorld> StreamableLevel : WorldLoadingInformation->StreamableLevels) {
		StreamableAssets.Add(StreamableLevel.GetUniqueID());
		PrimaryAssetIds.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*StreamableLevel.GetUniqueID().GetLongPackageName())));
		StreamablePackages.Add(FName(*StreamableLevel.GetAssetName()));
	}
	for (TAssetPtr<UWorld> StreamableLevel : WorldLoadingInformation->UnloadStreamableLevels) {
		UnloadStreamablePackages.Add(FName(*StreamableLevel.GetAssetName()));
		UnloadPrimaryAssetIds.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*StreamableLevel.GetUniqueID().GetLongPackageName())));
	}

	FAssetsLoadingManagerComponentInfo AssetsComponentLoadingInfo;
	AssetsComponentLoadingInfo.AssetsList.Append(StreamableAssets);
	AssetsComponentLoadingInfo.PrimaryAssetsList.Append(PrimaryAssetIds);
	AssetsComponentLoadingInfo.BundleNames.Append(WorldLoadingInformation->AssetBundleNames);
	AssetsComponentLoadingInfo.UnloadPrimaryAssetsList.Append(UnloadPrimaryAssetIds);
	FStreamLoadingManagerComponentInfo StreamComponentLoadingInfo;
	StreamComponentLoadingInfo.StreamableLevelsList.Append(StreamablePackages);
	StreamComponentLoadingInfo.UnloadStreamableLevelsList.Append(UnloadStreamablePackages);

	AssetsLoadingManagerComponent->AssignLoadingInfo(AssetsComponentLoadingInfo);
	StreamLoadingManagerComponent->AssignLoadingInfo(StreamComponentLoadingInfo);

	PrepareLoadingWidget(WorldLoadingInformation);
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG,
		TEXT("[%s] Prepare simple loading:\nWorld loading information:\nPersistentMap = [%s]\nStreamableLevels = %s\nBundleNames = %s\nStreamableLevelsTounload = %s\nPrepared loading info:\nAssets = %s\nPrimaryAssets = %s\nBundleNames = %s\nPrimaryAssetsToUnload = %s\nStreamableLevels = %s\nStreamableLevelsToInload = %s"),
		*GetName(),
		*WorldLoadingInformation->PersistentMap.GetAssetName(),
		*UGameCommonUtils::ArrayAsString(WorldLoadingInformation->StreamableLevels),
		*UGameCommonUtils::ArrayAsString(WorldLoadingInformation->AssetBundleNames),
		*UGameCommonUtils::ArrayAsString(WorldLoadingInformation->UnloadStreamableLevels),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.AssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.PrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.BundleNames),
		*UGameCommonUtils::ArrayAsString(AssetsComponentLoadingInfo.UnloadPrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(StreamComponentLoadingInfo.StreamableLevelsList),
		*UGameCommonUtils::ArrayAsString(StreamComponentLoadingInfo.UnloadStreamableLevelsList)
		);
}

void AStreamLoadingActor::PrepareLoadingWidget(UWorldLoadingInformation* WorldLoadingInformation) {
	if(IsValid(WorldLoadingInformation) && IsValid(WorldLoadingInformation->LoadingWidgetClass))
	LoadingWidgetObj = CreateWidget<UGameLoadingWidget>(GetWorld()->GetFirstPlayerController(), WorldLoadingInformation->LoadingWidgetClass.Get());;
}

/** Overridable function which ends loading */
void AStreamLoadingActor::End_Implementation() {
	Super::End_Implementation();
	TransferProgress = 0.f;
}

/** Returns loading progress */
float AStreamLoadingActor::GetProgress_Implementation() {
	return (1.f - TransferProgress) * Super::GetProgress_Implementation() + TransferProgress;
}

