// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Loading/StreamLoadingActor.h"
#include "MapLoadingActor.generated.h"

/**
* @base AStreamLoadingActor
* @descr Actor class which loads and changes maps
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API AMapLoadingActor : public AStreamLoadingActor {

	GENERATED_BODY()

private:
	TAssetPtr<UWorld> PersistentMap;
	bool bPersistentMapReady;
	float PercentIteration, PostProgress;

private:
	void PrepareLoading(UWorldLoadingInformation* WorldLoadingInformation);
	void PrepareLoadingWidget(UWorldLoadingInformation* WorldLoadingInformation);
	void PrepareTransferLoadingData(UWorldLoadingInformation* WorldLoadingInformation);
	void PreparePostProgress(UWorldLoadingInformation* WorldLoadingInformation);
	void LoadPersistentMap();
	void OnPersistentMapLoaded(const FName& PackageName, UPackage* InLevelPackage, EAsyncLoadingResult::Type Result);
	void DoChangeMap();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	/** Overridable function which is called when loading has finished */
	virtual void OnLoadingFinished_Implementation() override;

public:
	/** Returns loading progress */
	virtual float GetProgress_Implementation() override;

public:
	/**
	* @descr Initialize loading and automatically change a map
	* @param WorldLoadingInformation - World loading information of map
	* @return true if loading is active otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Loading Management")
		bool ChangeMap(UPARAM(DisplayName = "Map Info") UWorldLoadingInformation* WorldLoadingInformation);
	
};

