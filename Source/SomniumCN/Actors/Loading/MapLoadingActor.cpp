// Fill out your copyright notice in the Description page of Project Settings.

#include "MapLoadingActor.h"
#include "Providers/GameInstanceDataProvider.h"
#include "Utils/GameCommonUtils.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

// Called every frame
void AMapLoadingActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bPersistentMapReady) {
		DoChangeMap();
	}
}

/** 
* Initialize loading and automatically change a map
*/
bool AMapLoadingActor::ChangeMap(UWorldLoadingInformation* WorldLoadingInformation) {
	if (!IsValid(WorldLoadingInformation)) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::WARNING, TEXT("\n[%s] World loading information is null, please check!"), *GetName());
	}
	else if (IsReadyToStart()) {
		PrepareLoading(WorldLoadingInformation);
		Begin();
	}
	return !IsLoadingFinished();
}

void AMapLoadingActor::PrepareLoading(UWorldLoadingInformation* WorldLoadingInformation) {
	FAssetsLoadingManagerComponentInfo AssetsComponentLoadingInfo;
	PersistentMap = WorldLoadingInformation->PersistentMap;
	AssetsComponentLoadingInfo.AssetsList.Add(WorldLoadingInformation->PersistentMap.GetUniqueID());
	AssetsComponentLoadingInfo.PrimaryAssetsList.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*PersistentMap.GetUniqueID().GetLongPackageName())));
	AssetsComponentLoadingInfo.BundleNames.Append(WorldLoadingInformation->AssetBundleNames);
	AssetsLoadingManagerComponent->AssignLoadingInfo(AssetsComponentLoadingInfo);
	// prepare post progress
	PreparePostProgress(WorldLoadingInformation);
	// prepare widget
	PrepareLoadingWidget(WorldLoadingInformation);
	// prepare transfer loading data
	PrepareTransferLoadingData(WorldLoadingInformation);
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Changing map:\nPersistentMap = [%s], AssetBundles = %s"),
		*GetName(),
		*WorldLoadingInformation->PersistentMap.GetAssetName(),
		*UGameCommonUtils::ArrayAsString(WorldLoadingInformation->AssetBundleNames)
	);
}

void AMapLoadingActor::PreparePostProgress(UWorldLoadingInformation* WorldLoadingInformation) {
	int32 CommonAmount = WorldLoadingInformation->StreamableLevels.Num() * 3 + 3 + (WorldLoadingInformation->UnloadPrimaryAssetsList.Num() > 0) + (WorldLoadingInformation->UnloadStreamableLevels.Num() > 0);
	PercentIteration = 1.f / (float)CommonAmount;
	PostProgress = 3.f / (float)CommonAmount;
}

void AMapLoadingActor::PrepareLoadingWidget(UWorldLoadingInformation* WorldLoadingInformation) {
	if (IsValid(WorldLoadingInformation) && IsValid(WorldLoadingInformation->LoadingWidgetClass))
		LoadingWidgetObj = CreateWidget<UGameLoadingWidget>(GetWorld()->GetFirstPlayerController(), WorldLoadingInformation->LoadingWidgetClass.Get());
}

void AMapLoadingActor::PrepareTransferLoadingData(UWorldLoadingInformation* WorldLoadingInformation) {
	ULoadingMapData* LoadingData = UGameInstanceDataProvider::GetLoadingMapData(this);
	if (!IsValid(LoadingData)) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::WARNING, TEXT("\n[%s] Loading data is empty, please check!"), *GetName());
		return;
	}
	for (TAssetPtr<UWorld> StreamableLevel : WorldLoadingInformation->StreamableLevels) {
		LoadingData->AssetsList.Add(StreamableLevel.GetUniqueID());
		LoadingData->PrimaryAssetsList.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*StreamableLevel.GetUniqueID().GetLongPackageName())));
		LoadingData->StreamableLevelsList.Add(FName(*StreamableLevel.GetAssetName()));
		LoadingData->BundleNamesList.Append(WorldLoadingInformation->AssetBundleNames);
	}
	for (TAssetPtr<UWorld> StreamableLevel : WorldLoadingInformation->UnloadStreamableLevels) {
		LoadingData->UnloadPrimaryAssetsList.Add(FPrimaryAssetId(UGameAssetManager::MapType, FName(*StreamableLevel.GetUniqueID().GetLongPackageName())));
		LoadingData->UnloadStreamableLevelsList.Add(FName(*StreamableLevel.GetAssetName()));
	}
	LoadingData->TransferWidgetObject = LoadingWidgetObj;
	LoadingData->Progress = PostProgress;
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG,
		TEXT("[%s] Prepared transfer loading data:\nPostProgress = %f\nAssets = %s\nPrimaryAssets = %s\nStreamableLevels = %s\nBundleNames = %s\nPrimaryAssetsToUnload = %s\nStreamableLevelsTounload = %s"),
		*GetName(),
		LoadingData->Progress,
		*UGameCommonUtils::ArrayAsString(LoadingData->AssetsList),
		*UGameCommonUtils::ArrayAsString(LoadingData->PrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(LoadingData->StreamableLevelsList),
		*UGameCommonUtils::ArrayAsString(LoadingData->BundleNamesList),
		*UGameCommonUtils::ArrayAsString(LoadingData->UnloadPrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(LoadingData->UnloadStreamableLevelsList)
		);
}

/** Overridable function which is called when loading has finished */
void AMapLoadingActor::OnLoadingFinished_Implementation() {
	Super::OnLoadingFinished_Implementation();
	LoadPersistentMap();
}

void AMapLoadingActor::LoadPersistentMap() {
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("\n[%s] Loading persistent map:\nPackage = [%s]"),
		*GetName(), *PersistentMap.GetLongPackageName());

	LoadPackageAsync(PersistentMap.GetLongPackageName(), FLoadPackageAsyncDelegate::CreateUObject(this, &AMapLoadingActor::OnPersistentMapLoaded));
}

void AMapLoadingActor::OnPersistentMapLoaded(const FName& PackageName, UPackage* InLevelPackage, EAsyncLoadingResult::Type Result) {
	if (EAsyncLoadingResult::Type::Succeeded) {
		bPersistentMapReady = true;
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Persistent map has loaded!"), *GetName(), *PackageName.ToString());
	}
}

void AMapLoadingActor::DoChangeMap() {
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Map is going to change to [%s]"), *GetName(), *PersistentMap.GetLongPackageName());
	
	//End();
	UGameplayStatics::OpenLevel(GetWorld(), FName(*PersistentMap.GetLongPackageName()));
}

/** Returns loading progress */
float AMapLoadingActor::GetProgress_Implementation() {
	return bPersistentMapReady ? PostProgress : (PostProgress - PercentIteration) * Super::GetProgress_Implementation();
}

