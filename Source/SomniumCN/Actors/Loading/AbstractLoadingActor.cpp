// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractLoadingActor.h"
#include "Components/RegisterableInputComponent.h"

AAbstractLoadingActor::AAbstractLoadingActor() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	bHidden = true;

	AssetsLoadingManagerComponent = CreateDefaultSubobject<UAssetsLoadingManagerComponent>(TEXT("AssetsLoadingManagerComponent"));
	StreamLoadingManagerComponent = CreateDefaultSubobject<UStreamLoadingManagerComponent>(TEXT("StreamLoadingManagerComponent"));
}

// Called when the game starts or when spawned
void AAbstractLoadingActor::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AAbstractLoadingActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	UpdateWidgetObject(DeltaTime);
	if (!IsLoadingAllowed()) {
		if (ActorProgressInfo.bInProgress) {
			OnLoadingFinished();
			ActorProgressInfo.bInProgress = false;
		}
		// deactivate actor when widget is ready to close or silent load is used
		if (bSilentLoad || !IsValid(LoadingWidgetObj) || IsValid(LoadingWidgetObj) && LoadingWidgetObj->IsReady()) {
			End();
		}
		return;
	}
	ActorProgressInfo.bInProgress = true;
	ActorProgressInfo.ProgressValue = GetProgress();

	// start stream loading
	ActorProgressInfo.bAssetsLoaded = CheckAssetsLoaded();
	if (ActorProgressInfo.bAssetsLoaded && !ActorProgressInfo.bStreamInProgress) {
		StreamLoadingManagerComponent->Load();
		ActorProgressInfo.bStreamInProgress = true;
	}
	if (ActorProgressInfo.bStreamInProgress) {
		ActorProgressInfo.bStreamLoaded = CheckStreamLevelsLoaded();
	}

	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] Loading actor info: AssetsLoaded = %s, StreamLoaded = %s, Progress = %f"),
		*GetName(),
		ActorProgressInfo.bAssetsLoaded ? TEXT("true") : TEXT("false"),
		ActorProgressInfo.bStreamLoaded ? TEXT("true") : TEXT("false"),
		ActorProgressInfo.ProgressValue
	);
}

void AAbstractLoadingActor::UpdateWidgetObject(float DeltaTime) {
	if (IsValid(LoadingWidgetObj)) {
		LoadingWidgetObj->SetLoadingProgress(GetProgress());
		LoadingWidgetObj->OnExternalTick(DeltaTime);
	}
}

// Overridable function called whenever this actor is being removed from a level 
void AAbstractLoadingActor::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);
	End();
}

/** Overridable function which begins to load */
void AAbstractLoadingActor::Begin_Implementation() {
	// start assets loading
	bool bGood = AssetsLoadingManagerComponent->Load();
	if (bGood) {
		ActorProgressInfo.RequestedGroups = AssetsLoadingManagerComponent->CheckHasObjectsToLoad() + StreamLoadingManagerComponent->CheckHasObjectsToLoad();
		if (!bSilentLoad && IsValid(LoadingWidgetObj)) {
			LoadingWidgetObj->SetLoadingProgress(GetProgress());
			if (!LoadingWidgetObj->IsInViewport()) {
				LoadingWidgetObj->AddToViewport();
			}
		}
		SetActorTickEnabled(true);
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Actor begins to load assets, bSilentLoad = %s, LoadingWidget = %s"), 
			*GetName(), bSilentLoad ? TEXT("true") : TEXT("false"), IsValid(LoadingWidgetObj) ? *LoadingWidgetObj->GetName() : TEXT("null"));
	}
	else {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::WARNING, TEXT("[%s] Previous loading is still active, please wait!"), *GetName());
	}
}

/** Overridable function which ends loading */
void AAbstractLoadingActor::End_Implementation() {
	ActorProgressInfo.Reset();
	if (IsValid(LoadingWidgetObj) && LoadingWidgetObj->IsInViewport()) {
		LoadingWidgetObj->RemoveFromParent();
	}
	SetActorTickEnabled(false);
}

/** Overridable function which checks is loading allowed or not */
bool AAbstractLoadingActor::IsLoadingAllowed_Implementation() {
	return !IsLoadingFinished();
}

/** Validates preparing to start */
bool AAbstractLoadingActor::IsReadyToStart_Implementation() {
	return IsLoadingAllowed() && !IsLoadingInProgress();
}

/** Checks is loading still in progress or not */
bool AAbstractLoadingActor::IsLoadingInProgress_Implementation() {
	return ActorProgressInfo.bInProgress;
}

/** Checks is loading has finished or has not */
bool AAbstractLoadingActor::IsLoadingFinished_Implementation() {
	return ActorProgressInfo.bAssetsLoaded && ActorProgressInfo.bStreamLoaded;
}

/** Overridable function which checks are assets loaded or not */
bool AAbstractLoadingActor::CheckAssetsLoaded_Implementation() {
	return AssetsLoadingManagerComponent->CheckIsLoaded() || !AssetsLoadingManagerComponent->CheckHasObjectsToLoad();
}

/** Overridable function which checks are stream levels loaded or not */
bool AAbstractLoadingActor::CheckStreamLevelsLoaded_Implementation() {
	return StreamLoadingManagerComponent->CheckIsLoaded() || !StreamLoadingManagerComponent->CheckHasObjectsToLoad();
}

/** Overridable function which returns loading progress */
float AAbstractLoadingActor::GetProgress_Implementation() {
	float AssetsPercent = ActorProgressInfo.RequestedGroups > 0.f ? AssetsLoadingManagerComponent->GetProgress() / (float)ActorProgressInfo.RequestedGroups : 0.f;
	float StreamPercent = ActorProgressInfo.RequestedGroups > 0.f ? StreamLoadingManagerComponent->GetProgress() / (float)ActorProgressInfo.RequestedGroups : 0.f;
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] AssetsComponentProgress = %f, StreamComponentProgress = %f, ComponentsProgress = %f"),
		*GetName(), AssetsLoadingManagerComponent->GetProgress(), StreamLoadingManagerComponent->GetProgress(), AssetsPercent + StreamPercent);

	return AssetsPercent + StreamPercent;
}

/** Overridable function which is called when loading has finished */
void AAbstractLoadingActor::OnLoadingFinished_Implementation() {
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] Actor has finished loading!"), *GetName());
}

