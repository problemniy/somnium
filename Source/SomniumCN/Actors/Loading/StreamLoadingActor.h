// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Loading/AbstractLoadingActor.h"
#include "Data/DataAssets/Primary/WorldLoadingInformation.h"
#include "Data/GameInstanceData/LoadingMapData.h"
#include "StreamLoadingActor.generated.h"

/**
* @base AAbstractLoadingActor
* @descr Actor class which loads streamable levels
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API AStreamLoadingActor : public AAbstractLoadingActor {

	GENERATED_BODY()

private:
	float TransferProgress;

private:
	void PrepareSimpleLoading(UWorldLoadingInformation* WorldLoadingInformation);
	void PrepareTransferLoading(ULoadingMapData* TransferLoadingData, UWorldLoadingInformation* WorldLoadingInformation);
	void PrepareLoadingWidget(UWorldLoadingInformation* WorldLoadingInformation);

protected:
	/** Allow to check loading data which will be taken into account while loading is in progress */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Settings")
		bool bCheckLoadingData;

protected:
	/** Overridable function which ends loading */
	virtual void End_Implementation() override;

public:
	/** Returns loading progress */
	virtual float GetProgress_Implementation() override;

public:
	/**
	* @descr Initialize loading streamable levels
	* @param WorldLoadingInformation - World loading information
	* @return true if loading is active otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Loading Management")
		bool LoadMap(UPARAM(DisplayName = "Map Info") UWorldLoadingInformation* WorldLoadingInformation);

};

