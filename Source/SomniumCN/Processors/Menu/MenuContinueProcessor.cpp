// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuContinueProcessor.h"
#include "Providers/ServicesProvider.h"
#include "Utils/GameCommonUtils.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UMenuContinueProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	
	// check for main menu widget
	if (IsMainMenuWidgetToOpen(WidgetToClose, WidgetToOpen, ButtonTag)) {
		UGameButton* ContinueButton = nullptr;
		for (UGameButton* Button : WidgetToOpen->GameButtons) {
			if (EUIElementTag::CONTINUE == Button->GetUITag()) {
				ContinueButton = Button;
				break;
			}
		}
		// prepare logic for continue button
		if (ContinueButton) {
			LastSavedSlot = GetActualGameSlot();
			ContinueButton->SetVisibility(LastSavedSlot ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
		}
	}
	
}


// Checks for main menu widget to open
bool UMenuContinueProcessor::IsMainMenuWidgetToOpen(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	return !WidgetToClose && WidgetToOpen && EUIElementTag::EMPTY == ButtonTag;
}


// Returns last saved game slot if it's available
UGameSlotData* UMenuContinueProcessor::GetActualGameSlot() {
	UGameSlotData* ActualGameSlot = nullptr;
	USaveGameService* SaveGameService = UServicesProvider::GetSaveGameService();
	TArray<UGameSlotData*> SaveSlots;
	UGameCommonUtils::AddNotNull(SaveSlots, SaveGameService->ReceiveGameSlotData(EGameSlotIdentity::SLOT_1, false));
	UGameCommonUtils::AddNotNull(SaveSlots, SaveGameService->ReceiveGameSlotData(EGameSlotIdentity::SLOT_2, false));
	UGameCommonUtils::AddNotNull(SaveSlots, SaveGameService->ReceiveGameSlotData(EGameSlotIdentity::SLOT_3, false));	
	if (SaveSlots.Num() > 0) {
		// sort to get last save
		SaveSlots.Sort([&](const UGameSlotData& A, const UGameSlotData& B) {
			return B.SaveDate > A.SaveDate;
		});
		ActualGameSlot = SaveSlots.Last();
	}
	return ActualGameSlot;
}


