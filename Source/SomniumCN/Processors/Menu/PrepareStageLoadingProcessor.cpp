// Fill out your copyright notice in the Description page of Project Settings.


#include "PrepareStageLoadingProcessor.h"

/** Overridable function which handles clicked ui elements in menu */
void UPrepareStageLoadingProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);
	if (!ClickedElement->IsA(UGameButton::StaticClass())) {
		return;
	}



	if (EUIElementTag::OVERLAP_IMG == IUISpecificElement::Execute_GetUITag(Cast<UGameButton>(ClickedElement))) {

	}

}

