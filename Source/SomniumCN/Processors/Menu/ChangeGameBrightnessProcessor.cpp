// Fill out your copyright notice in the Description page of Project Settings.

#include "ChangeGameBrightnessProcessor.h"
#include "Providers/ConstantsProvider.h"
#include "Providers/ServicesProvider.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UChangeGameBrightnessProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);

	// set game brightness at start game menu
	if (ActualBrightness.IsZero()) { // check for empty brightness
		UGameSettingsData* SavedGameData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, false));
		if (IsValid(SavedGameData)) {
			ChangeCameraBrightness(UServicesProvider::GetWorldManagementService()->GetActiveCameraComponent(UGameplayStatics::GetPlayerPawn(WidgetToOpen->GetWorld(), 0)), SavedGameData->Brightness);
		}
		else {
			UGameDefaultConfiguration* GameDefaultConfiguration = UConstantsProvider::GetGameDefaultConfiguration();
			// set default brightness when saved settings data is empty
			ChangeCameraBrightness(UServicesProvider::GetWorldManagementService()->GetActiveCameraComponent(UGameplayStatics::GetPlayerPawn(WidgetToOpen->GetWorld(), 0)), GameDefaultConfiguration->DefaultBrightness);
		}
	}
	// set new game brightness when player has specified it
	else if (IsValid(WidgetToClose) && EUIElementTag::CHECK_BRIGHTNESS == IUISpecificElement::Execute_GetUITag(WidgetToClose)) {
		UCameraComponent* ActiveCameraComponent = UServicesProvider::GetWorldManagementService()->GetActiveCameraComponent(UGameplayStatics::GetPlayerPawn(WidgetToOpen->GetWorld(), 0));
		if (IsValid(ActiveCameraComponent)) {
			FVector2D CameraBrightness = FVector2D(ActiveCameraComponent->PostProcessSettings.AutoExposureMinBrightness, ActiveCameraComponent->PostProcessSettings.AutoExposureMaxBrightness);
			if (!ActualBrightness.Equals(CameraBrightness)) {
				SaveCameraBrightness(CameraBrightness.Y);
			}
		}
	}

}

void UChangeGameBrightnessProcessor::ChangeCameraBrightness(UCameraComponent* ActiveCameraComponent, const float NewBrightness) {
	if (IsValid(ActiveCameraComponent)) {
		// save new post process settings on processor
		ActualBrightness.Set(NewBrightness, NewBrightness);
		// change brightness
		UGameSettingsService* SettingsService = UServicesProvider::GetGameSettingsService();
		SettingsService->ChangeCameraBrightness(ActiveCameraComponent, ActualBrightness);
	}
}

void UChangeGameBrightnessProcessor::SaveCameraBrightness(const float NewBrightness) {
	UGameSettingsData* SavedGameData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, true));
	if (IsValid(SavedGameData)) {
		SavedGameData->Brightness = NewBrightness;
		UServicesProvider::GetSaveGameService()->SaveGameData(SavedGameData);
	}
}

