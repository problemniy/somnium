// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "PrepareStageLoadingProcessor.generated.h"

/**
* @descr Class which prepares stage loading from game menu
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UPrepareStageLoadingProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	bool SearchStageToLoad();

protected:
	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;
	
};

