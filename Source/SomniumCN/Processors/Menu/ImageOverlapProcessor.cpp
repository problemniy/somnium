// Fill out your copyright notice in the Description page of Project Settings.

#include "ImageOverlapProcessor.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UImageOverlapProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);

	// restore all images to original condition
	for (UGameImage* Image : TargetImages) {
		Image->ResetOpacityAnimation();
		if (HiddenImages.Contains(Image)) {
			Image->SetVisibility(ESlateVisibility::Hidden);
		}
	}
	TargetImages.Empty();
	HiddenImages.Empty();
}

/** Overridable function which handles clicked ui elements in menu */
void UImageOverlapProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);

	if (!ClickedElement->IsA(UGameButton::StaticClass()) || EUIElementTag::GO != IUISpecificElement::Execute_GetUITag(Cast<UGameButton>(ClickedElement)) || TargetImages.Num() > 0) {
		return;
	}

	for (UGameImage* Image : CurrentWidget->GameImages) {
		if (EUIElementTag::OVERLAP_IMG == IUISpecificElement::Execute_GetUITag(Image)) {
			TargetImages.Add(Image);
		}
	}
}

/** Overridable function which handles menu widget component's tick in menu */
void UImageOverlapProcessor::MenuTick_Implementation(float DeltaTime) {
	Super::MenuTick_Implementation(DeltaTime);

	if (TargetImages.Num() > 0) {
		for (UGameImage* Image : TargetImages) {
			if (ESlateVisibility::Hidden == Image->GetVisibility()) {
				HiddenImages.Add(Image);
				Image->SetVisibility(ESlateVisibility::Visible);
			}
			Image->MakeOpacityAnimation(DeltaTime);
		}
	}
}

