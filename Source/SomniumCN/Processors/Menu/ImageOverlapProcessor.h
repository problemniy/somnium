// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "ImageOverlapProcessor.generated.h"

/**
* @descr Class which controls images to overlap widget
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UImageOverlapProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	UPROPERTY()
		TArray<UGameImage*> HiddenImages;

	UPROPERTY()
		TArray<UGameImage*> TargetImages;
	
protected:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);

	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;

	/** Overridable function which handles menu widget component's tick in menu */
	virtual void MenuTick_Implementation(float DeltaTime) override;

};
