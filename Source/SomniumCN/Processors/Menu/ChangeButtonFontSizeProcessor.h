// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "ChangeButtonFontSizeProcessor.generated.h"

/**
* @descr Class which changes text font size in game menu buttons
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UChangeButtonFontSizeProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()
	
private:
	TMap<UGameButton*, int32> ButtonToPrimaryFontSizeMap;

private:
	UGameText* GetGameText(UGameButton* GameButton);
	void RestoreFontSize(UGameButton* PointedButton);
	
protected:
	/** Specify new font size for pointed buttons */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration", meta = (ClampMin = 1))
		int32 ButtonPointedSize = 50;

protected:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;

	/** Overridable function which handles pointed ui elements in menu */
	virtual void ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) override;
	
};
