// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "Camera/CameraComponent.h"
#include "ChangeGameBrightnessProcessor.generated.h"

/**
* @descr Class which changes game brightness
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UChangeGameBrightnessProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	FVector2D ActualBrightness;

private:
	void ChangeCameraBrightness(UCameraComponent* ActiveCameraComponent, const float NewBrightness);
	void SaveCameraBrightness(const float NewBrightness);

public:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};

