// Fill out your copyright notice in the Description page of Project Settings.

#include "ShowHideMenuCharacterProcessor.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UShowHideMenuCharacterProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);

	// check character after first menu opening
	if (!IsValid(WidgetToClose)) {
		CheckCharacterInMenu(WidgetToOpen);
	}
	// apply show/hide logic
	if (EUIElementTag::CONFIRM_START == IUISpecificElement::Execute_GetUITag(WidgetToOpen) && IsValid(CharacterActor)) {
		CharacterActor->SetActorHiddenInGame(true);
	}
	else if (IsValid(WidgetToClose) && EUIElementTag::CONFIRM_START == IUISpecificElement::Execute_GetUITag(WidgetToClose) && IsValid(CharacterActor)) {
		CharacterActor->SetActorHiddenInGame(false);
	}

}

// Search character actor in menu
void UShowHideMenuCharacterProcessor::CheckCharacterInMenu(UObject* ContextObject) {
	if (!IsValid(CharacterActor)) {
		TArray<AActor*> FindActors;
		UGameplayStatics::GetAllActorsWithTag(ContextObject, CharacterActorTag, FindActors);
		if (FindActors.Num() > 0) {
			CharacterActor = FindActors.Last();
		}
	}
}

