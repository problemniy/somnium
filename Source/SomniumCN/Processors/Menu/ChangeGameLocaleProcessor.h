// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "ChangeGameLocaleProcessor.generated.h"

/**
* @descr Structure which associates ui element tag with locale culture name
* @author problemniy
*/
USTRUCT(NoExport)
struct SOMNIUMCN_API FChangeLocaleProcessorParser {

public:
	/** Specify value (locale culture name) for ui element tag */
	UPROPERTY(EditInstanceOnly)
		FString LocaleCultureName;

	/** Specify ui element tag which should be associated with locale culture name */
	UPROPERTY(EditInstanceOnly)
		EUIElementTag UIElementTag;
};

/**
* @descr Class which changes game locale
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UChangeGameLocaleProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

	UChangeGameLocaleProcessor();

private:
	EUIElementTag ActualLanguage;

private:
	FString ParseLocale(const EUIElementTag& UIElementTag);

	// Checks locale culture name and fills actual language by associated ui lement tag
	void DetermineActualLanguage();

protected:
	/** Specify processor locale parsers */
	UPROPERTY(EditAnywhere, Category = "Configuration")
		TArray<FChangeLocaleProcessorParser> LocaleParsers;

public:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};
