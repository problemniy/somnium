// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractMenuProcessor.h"

/** Overridable function which is invoked before widgets switching to verify resolution */
bool UAbstractMenuProcessor::ValidateWidgetChange_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	// unimplemented();
	return true;
}

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UAbstractMenuProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	// unimplemented();
}

/** Overridable function which handles pointed ui elements in menu */
void UAbstractMenuProcessor::ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) {
	// unimplemented();
}

/** Overridable function which handles clicked ui elements in menu */
void UAbstractMenuProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	// unimplemented();
}

/** Overridable function which handles menu widget component's tick in menu */
void UAbstractMenuProcessor::MenuTick_Implementation(float DeltaTime) {
	// unimplemented();
}

