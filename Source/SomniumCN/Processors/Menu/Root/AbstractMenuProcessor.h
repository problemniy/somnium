// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Processors/AbstractProcessor.h"
#include "UMG/Widgets/GameMenuWidget.h"
#include "Misc/Enums/UserInterfaceEnums.h"
#include "AbstractMenuProcessor.generated.h"

/**
* @descr Abstract class which specifies Menu Processor
* @author problemniy
*/
UCLASS(Abstract)
class SOMNIUMCN_API UAbstractMenuProcessor : public UAbstractProcessor {

	GENERATED_BODY()

public:
	/**
	* @descr Overridable function which is invoked before widgets switching to verify resolution
	* @param WidgetToClose - Widget which will be closed in menu (can be nullptr)
	* @param WidgetToOpen - Widget which will be opened in menu
	* @param ButtonTag - Specific tag of clicked button
	* @return If false then the change will be prevented
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Menu Processors")
		bool ValidateWidgetChange(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);

	/**
	* @descr Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	* @param WidgetToClose - Widget which will be closed in menu (can be nullptr)
	* @param WidgetToOpen - Widget which will be opened in menu
	* @param ButtonTag - Specific tag of clicked button
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Menu Processors")
		void WidgetChanged(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);

	/**
	* @descr Overridable function which handles pointed ui elements in menu
	* @param CurrentWidget - Current menu widget
	* @param PointedElement - Detected pointed element
	* @param ElementTag - Specific tag of detected element
	* @param bIsHovered - true if element was hovered otherwise false
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Menu Processors")
		void ElementPointed(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered);

	/**
	* @descr Overridable function which handles clicked ui elements in menu
	* @param CurrentWidget - Current menu widget
	* @param ClickedElement - Detected clicked element
	* @param ElementTag - Specific tag of detected element
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Menu Processors")
		void ElementClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag);

	/**
	* @descr Overridable function which handles menu widget component's tick in menu
	* @param MenuWidgetComponent - Reference to widget component that calls
	* @param DeltaTime - Tick delta time
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Menu Processors")
		void MenuTick(float DeltaTime);

};

