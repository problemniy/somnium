// Fill out your copyright notice in the Description page of Project Settings.

#include "ChangeGameLocaleProcessor.h"
#include "Utils/AssetManagementUtils.h"
#include "Providers/ConstantsProvider.h"
#include "Providers/ServicesProvider.h"

UChangeGameLocaleProcessor::UChangeGameLocaleProcessor() {
	ActualLanguage = EUIElementTag::EMPTY;
}

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UChangeGameLocaleProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);

	if (LocaleParsers.Num() > 0 && (ActualLanguage == EUIElementTag::EMPTY || IsValid(WidgetToClose) && EUIElementTag::SETTINGS == IUISpecificElement::Execute_GetUITag(WidgetToClose))) {
		DetermineActualLanguage();
	}
}

// Checks locale culture name and fills actual language by associated ui lement tag
void UChangeGameLocaleProcessor::DetermineActualLanguage() {
	UGameSettingsData* SavedGameData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, false));
	if (IsValid(SavedGameData) && ActualLanguage != SavedGameData->Language) {
		FString LocaleCultureName = ParseLocale(SavedGameData->Language);
		if (UAssetManagementUtils::IsLocaleCultureNameCorrect(LocaleCultureName)) {
			UAssetManagementUtils::ChangeGameLocale(LocaleCultureName);
			ActualLanguage = SavedGameData->Language;
		}
	}
}

FString UChangeGameLocaleProcessor::ParseLocale(const EUIElementTag& UIElementTag) {
	FString Result;
	for (FChangeLocaleProcessorParser LocaleParser : LocaleParsers) {
		if (LocaleParser.UIElementTag == UIElementTag) {
			Result = LocaleParser.LocaleCultureName;
		}
	}
	return Result;
}
