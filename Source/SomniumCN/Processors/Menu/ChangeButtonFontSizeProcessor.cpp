// Fill out your copyright notice in the Description page of Project Settings.

#include "ChangeButtonFontSizeProcessor.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UChangeButtonFontSizeProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	
	// restore texts font size and clear temporary collection if widget was changed
	if (WidgetToOpen && ButtonToPrimaryFontSizeMap.Num() > 0) {
		for (TPair<UGameButton*, int32> PairValue : ButtonToPrimaryFontSizeMap) {
			UGameText* ButtonText = GetGameText(PairValue.Key);
			ButtonText->SetFontSize(PairValue.Value);
		}
		ButtonToPrimaryFontSizeMap.Empty();
	}
	
}

/** Overridable function which handles pointed ui elements in menu */
void UChangeButtonFontSizeProcessor::ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) {
	
	if (PointedElement->IsA(UGameButton::StaticClass())) {
		UGameButton* PointedButton = Cast<UGameButton>(PointedElement);
		bool bIsAlreadyCheckedButton = ButtonToPrimaryFontSizeMap.Contains(PointedButton);

		// Check button for available to click
		if (!bIsAlreadyCheckedButton && !PointedButton->GetIsEnabled()) {
			return;
		}
		// set to new font size
		if (bIsHovered) {
			UGameText* GameText = GetGameText(PointedButton);
			if (GameText) {
				if (!bIsAlreadyCheckedButton) {
					ButtonToPrimaryFontSizeMap.Add(PointedButton, GameText->Font.Size);
				}
				GameText->SetFontSize(ButtonPointedSize);
			}
		}
		// set to primary font size
		else if (bIsAlreadyCheckedButton) {
			RestoreFontSize(PointedButton);
		}
	}
	
};

UGameText* UChangeButtonFontSizeProcessor::GetGameText(UGameButton* GameButton) {
	UGameText* Result = nullptr;
	for (int32 i = 0; i < GameButton->GetChildrenCount(); i++) {
		UGameText* ButtonText = Cast<UGameText>(GameButton->GetChildAt(i));
		if (ButtonText) {
			Result = ButtonText;
			break;
		}
	}
	return Result;
};

void UChangeButtonFontSizeProcessor::RestoreFontSize(UGameButton* PointedButton) {
	UGameText* GameText = GetGameText(PointedButton);
	int32* PrimaryFontSize = ButtonToPrimaryFontSizeMap.Find(PointedButton);
	GameText->SetFontSize(*PrimaryFontSize);
	ButtonToPrimaryFontSizeMap.Remove(PointedButton);
}

