// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "Data/SaveGame/GameSlotData.h"
#include "MenuContinueProcessor.generated.h"

/**
* @descr Class which implements continue logic in game menu
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UMenuContinueProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	// Returns last saved game slot if it's available
	UGameSlotData* GetActualGameSlot();
	// Checks for main menu widget to open
	bool IsMainMenuWidgetToOpen(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);

private:
	// Holds last saved slot if processor found it
	UPROPERTY()
		UGameSlotData* LastSavedSlot;
	
protected:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;

};
