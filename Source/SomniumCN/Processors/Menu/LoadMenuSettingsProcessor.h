// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "LoadMenuSettingsProcessor.generated.h"

/**
* @descr Class which loads menu settings from data file
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API ULoadMenuSettingsProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	void LoadSettingsValues(UGameMenuWidget* Widget, UGameSettingsData* GameSettingsData);
	// Parses and fills values to line selectors
	void ParseLineSelectorOption(ULineSelector* LineSelector, const EUIElementTag SelectorTag, UGameSettingsData* GameSettingsData);

public:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};
