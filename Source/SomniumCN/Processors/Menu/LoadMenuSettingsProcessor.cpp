// Fill out your copyright notice in the Description page of Project Settings.

#include "LoadMenuSettingsProcessor.h"
#include "Providers/ServicesProvider.h"
#include "Utils/SaveGameUtils.h"
#include "Kismet/KismetSystemLibrary.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void ULoadMenuSettingsProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	if (EUIElementTag::SETTINGS == IUISpecificElement::Execute_GetUITag(WidgetToOpen)) {
		UGameSettingsData* GameSettingsData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, false));
		if (!GameSettingsData) {
			// save a new settings data if it's missing
			GameSettingsData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, true));
			UServicesProvider::GetSaveGameService()->SaveGameData(GameSettingsData);
		}
		LoadSettingsValues(WidgetToOpen, GameSettingsData);
	}
}

void ULoadMenuSettingsProcessor::LoadSettingsValues(UGameMenuWidget* Widget, UGameSettingsData* GameSettingsData) {
	for (ULineSelector* Selector : Widget->LineSelectors) {
		ParseLineSelectorOption(Selector, IUISpecificElement::Execute_GetUITag(Selector), GameSettingsData);
	}
}

// Parses and fills values to line selectors
void ULoadMenuSettingsProcessor::ParseLineSelectorOption(ULineSelector* LineSelector, const EUIElementTag SelectorTag, UGameSettingsData* GameSettingsData) {
	EUIElementTag DefaultValue = EUIElementTag::EMPTY;
	EUIElementTag& ActualFieldWithValue = USaveGameUtils::GetSettingsDataField(GameSettingsData, SelectorTag, DefaultValue);
	if (ActualFieldWithValue != EUIElementTag::EMPTY) {
		LineSelector->SetOptionByUITag(ActualFieldWithValue);
	}
}