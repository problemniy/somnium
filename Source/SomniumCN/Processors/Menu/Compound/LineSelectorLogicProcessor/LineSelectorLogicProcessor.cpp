// Fill out your copyright notice in the Description page of Project Settings.

#include "LineSelectorLogicProcessor.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void ULineSelectorLogicProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);

}

/** Overridable function which handles clicked ui elements in menu */
void ULineSelectorLogicProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);

}

/** Overridable function which handles pointed ui elements in menu */
void ULineSelectorLogicProcessor::ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) {
	Super::ElementPointed_Implementation(CurrentWidget, PointedElement, ElementTag, bIsHovered);

}