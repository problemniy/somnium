// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/LineSelectorLogicProcessor/Logic/ShowLSelectorArrowsProcessor.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "UpdateSettingsViaLSelectorPro.generated.h"

/**
* @descr Class which updates menu game settings via line selectors
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UUpdateSettingsViaLSelectorPro : public UShowLSelectorArrowsProcessor {

	GENERATED_BODY()

private:
	void PrepareNewValues(UGameSettingsData* GameSettingsData, const EUIElementTag SelectedOption);
	// Checks whether the settings property should change
	bool PropertyShouldBeChanged(UGameSettingsData* GameSettingsData, const EUIElementTag SelectedOption);

protected:
	// Overridable function which will be invoked when user clicked on line selector option
	virtual void OnOptionSelected(const EUIElementTag OptionTag) override;
	
};
