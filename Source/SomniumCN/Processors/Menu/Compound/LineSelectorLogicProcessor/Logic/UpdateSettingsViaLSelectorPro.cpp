// Fill out your copyright notice in the Description page of Project Settings.

#include "UpdateSettingsViaLSelectorPro.h"
#include "Providers/ServicesProvider.h"
#include "Utils/SaveGameUtils.h"
#include "Providers/GameInstanceDataProvider.h"

// Overridable function which will be invoked when user clicked on line selector option
void UUpdateSettingsViaLSelectorPro::OnOptionSelected(const EUIElementTag OptionTag) {
	Super::OnOptionSelected(OptionTag);

	UGameSettingsData* GameSettingsData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, false));
	if (!GameSettingsData) {
		return;
	}
	else if (PropertyShouldBeChanged(GameSettingsData, OptionTag)) {
		UPreparedChangesToSaveData* PreparedChanges = UGameInstanceDataProvider::GetPreparedChangesToSaveData(PreviousLineSelector);
		if (PreparedChanges) {
			if (!PreparedChanges->GameSettingsDataToSave) {
				PreparedChanges->GameSettingsDataToSave = DuplicateObject(GameSettingsData, NULL);
			}
			PrepareNewValues(PreparedChanges->GameSettingsDataToSave, OptionTag);
		}
	}

}

// Checks whether the settings property should change
bool UUpdateSettingsViaLSelectorPro::PropertyShouldBeChanged(UGameSettingsData* GameSettingsData, const EUIElementTag SelectedOption) {
	EUIElementTag DefaultValue = EUIElementTag::EMPTY;
	EUIElementTag& ActualFieldWithValue = USaveGameUtils::GetSettingsDataField(GameSettingsData, SelectedOption, DefaultValue);
	return SelectedOption != ActualFieldWithValue;
}

void UUpdateSettingsViaLSelectorPro::PrepareNewValues(UGameSettingsData* GameSettingsData, const EUIElementTag SelectedOption) {
	EUIElementTag DefaultValue = EUIElementTag::EMPTY;
	EUIElementTag& ActualFieldWithValue = USaveGameUtils::GetSettingsDataField(GameSettingsData, SelectedOption, DefaultValue);
	ActualFieldWithValue = SelectedOption;
}