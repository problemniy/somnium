// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/LineSelectorLogicProcessor/Logic/ChangeLSelectorFontSizeProcessor.h"
#include "ShowLSelectorArrowsProcessor.generated.h"

/**
* @descr Class which controls row buttons of line selectors
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UShowLSelectorArrowsProcessor : public UChangeLSelectorFontSizeProcessor {

	GENERATED_BODY()

protected:
	// Overridable function which will be invoked when user changed line selector to another one
	virtual void OnSelectorChanged(ULineSelector* NewLineSelector) override;

	virtual void ResetProperties() override;

protected:
	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;

};
