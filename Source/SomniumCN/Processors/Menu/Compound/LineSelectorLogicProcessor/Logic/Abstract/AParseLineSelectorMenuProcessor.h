// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "AParseLineSelectorMenuProcessor.generated.h"

/**
* @descr Abstract class which parses menu line selectors
* @author problemniy
*/
UCLASS(Abstract)
class SOMNIUMCN_API UAParseLineSelectorMenuProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

protected:
	// Should be true when user are using arrows to switch options
	bool bChoosingOption;
	// It will be previous line selector in case when user changed to another one
	UPROPERTY()
		ULineSelector* PreviousLineSelector;

protected:
	// Checks if clicked option is line selector arrow
	bool IsArrowOption(const EUIElementTag OptionTag) const;
	// Checks if selector was changed by user
	bool IsSelectorWasChanged(ULineSelector* SelectorToCheck) const;
	// Checks if previous selector has already filled
	bool IsPreviousSelectorAvailable() const;

protected:
	// Overridable function which will be invoked when options are available to choose
	virtual void OnOptionChoosing(const EUIElementTag OptionTag);

	// Overridable function which will be invoked when user clicked on line selector option
	virtual void OnOptionSelected(const EUIElementTag OptionTag);

	// Overridable function which will be invoked when user changed line selector to another one
	virtual void OnSelectorChanged(ULineSelector* NewLineSelector);

	// Determines choosing option flag
	virtual bool DetermineChoosingOption(const EUIElementTag OptionTag, ULineSelector* LineSelector);

	// Clears temporary properties (invokes on menu changed action)
	virtual void ResetProperties();

public:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;

	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;
	
};
