// Fill out your copyright notice in the Description page of Project Settings.

#include "AParseLineSelectorMenuProcessor.h"

/**
* Overridable function which handles switching widgets in menu.
* WidgetToClose can be nullptr (usually when first opening)
*/
void UAParseLineSelectorMenuProcessor::WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	Super::WidgetChanged_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);
	ResetProperties();
}

/** Overridable function which handles clicked ui elements in menu */
void UAParseLineSelectorMenuProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);

	if (bChoosingOption && ClickedElement->IsA(UGameMenuWidget::StaticClass())) {
		if (IsValid(PreviousLineSelector)) {
			OnOptionSelected(IUISpecificElement::Execute_GetUITag(PreviousLineSelector));
		}
		ResetProperties();
	}
	else if (ClickedElement->IsA(ULineSelector::StaticClass())) {
		ULineSelector* LineSelector = Cast<ULineSelector>(ClickedElement);
		bool bSelectorChanged = IsSelectorWasChanged(LineSelector);
		bChoosingOption = DetermineChoosingOption(ElementTag, LineSelector);

		// invoke appropriate abstract method
		if (bChoosingOption) {
			OnOptionChoosing(ElementTag);
		}
		else OnOptionSelected(ElementTag);
		// invoke selector changed
		if (IsSelectorWasChanged(LineSelector)) {
			// invoke option selected for previous selector
			if (IsValid(PreviousLineSelector)) {
				OnOptionSelected(IUISpecificElement::Execute_GetUITag(PreviousLineSelector));
			}
			OnSelectorChanged(LineSelector);
		}
		// update previous line selector
		PreviousLineSelector = LineSelector;
	}
	
}

// Checks if clicked option is line selector arrow
bool UAParseLineSelectorMenuProcessor::IsArrowOption(const EUIElementTag OptionTag) const {
	return EUIElementTag::LEFT_ARROW == OptionTag || EUIElementTag::RIGHT_ARROW == OptionTag;
}

// Checks if selector was changed by user
bool UAParseLineSelectorMenuProcessor::IsSelectorWasChanged(ULineSelector* SelectorToCheck) const {
	return IsPreviousSelectorAvailable() && PreviousLineSelector != SelectorToCheck;
}

// Checks if previous selector has already filled
bool UAParseLineSelectorMenuProcessor::IsPreviousSelectorAvailable() const {
	return IsValid(PreviousLineSelector);
}

// Determines choosing option flag
bool UAParseLineSelectorMenuProcessor::DetermineChoosingOption(const EUIElementTag OptionTag, ULineSelector* LineSelector) {
	return ((!IsPreviousSelectorAvailable() || !IsSelectorWasChanged(LineSelector))
		&& (!bChoosingOption && !IsArrowOption(OptionTag) || bChoosingOption && IsArrowOption(OptionTag)))
		|| (!IsArrowOption(OptionTag) && IsSelectorWasChanged(LineSelector));
}

// Clears temporary properties (invokes on menu changed action)
void UAParseLineSelectorMenuProcessor::ResetProperties() {
	bChoosingOption = false;
	PreviousLineSelector = nullptr;
}

// Overridable function which will be invoked when options are available to choose
void UAParseLineSelectorMenuProcessor::OnOptionChoosing(const EUIElementTag OptionTag) {
	// unimplemented();
}

// Overridable function which will be invoked when user clicked on line selector option
void UAParseLineSelectorMenuProcessor::OnOptionSelected(const EUIElementTag OptionTag) {
	// unimplemented();
}

// Overridable function which will be invoked when user changed line selector to another one
void UAParseLineSelectorMenuProcessor::OnSelectorChanged(ULineSelector* NewLineSelector) {
	// unimplemented();
}
