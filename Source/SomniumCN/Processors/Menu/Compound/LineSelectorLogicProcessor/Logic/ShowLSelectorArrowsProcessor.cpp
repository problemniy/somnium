// Fill out your copyright notice in the Description page of Project Settings.

#include "ShowLSelectorArrowsProcessor.h"

/** Overridable function which handles clicked ui elements in menu */
void UShowLSelectorArrowsProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);
	
	if (ClickedElement->IsA(UGameMenuWidget::StaticClass()) && IsPreviousSelectorAvailable()) {
		PreviousLineSelector->SetArrowsVisibility(false);
	}
	else if (ClickedElement->IsA(ULineSelector::StaticClass())) {
		ULineSelector* LineSelector = Cast<ULineSelector>(ClickedElement);
		// set visibility
		LineSelector->SetArrowsVisibility(bChoosingOption);
	}
	
}

// Overridable function which will be invoked when user changed line selector to another one
void UShowLSelectorArrowsProcessor::OnSelectorChanged(ULineSelector* NewLineSelector) {
	Super::OnSelectorChanged(NewLineSelector);
	PreviousLineSelector->SetArrowsVisibility(false);
}

void UShowLSelectorArrowsProcessor::ResetProperties() {
	if (IsPreviousSelectorAvailable()) {
		PreviousLineSelector->SetArrowsVisibility(false);
	}
	Super::ResetProperties();
}