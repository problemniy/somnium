// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/LineSelectorLogicProcessor/Logic/Abstract/AParseLineSelectorMenuProcessor.h"
#include "ChangeLSelectorFontSizeProcessor.generated.h"

/**
* @descr Class which changes text font size in line selectors
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UChangeLSelectorFontSizeProcessor : public UAParseLineSelectorMenuProcessor {

	GENERATED_BODY()

private:
	int32 PrimaryFontSize;

private:
	void ChangeOptionFontSize(const EUIElementTag OptionTag, ULineSelector* LineSelector, const int32 NewFontSize);
	void ResetOptionFontSize(ULineSelector* LineSelector);

protected:
	/** Specify new font size for pointed options */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration", meta = (ClampMin = 1))
		int32 OptionPointedSize = 50;
	/** Specify new font size for option while choosing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration", meta = (ClampMin = 1))
		int32 OptionSizeWhileChoosing = 35;

protected:
	// Overridable function which will be invoked when user changed line selector to another one
	virtual void OnSelectorChanged(ULineSelector* NewLineSelector) override;
	// Clears temporary properties (invokes on menu changed action)
	virtual void ResetProperties() override;

public:
	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;

	/** Overridable function which handles pointed ui elements in menu */
	virtual void ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) override;
	
};
