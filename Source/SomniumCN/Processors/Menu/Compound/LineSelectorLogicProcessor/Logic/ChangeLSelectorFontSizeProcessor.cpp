// Fill out your copyright notice in the Description page of Project Settings.

#include "ChangeLSelectorFontSizeProcessor.h"

/** Overridable function which handles clicked ui elements in menu */
void UChangeLSelectorFontSizeProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);
	
	if (ClickedElement->IsA(UGameMenuWidget::StaticClass()) && IsPreviousSelectorAvailable()) {
		ResetOptionFontSize(PreviousLineSelector);
	}
	else if (ClickedElement->IsA(ULineSelector::StaticClass())) {
		ULineSelector* LineSelector = Cast<ULineSelector>(ClickedElement);

		// reset option if needed
		if (!IsArrowOption(ElementTag)) {
			ChangeOptionFontSize(ElementTag, LineSelector, bChoosingOption ? OptionSizeWhileChoosing : OptionPointedSize);
		}
		// reset arrow if needed
		else if (!bChoosingOption) {
			ResetOptionFontSize(LineSelector);
		}
	}
	
}

/** Overridable function which handles pointed ui elements in menu */
void UChangeLSelectorFontSizeProcessor::ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) {
	Super::ElementPointed_Implementation(CurrentWidget, PointedElement, ElementTag, bIsHovered);
	
	if (PointedElement->IsA(ULineSelector::StaticClass())) {
		ULineSelector* LineSelector = Cast<ULineSelector>(PointedElement);
		if (bIsHovered && !LineSelector->GetIsEnabled()) {
			return;
		}

		if (bChoosingOption && IsArrowOption(ElementTag) || !bChoosingOption && !IsArrowOption(ElementTag)) {
			// change font size for appropriated options
			if (bIsHovered) {
				PrimaryFontSize = LineSelector->Font.Size;
			}
			ChangeOptionFontSize(ElementTag, LineSelector, bIsHovered ? OptionPointedSize : PrimaryFontSize);
		}
		else if (bChoosingOption && !IsArrowOption(ElementTag) || !bChoosingOption && IsArrowOption(ElementTag)) {
			// return default font size for non-appropriated options
			if (bIsHovered && !IsSelectorWasChanged(LineSelector)) {
				PrimaryFontSize = LineSelector->Font.Size;
			}
			ChangeOptionFontSize(ElementTag, LineSelector,
				!IsArrowOption(ElementTag) && IsSelectorWasChanged(LineSelector) ?
				bIsHovered ? OptionPointedSize : PrimaryFontSize
				: OptionSizeWhileChoosing);
		}
	}
}

// Overridable function which will be invoked when user changed line selector to another one
void UChangeLSelectorFontSizeProcessor::OnSelectorChanged(ULineSelector* NewLineSelector) {
	Super::OnSelectorChanged(NewLineSelector);
	ResetOptionFontSize(PreviousLineSelector);
}

// Clears temporary properties (invokes on menu changed action)
void UChangeLSelectorFontSizeProcessor::ResetProperties() {
	if (IsPreviousSelectorAvailable()) {
		ResetOptionFontSize(PreviousLineSelector);
	}
	Super::ResetProperties();
}

void UChangeLSelectorFontSizeProcessor::ResetOptionFontSize(ULineSelector* LineSelector) {
	ChangeOptionFontSize(EUIElementTag::EMPTY, LineSelector, PrimaryFontSize);
}

void UChangeLSelectorFontSizeProcessor::ChangeOptionFontSize(const EUIElementTag OptionTag, ULineSelector* LineSelector, const int32 NewFontSize) {
	switch (OptionTag) {
	case EUIElementTag::LEFT_ARROW:
		LineSelector->SetLeftArrowFontSize(NewFontSize);
		break;
	case EUIElementTag::RIGHT_ARROW:
		LineSelector->SetRightArrowFontSize(NewFontSize);
		break;
	default:
		LineSelector->SetOptionFontSize(NewFontSize);
	}
}