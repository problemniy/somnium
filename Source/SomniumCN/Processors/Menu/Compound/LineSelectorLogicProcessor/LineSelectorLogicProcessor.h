// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/LineSelectorLogicProcessor/Logic/UpdateSettingsViaLSelectorPro.h"
#include "LineSelectorLogicProcessor.generated.h"

/**
* @descr Class which represents common logic for line selector elements in menu
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API ULineSelectorLogicProcessor : public UUpdateSettingsViaLSelectorPro {

	GENERATED_BODY()

public:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;

	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;

	/** Overridable function which handles pointed ui elements in menu */
	virtual void ElementPointed_Implementation(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) override;
	
};
