// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Processors/Menu/Compound/ConfirmSetChangesLogicProcessor/Logic/Abstract/ACheckSettingsChangesProcessor.h"
#include "ShowSetChangesPopupProcessor.generated.h"

/**
* @descr Class which controls popup for confirmation settings changes
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UShowSetChangesPopupProcessor : public UACheckSettingsChangesProcessor {

	GENERATED_BODY()

private:
	UGameButton* FindBackButton(UGameMenuWidget* ContextWidget);
	void ResetPopup(UGameMenuWidget* CurrentWidget);

protected:
	UPROPERTY()
		UGameWidget* PopupWidget;

protected:
	virtual void OnConfirmChangesPopupOpened(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);
	virtual void OnConfirmChangesYesClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag);
	virtual void OnConfirmChangesNoClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag);

	// Makes clickable/unclickable elements on background while popup is opened
	void SetEnabledBackgoroundClickableElements(UGameMenuWidget* ContextWidget, bool bIsEnabled);

	void SetEnabledBackgroundButtons(UGameMenuWidget* ContextWidget, bool bIsEnabled);
	void SetEnabledBackgroundLSelectors(UGameMenuWidget* ContextWidget, bool bIsEnabled);
	void SetEnabledBackgroundTexts(UGameMenuWidget* ContextWidget, bool bIsEnabled);

	/**
	* Ivoked when settings changes were detected.
	* �alling this method ensures that game settings data and prepared game settings data are existed.
	* It should return true if widget change is allowed
	*/
	virtual bool OnSettingsWereChanged(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;

public:
	/** Overridable function which handles clicked ui elements in menu */
	virtual void ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;
	
};
