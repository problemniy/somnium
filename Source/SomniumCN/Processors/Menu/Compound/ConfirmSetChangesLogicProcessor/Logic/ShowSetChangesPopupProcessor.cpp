// Fill out your copyright notice in the Description page of Project Settings.

#include "ShowSetChangesPopupProcessor.h"
#include "Kismet/KismetSystemLibrary.h"

/** Overridable function which handles clicked ui elements in menu */
void UShowSetChangesPopupProcessor::ElementClicked_Implementation(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	Super::ElementClicked_Implementation(CurrentWidget, ClickedElement, ElementTag);

	if (ClickedElement->IsA(UGameButton::StaticClass())) {
		if (EUIElementTag::CONFIRM_SCHANGES_YES == ElementTag) {
			OnConfirmChangesYesClicked(CurrentWidget, ClickedElement, ElementTag);
		}
		else if (EUIElementTag::CONFIRM_SCHANGES_NO == ElementTag) {
			OnConfirmChangesNoClicked(CurrentWidget, ClickedElement, ElementTag);
		}
	}
}

/**
* Ivoked when settings changes were detected.
* �alling this method ensures that game settings data and prepared game settings data are existed.
* It should return true if widget change is allowed
*/
bool UShowSetChangesPopupProcessor::OnSettingsWereChanged(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	bool bResult = Super::OnSettingsWereChanged(WidgetToClose, WidgetToOpen, ButtonTag);
	if (!IsValid(PopupWidget)) {
		// try to find popup sub-widget
		for (UGameWidget* Widget : WidgetToClose->GameWidgets) {
			if (UKismetSystemLibrary::DoesImplementInterface(Widget, UUISpecificElement::StaticClass())
				&& EUIElementTag::CONFIRM_SCHANGES == IUISpecificElement::Execute_GetUITag(Widget)) {
				PopupWidget = Widget;
				Widget->SetVisibility(ESlateVisibility::Visible);
				OnConfirmChangesPopupOpened(WidgetToClose, WidgetToOpen, ButtonTag);
				bResult = false;
				break;
			}
		}
	}
	return bResult;
}



// Makes unclickable buttons on background while popup is opened
void UShowSetChangesPopupProcessor::SetEnabledBackgoroundClickableElements(UGameMenuWidget* ContextWidget, bool bIsEnabled) {
	SetEnabledBackgroundButtons(ContextWidget, bIsEnabled);
	SetEnabledBackgroundLSelectors(ContextWidget, bIsEnabled);
	SetEnabledBackgroundTexts(ContextWidget, bIsEnabled);
}

void UShowSetChangesPopupProcessor::SetEnabledBackgroundButtons(UGameMenuWidget* ContextWidget, bool bIsEnabled) {
	for (UGameButton* Button : ContextWidget->GameButtons) {
		if (EUIElementTag::CONFIRM_SCHANGES_YES != IUISpecificElement::Execute_GetUITag(Button) && EUIElementTag::CONFIRM_SCHANGES_NO != IUISpecificElement::Execute_GetUITag(Button)) {
			Button->SetIsEnabled(bIsEnabled);
		}
	}
}

void UShowSetChangesPopupProcessor::SetEnabledBackgroundLSelectors(UGameMenuWidget* ContextWidget, bool bIsEnabled) {
	for (ULineSelector* LSelector : ContextWidget->LineSelectors) {
		LSelector->SetIsEnabled(bIsEnabled);
	}
}

void UShowSetChangesPopupProcessor::SetEnabledBackgroundTexts(UGameMenuWidget* ContextWidget, bool bIsEnabled) {
	for (UGameText* Text : ContextWidget->GameTexts) {
		Text->SetIsEnabled(bIsEnabled);
	}
}

UGameButton* UShowSetChangesPopupProcessor::FindBackButton(UGameMenuWidget* ContextWidget) {
	UGameButton* Result = nullptr;
	for (UGameButton* Button : ContextWidget->GameButtons) {
		if (EUIElementTag::BACK == IUISpecificElement::Execute_GetUITag(Button)) {
			Result = Button;
			break;
		}
	}
	return Result;
}

void UShowSetChangesPopupProcessor::OnConfirmChangesPopupOpened(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	SetEnabledBackgoroundClickableElements(WidgetToClose, false);
}

void UShowSetChangesPopupProcessor::OnConfirmChangesYesClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	ResetPopup(CurrentWidget);
}

void UShowSetChangesPopupProcessor::OnConfirmChangesNoClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	ResetPopup(CurrentWidget);
}

void UShowSetChangesPopupProcessor::ResetPopup(UGameMenuWidget* CurrentWidget) {
	SetEnabledBackgoroundClickableElements(CurrentWidget, true);
	UGameButton* BackButton = FindBackButton(CurrentWidget);
	if (BackButton) {
		// try to invoke back button if it's existed
		BackButton->OnReleased.Broadcast();
	}
	PopupWidget->SetVisibility(ESlateVisibility::Hidden);
	PopupWidget = nullptr;
}

