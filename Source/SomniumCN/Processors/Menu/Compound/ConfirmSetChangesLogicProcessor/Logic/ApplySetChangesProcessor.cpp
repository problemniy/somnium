// Fill out your copyright notice in the Description page of Project Settings.

#include "ApplySetChangesProcessor.h"
#include "Providers/GameInstanceDataProvider.h"
#include "Providers/ServicesProvider.h"

void UApplySetChangesProcessor::OnConfirmChangesYesClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	UPreparedChangesToSaveData* PreparedChanges = UGameInstanceDataProvider::GetPreparedChangesToSaveData(CurrentWidget);
	UServicesProvider::GetSaveGameService()->SaveGameData(PreparedChanges->GameSettingsDataToSave);
	Super::OnConfirmChangesYesClicked(CurrentWidget, ClickedElement, ElementTag);
}

void UApplySetChangesProcessor::OnConfirmChangesNoClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	UPreparedChangesToSaveData* PreparedChanges = UGameInstanceDataProvider::GetPreparedChangesToSaveData(CurrentWidget);
	// just clear prepared settings data
	PreparedChanges->Reset_Implementation();
	Super::OnConfirmChangesNoClicked(CurrentWidget, ClickedElement, ElementTag);
}