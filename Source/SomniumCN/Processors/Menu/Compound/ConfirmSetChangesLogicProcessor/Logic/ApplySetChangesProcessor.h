// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/ConfirmSetChangesLogicProcessor/Logic/ShowSetChangesPopupProcessor.h"
#include "Data/GameInstanceData/PreparedChangesToSaveData.h"
#include "ApplySetChangesProcessor.generated.h"

/**
* @descr Class which controls applying settings changes
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UApplySetChangesProcessor : public UShowSetChangesPopupProcessor {

	GENERATED_BODY()

protected:
	virtual void OnConfirmChangesYesClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;

	virtual void OnConfirmChangesNoClicked(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) override;
	
};
