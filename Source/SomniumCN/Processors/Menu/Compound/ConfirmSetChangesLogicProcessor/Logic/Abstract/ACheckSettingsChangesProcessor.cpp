// Fill out your copyright notice in the Description page of Project Settings.

#include "ACheckSettingsChangesProcessor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Providers/GameInstanceDataProvider.h"
#include "Providers/ServicesProvider.h"

/** Overridable function which is invoked before widgets switching to verify resolution */
bool UACheckSettingsChangesProcessor::ValidateWidgetChange_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	if (UKismetSystemLibrary::DoesImplementInterface(WidgetToClose, UUISpecificElement::StaticClass())
		&& EUIElementTag::SETTINGS == IUISpecificElement::Execute_GetUITag(WidgetToClose)) {
		for (UGameWidget* Widget : WidgetToClose->GameWidgets) {
			if (IsSettingsWereChanged(WidgetToOpen)) {
				// invoke on settings were changed abstract
				return OnSettingsWereChanged(WidgetToClose, WidgetToOpen, ButtonTag);
			} 
		}
	}
	return Super::ValidateWidgetChange_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);
}

bool UACheckSettingsChangesProcessor::IsSettingsWereChanged(UGameMenuWidget* ContextWidget) {
	UGameSettingsData* GameSettingsData = Cast<UGameSettingsData>(UServicesProvider::GetSaveGameService()->ReceiveSavedGameData(UGameSettingsData::StaticClass(), EGameSaveDataIdentity::GAME_SETTINGS, false));
	if (!GameSettingsData) {
		return false;
	}
	UPreparedChangesToSaveData* PreparedChanges = UGameInstanceDataProvider::GetPreparedChangesToSaveData(ContextWidget);
	bool bResult = false;
	if (PreparedChanges && PreparedChanges->GameSettingsDataToSave) {
		// fill game datas for possibility children
		AvailableGameSettingsData = GameSettingsData;
		PreparedGameSettingsData = PreparedChanges->GameSettingsDataToSave;
		// check changes
		bResult = !GameSettingsData->AreDataEqualsTo(PreparedChanges->GameSettingsDataToSave);
	}
	return bResult;
}

/**
* Ivoked when settings changes were detected.
* �alling this method ensures that game settings data and prepared game settings data are existed.
* It should return true if widget change is allowed
*/
bool UACheckSettingsChangesProcessor::OnSettingsWereChanged(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	// unimplemented();
	return true;
}