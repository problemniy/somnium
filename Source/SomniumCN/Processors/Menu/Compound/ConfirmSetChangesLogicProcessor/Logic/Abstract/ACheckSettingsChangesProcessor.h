// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "ACheckSettingsChangesProcessor.generated.h"

/**
* @descr Abstract class which checks changes in menu settings
* @author problemniy
*/
UCLASS(Abstract)
class SOMNIUMCN_API UACheckSettingsChangesProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

protected:
	UPROPERTY()
		UGameSettingsData* AvailableGameSettingsData;
	UPROPERTY()
		UGameSettingsData* PreparedGameSettingsData;

private:
	bool IsSettingsWereChanged(UGameMenuWidget* ContextWidget);

protected:
	/** 
	* Ivoked when settings changes were detected.
	* �alling this method ensures that game settings data and prepared game settings data are existed.
	* It should return true if widget change is allowed
	*/
	virtual bool OnSettingsWereChanged(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);

public:
	/** Overridable function which is invoked before widgets switching to verify resolution */
	virtual bool ValidateWidgetChange_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};
