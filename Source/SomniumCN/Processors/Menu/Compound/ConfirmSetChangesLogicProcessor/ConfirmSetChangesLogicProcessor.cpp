// Fill out your copyright notice in the Description page of Project Settings.

#include "ConfirmSetChangesLogicProcessor.h"

/** Overridable function which is invoked before widgets switching to verify resolution */
bool UConfirmSetChangesLogicProcessor::ValidateWidgetChange_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	return Super::ValidateWidgetChange_Implementation(WidgetToClose, WidgetToOpen, ButtonTag);
}

