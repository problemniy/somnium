// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Compound/ConfirmSetChangesLogicProcessor/Logic/ApplySetChangesProcessor.h"
#include "ConfirmSetChangesLogicProcessor.generated.h"

/**
* @descr Class which represents common logic for confirm settings changes
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UConfirmSetChangesLogicProcessor : public UApplySetChangesProcessor {

	GENERATED_BODY()

public:
	/** Overridable function which is invoked before widgets switching to verify resolution */
	virtual bool ValidateWidgetChange_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};
