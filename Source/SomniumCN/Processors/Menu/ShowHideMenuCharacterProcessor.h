// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "ShowHideMenuCharacterProcessor.generated.h"

/**
* @descr Class which shows or hides animated character in menu
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UShowHideMenuCharacterProcessor : public UAbstractMenuProcessor {

	GENERATED_BODY()

private:
	UPROPERTY()
		AActor* CharacterActor;

protected:
	/** Specify menu character actor tag */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Configuration")
		FName CharacterActorTag;

private:
	// Search character in menu
	void CheckCharacterInMenu(UObject* ContextObject);

protected:
	/**
	* Overridable function which handles switching widgets in menu.
	* WidgetToClose can be nullptr (usually when first opening)
	*/
	virtual void WidgetChanged_Implementation(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) override;
	
};
