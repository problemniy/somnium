// Fill out your copyright notice in the Description page of Project Settings.

#include "RegisterableInputComponent.h"

/** Removes binding for action and registration from game instance */
void URegisterableInputComponent::RemoveRegBindAction(const FName ActionName) {
	UGameInstanceManagementService* GameInstanceService = UServicesProvider::GetGameInstanceManagementService();
	if (GameInstanceService) {
		int32 EventPosition = GameInstanceService->GetActionEventPosition(ActionName, this);
		RemoveActionBinding(EventPosition);
		GameInstanceService->UnregActionEventOwner(ActionName, this);
	}

}
