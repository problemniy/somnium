// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InputComponent.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Providers/ServicesProvider.h"
#include "RegisterableInputComponent.generated.h"

/**
* @descr Input component which does event registration of binding in game instance
* @author problemniy
*/
UCLASS(ClassGroup = Components_)
class SOMNIUMCN_API URegisterableInputComponent : public UInputComponent {

	GENERATED_BODY()
	
public:
	/**
	* @descr Makes bind action with registration in game instance
	* @param ActionName - Action name to bind
	* @param KeyEvent - Key event to bind
	* @param Object - Delegate owner
	* @param FunctionPtr - Pointer to delegate function
	* @return FInputActionBinding struct which contains info about binding
	*/
	template<class UserClass>
	FInputActionBinding& RegBindAction(const FName ActionName, const EInputEvent KeyEvent, UserClass* Object, const void (UserClass::*FunctionPtr)()) {
		UGameInstanceManagementService* GameInstanceService = UServicesProvider::GetGameInstanceManagementService();
		if (GameInstanceService) {
			GameInstanceService->RegActionEventOwner(ActionName, this);
		}
		FInputActionBinding& ActionBinding = BindAction(ActionName, KeyEvent, Object, FunctionPtr);
		return ActionBinding;
	}

	/**
	* @descr Removes binding for action and registration from game instance
	* @param ActionName - Action name to remove
	*/
	void RemoveRegBindAction(const FName ActionName);
	
};
