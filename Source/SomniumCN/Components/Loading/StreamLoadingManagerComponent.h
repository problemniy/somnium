// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Loading/Abstract/LoadingManagerComponent.h"
#include "StreamLoadingManagerComponent.generated.h"

/**
* @descr Structure with loading info for streamable loading manager component
* @author problemniy
*/
USTRUCT(BlueprintType)
struct FStreamLoadingManagerComponentInfo {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify streamable levels packages to load */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FName> StreamableLevelsList;

	/** Specify streamable levels packages to unload */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FName> UnloadStreamableLevelsList;

};

/**
* @base ULoadingManagerComponent
* @descr Component class which manages streamable levels loading
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UStreamLoadingManagerComponent : public ULoadingManagerComponent {
	
	GENERATED_BODY()

public:
	UStreamLoadingManagerComponent();

private:
	struct StreamProgressInfoStruct : ProgressInfoStruct {
		int32 StreamableLevelsAmount, StreamableLevelsLoaded;
		int32 UnloadStreamableLevelsAmount, StreamableLevelsUnloaded;
		int32 LoadStreamableLatentIterator, UnloadStreamableLatentIterator;
		int32 CommonAmount;

		int RequestedGroups;

		FLatentActionInfo LoadStreamableLatentInfo;
		FLatentActionInfo UnloadStreamableLatentInfo;

		bool bLoadingStreamableLevelsInProgress;
		bool bUnloadingStreamableLevelsInProgress;

		void Reset() {
			StreamableLevelsAmount = 0;
			StreamableLevelsLoaded = 0;
			UnloadStreamableLevelsAmount = 0;
			StreamableLevelsUnloaded = 0;
			LoadStreamableLatentIterator = 0;
			UnloadStreamableLatentIterator = 0;
			CommonAmount = 0;

			RequestedGroups = 0;

			LoadStreamableLatentInfo.UUID = 0;
			UnloadStreamableLatentInfo.UUID = 0;

			bLoadingStreamableLevelsInProgress = false;
			bUnloadingStreamableLevelsInProgress = false;
		}
	} LoadingInfo;

	UFUNCTION()
		void OnStreamableLevelLoaded();
	UFUNCTION()
		void OnStreamableLevelUnloaded();

protected:
	/** Specify streamable loading info */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Prerequisites")
		FStreamLoadingManagerComponentInfo StreamableLoadingInfo;

protected:
	virtual bool DoLoadStreamableLevels();
	virtual bool DoUnloadStreamableLevels();

protected:
	/** Overridable function which checks loading for availability */
	virtual bool CheckIsLoadingAllowed_Implementation() override;

	/** Overridable function which is invoked on loading start */
	virtual void PrepareLoading_Implementation() override;

	/** Overridable function which starts loading */
	virtual bool DoLoad_Implementation() override;

public:
	/** Checks does loading manager have objects to load or not */
	virtual bool CheckHasObjectsToLoad_Implementation() override;

	/** Returns loading progress */
	virtual float GetProgress_Implementation() override;

public:
	/**
	* @descr Updates loading info if loading was not started
	* @param LoadingInfo - Streamable loading info
	* @return true if the info was updated otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Events")
		bool AssignLoadingInfo(FStreamLoadingManagerComponentInfo LoadingInfo);
	
};

