// Fill out your copyright notice in the Description page of Project Settings.


#include "StreamLoadingManagerComponent.h"
#include "Utils/GameCommonUtils.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

UStreamLoadingManagerComponent::UStreamLoadingManagerComponent() {
	//-------- prepare streamable latent infos
	// to load
	LoadingInfo.LoadStreamableLatentInfo.CallbackTarget = this;
	LoadingInfo.LoadStreamableLatentInfo.ExecutionFunction = FName(TEXT("OnStreamableLevelLoaded"));
	LoadingInfo.LoadStreamableLatentInfo.Linkage = 0;
	LoadingInfo.LoadStreamableLatentInfo.UUID = 0;
	// to unload
	LoadingInfo.UnloadStreamableLatentInfo.CallbackTarget = this;
	LoadingInfo.UnloadStreamableLatentInfo.ExecutionFunction = FName(TEXT("OnStreamableLevelUnloaded"));
	LoadingInfo.UnloadStreamableLatentInfo.Linkage = 0;
	LoadingInfo.UnloadStreamableLatentInfo.UUID = 0;
}

/** Overridable function which checks loading for availability */
bool UStreamLoadingManagerComponent::CheckIsLoadingAllowed_Implementation() {
	return Super::CheckIsLoadingAllowed_Implementation() && CheckHasObjectsToLoad();
}

/** Overridable function which is invoked on loading start */
void UStreamLoadingManagerComponent::PrepareLoading_Implementation() {
	Super::PrepareLoading_Implementation();
	LoadingInfo.Reset();

	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO,
		TEXT("[%s] Request to load stream levels:\nLevelsToLoad = %s\nLevelsToUnload = %s"),
		*GetName(),
		*UGameCommonUtils::ArrayAsString(StreamableLoadingInfo.StreamableLevelsList),
		*UGameCommonUtils::ArrayAsString(StreamableLoadingInfo.UnloadStreamableLevelsList)
		);

	LoadingInfo.StreamableLevelsAmount = StreamableLoadingInfo.StreamableLevelsList.Num();
	LoadingInfo.UnloadStreamableLevelsAmount = StreamableLoadingInfo.UnloadStreamableLevelsList.Num();
	LoadingInfo.CommonAmount = LoadingInfo.StreamableLevelsAmount + (LoadingInfo.UnloadStreamableLevelsAmount > 0);
	LoadingInfo.RequestedGroups = (LoadingInfo.StreamableLevelsAmount > 0) + (LoadingInfo.UnloadStreamableLevelsAmount > 0);
}

/** Overridable function which starts loading */
bool UStreamLoadingManagerComponent::DoLoad_Implementation() {
	if (LoadingInfo.CommonAmount == 0) {
		return true;
	}
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL,
		TEXT("[%s] Stream loading report:\nCommonAmount = %i, LoadedAmount = %i, UnloadedAmount = %i, ProgressPercent = %f\nInProgress = %s (LoadingInProgress = %s, UnloadingInProgress = %s"),
		*GetName(),
		LoadingInfo.CommonAmount,
		LoadingInfo.StreamableLevelsLoaded,
		LoadingInfo.StreamableLevelsUnloaded,
		GetProgress(),
		!LoadingInfo.bLoaded ? TEXT("true") : TEXT("false"),
		LoadingInfo.bLoadingStreamableLevelsInProgress ? TEXT("true") : TEXT("false"),
		LoadingInfo.bUnloadingStreamableLevelsInProgress ? TEXT("true") : TEXT("false")
		);

	return DoLoadStreamableLevels() && DoUnloadStreamableLevels();
}

bool UStreamLoadingManagerComponent::DoLoadStreamableLevels() {
	if (LoadingInfo.StreamableLevelsAmount == 0) {
		return true;
	}
	if (LoadingInfo.LoadStreamableLatentIterator == 0) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Load stream levels:\nStreamLevels = %s"),
			*GetName(),
			*UGameCommonUtils::ArrayAsString(StreamableLoadingInfo.StreamableLevelsList)
			);

		UGameplayStatics::LoadStreamLevel(GetWorld(), StreamableLoadingInfo.StreamableLevelsList[LoadingInfo.LoadStreamableLatentIterator], true, false, LoadingInfo.LoadStreamableLatentInfo);
		LoadingInfo.LoadStreamableLatentIterator++;
		LoadingInfo.bLoadingStreamableLevelsInProgress = true;
	}
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] Stream loading: LevelsAmount = %i, LevelsLoaded = %i, Progress = %f"),
		*GetName(),
		LoadingInfo.StreamableLevelsAmount,
		LoadingInfo.StreamableLevelsLoaded,
		(float)LoadingInfo.StreamableLevelsLoaded / (float)LoadingInfo.StreamableLevelsAmount
		);

	bool AreLoaded = LoadingInfo.StreamableLevelsLoaded == LoadingInfo.StreamableLevelsAmount;
	if (AreLoaded && LoadingInfo.bLoadingStreamableLevelsInProgress) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Stream levels have loaded!"), *GetName());

		LoadingInfo.bLoadingStreamableLevelsInProgress = false;
	}
	return AreLoaded;
}

bool UStreamLoadingManagerComponent::DoUnloadStreamableLevels() {
	if (LoadingInfo.UnloadStreamableLevelsAmount == 0) {
		return true;
	}
	if (LoadingInfo.UnloadStreamableLatentIterator == 0) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Unload stream levels:\nStreamLevels = %s"),
			*GetName(),
			*UGameCommonUtils::ArrayAsString(StreamableLoadingInfo.UnloadStreamableLevelsList)
		);

		UGameplayStatics::UnloadStreamLevel(GetWorld(), StreamableLoadingInfo.UnloadStreamableLevelsList[LoadingInfo.UnloadStreamableLatentIterator], LoadingInfo.UnloadStreamableLatentInfo, false);
		LoadingInfo.UnloadStreamableLatentIterator++;
		LoadingInfo.bUnloadingStreamableLevelsInProgress = true;
	}
	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] Stream unloading: LevelsAmount = %i, LevelsUnloaded = %i, Progress = %f"),
		*GetName(),
		LoadingInfo.UnloadStreamableLevelsAmount,
		LoadingInfo.StreamableLevelsUnloaded,
		(float)LoadingInfo.StreamableLevelsUnloaded / (float)LoadingInfo.UnloadStreamableLevelsAmount
		);

	bool AreUnloaded = LoadingInfo.StreamableLevelsUnloaded == LoadingInfo.UnloadStreamableLevelsAmount;
	if (AreUnloaded && LoadingInfo.bUnloadingStreamableLevelsInProgress) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Stream levels have unloaded!"), *GetName());

		LoadingInfo.bUnloadingStreamableLevelsInProgress = false;
	}
	return AreUnloaded;
}

void UStreamLoadingManagerComponent::OnStreamableLevelLoaded() {
	LoadingInfo.StreamableLevelsLoaded++;
	// continue streamable loading
	if (LoadingInfo.StreamableLevelsLoaded < LoadingInfo.StreamableLevelsAmount) {
		LoadingInfo.LoadStreamableLatentInfo.UUID++;
		UGameplayStatics::LoadStreamLevel(GetWorld(), StreamableLoadingInfo.StreamableLevelsList[LoadingInfo.LoadStreamableLatentIterator], true, false, LoadingInfo.LoadStreamableLatentInfo);
		LoadingInfo.LoadStreamableLatentIterator++;
	}
}

void UStreamLoadingManagerComponent::OnStreamableLevelUnloaded() {
	LoadingInfo.StreamableLevelsUnloaded++;
	// continue streamable unloading
	if (LoadingInfo.StreamableLevelsUnloaded < LoadingInfo.UnloadStreamableLevelsAmount) {
		LoadingInfo.UnloadStreamableLatentInfo.UUID++;
		UGameplayStatics::UnloadStreamLevel(GetWorld(), StreamableLoadingInfo.UnloadStreamableLevelsList[LoadingInfo.UnloadStreamableLatentIterator], LoadingInfo.UnloadStreamableLatentInfo, false);
		LoadingInfo.UnloadStreamableLatentIterator++;
	}
}

/** Updates loading info if loading was not started */
bool UStreamLoadingManagerComponent::AssignLoadingInfo(FStreamLoadingManagerComponentInfo $LoadingInfo) {
	if (!CheckInProgress()) {
		StreamableLoadingInfo = $LoadingInfo;
		return true;
	}
	return false;
}

/** Checks does loading manager have objects to load or not */
bool UStreamLoadingManagerComponent::CheckHasObjectsToLoad_Implementation() {
	return StreamableLoadingInfo.StreamableLevelsList.Num() > 0 || StreamableLoadingInfo.UnloadStreamableLevelsList.Num() > 0;
}

/** Returns loading progress */
float UStreamLoadingManagerComponent::GetProgress_Implementation() {
	float LoadStreamDenominator = (float)LoadingInfo.StreamableLevelsAmount * (float)LoadingInfo.RequestedGroups;
	float UnloadStreamDenominator = (float)LoadingInfo.UnloadStreamableLevelsAmount * (float)LoadingInfo.RequestedGroups;
	return LoadingInfo.RequestedGroups > 0.f ? 
		(LoadStreamDenominator > 0.f ? (float)LoadingInfo.StreamableLevelsLoaded / LoadStreamDenominator : 0.f)
		+ (UnloadStreamDenominator > 0.f ? (float)LoadingInfo.StreamableLevelsUnloaded / UnloadStreamDenominator : 0.f)
		: 0.f;
}

