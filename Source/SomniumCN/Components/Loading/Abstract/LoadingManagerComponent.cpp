// Fill out your copyright notice in the Description page of Project Settings.

#include "LoadingManagerComponent.h"
#include "Engine.h"

// Sets default values for this component's properties
ULoadingManagerComponent::ULoadingManagerComponent() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;


}

// Called when the game starts
void ULoadingManagerComponent::BeginPlay() {
	Super::BeginPlay();
	AssetManager = Cast<UGameAssetManager>(GEngine->AssetManager);
	if (!IsValid(AssetManager)) {
		GAME_ERROR(ELogsGroup::LOADING_ASSETS, TEXT("[%s] Cannot find Game Asset Manager, please check!"), *GetName());
	}
}

// Called every frame
void ULoadingManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!CheckIsLoadingAllowed()) {
		return;
	}

	if (!ProgressInfo.bInProgress) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] >>> Loading has started!"), *GetName());

		PrepareLoading();
	}
	ProgressInfo.bLoaded = DoLoad();
	ProgressInfo.bInProgress = !ProgressInfo.bLoaded;

	if (ProgressInfo.bLoaded) {
		bAllowTrigger = false;
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO, TEXT("[%s] >>> Loading has finished!"), *GetName());
	}
}

/** Overridable function which is invoked on loading start */
void ULoadingManagerComponent::PrepareLoading_Implementation() {
	// unimplemented();
}

/** Overridable function which starts loading */
bool ULoadingManagerComponent::DoLoad_Implementation() {
	return true;
}

/** Begins to load */
bool ULoadingManagerComponent::Load_Implementation() {
	if (ProgressInfo.bInProgress) {
		return false;
	}
	ProgressInfo.Reset();
	bAllowTrigger = true;
	return true;
}

/** Checks does loading manager have objects to load or not */
bool ULoadingManagerComponent::CheckHasObjectsToLoad_Implementation() {
	return ProgressInfo.bLoaded;
}

/** Checks is loading in progress or not */
bool ULoadingManagerComponent::CheckInProgress_Implementation() {
	return ProgressInfo.bInProgress;
}

/** Checks is loading finished or not */
bool ULoadingManagerComponent::CheckIsLoaded_Implementation() {
	return ProgressInfo.bLoaded;
}

/** Returns loading progress */
float ULoadingManagerComponent::GetProgress_Implementation() {
	return 0.f;
}

/** Overridable function which checks loading for availability */
bool ULoadingManagerComponent::CheckIsLoadingAllowed_Implementation() {
	return bAllowTrigger && !ProgressInfo.bLoaded;
}

