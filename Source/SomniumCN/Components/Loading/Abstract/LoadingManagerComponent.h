// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "EngineData/AssetManagers/GameAssetManager.h"
#include "Logging/GameLogMacros.h"
#include "Logging/GameLogDefinitions.h"
#include "LoadingManagerComponent.generated.h"


UCLASS(Abstract, ClassGroup = Components_, meta = (BlueprintSpawnableComponent))
class SOMNIUMCN_API ULoadingManagerComponent : public UActorComponent {

	GENERATED_BODY()

private:
	// Component will load if this trigger is true
	bool bAllowTrigger;

protected:
	struct ProgressInfoStruct {
		bool bInProgress;
		bool bLoaded;

		void Reset() {
			bInProgress = false;
			bLoaded = false;
		}
	} ProgressInfo;

	UPROPERTY(BlueprintReadOnly, Category = "Management")
		UGameAssetManager* AssetManager;

public:	
	// Sets default values for this component's properties
	ULoadingManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	/**
	* @descr Overridable function which checks loading for availability
	* @return true if loading is allowed otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool CheckIsLoadingAllowed();

	/**
	* @descr Overridable function which is invoked on loading start
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		void PrepareLoading();

	/**
	* @descr Overridable function which starts loading
	* @return true if loading is still active otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool DoLoad();

public:
	/**
	* @descr Begins to load
	* @return true if loading was started otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool Load();

	/**
	* @descr Checks does loading manager have objects to load or not
	* @return true if loading manager has objects to load otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool CheckHasObjectsToLoad();

	/**
	* @descr Checks is loading in progress or not
	* @return true if loading is in progress otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool CheckInProgress();

	/**
	* @descr Checks is loading finished or not
	* @return true if loading was finished otherwise false
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		bool CheckIsLoaded();

	/**
	* @descr Returns loading progress
	* @return Loading progress (should be calculated by sub-class otherwise it will be zero)
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Loading Manager Component")
		float GetProgress();

};

