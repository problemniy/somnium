// Fill out your copyright notice in the Description page of Project Settings.

#include "AssetsLoadingManagerComponent.h"
#include "Utils/GameCommonUtils.h"

/** Overridable function which checks loading for availability */
bool UAssetsLoadingManagerComponent::CheckIsLoadingAllowed_Implementation() {
	return Super::CheckIsLoadingAllowed_Implementation() && CheckHasObjectsToLoad();
}

/** Overridable function which is invoked on loading start */
void UAssetsLoadingManagerComponent::PrepareLoading_Implementation() {
	Super::PrepareLoading_Implementation();
	LoadingInfo.Reset();

	// -------- logs
	SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::INFO,
		TEXT("[%s] Request to load assets:\nAssetsList = %s\nPrimaryAssetsList = %s\nBundleNames = %s\nUnloadPrimaryAssetsList = %s"),
		*GetName(),
		*UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.AssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.PrimaryAssetsList),
		*UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.BundleNames),
		*UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.UnloadPrimaryAssetsList)
		);

	LoadingInfo.AssetsAmount = AssetsLoadingInfo.AssetsList.Num();
	LoadingInfo.PrimaryAssetsAmount = AssetsLoadingInfo.PrimaryAssetsList.Num();
	LoadingInfo.UnloadPrimaryAssetsAmount = AssetsLoadingInfo.UnloadPrimaryAssetsList.Num();
	LoadingInfo.CommonAmount = LoadingInfo.AssetsAmount + LoadingInfo.PrimaryAssetsAmount + (LoadingInfo.UnloadPrimaryAssetsAmount > 0);

	LoadingInfo.RequestedGroups = (LoadingInfo.AssetsAmount > 0) + (LoadingInfo.PrimaryAssetsAmount > 0) + (LoadingInfo.UnloadPrimaryAssetsAmount > 0);
}

// Called every frame
void UAssetsLoadingManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	// -------- logs
	if (CheckInProgress()) {
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL,
			TEXT("[%s] Assets loading report:\nCommonAmount = %i, LoadedAmount = %i, UnloadedAmount = %i, ProgressPercent = %f\nIsLoaded = %s (AssetsLoaded = %s, PrimaryAssetsLoaded = %s)"),
			*GetName(),
			LoadingInfo.CommonAmount,
			LoadingInfo.AssetsLoaded + LoadingInfo.PrimaryAssetsLoaded,
			LoadingInfo.PrimaryAssetsUnloaded,
			GetProgress(),
			LoadingInfo.bLoaded ? TEXT("true") : TEXT("false"),
			LoadingInfo.AssetsAmount > 0 && LoadingInfo.AssetsAmount == LoadingInfo.AssetsLoaded ? TEXT("true") : TEXT("false"),
			LoadingInfo.PrimaryAssetsAmount > 0 && LoadingInfo.PrimaryAssetsAmount == LoadingInfo.PrimaryAssetsLoaded ? TEXT("true") : TEXT("false")
		);
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

/** Overridable function which starts loading */
bool UAssetsLoadingManagerComponent::DoLoad_Implementation() {
	if (LoadingInfo.CommonAmount == 0) {
		return true;
	}
	return DoLoadAssets() && DoLoadPrimaryAssets() && DoUnloadPrimaryAssets();
}

/** Overridable function which starts loading */
bool UAssetsLoadingManagerComponent::DoLoadAssets() {
	if (LoadingInfo.AssetsLoaded == LoadingInfo.AssetsAmount) {
		if (LoadingInfo.bAssetsInProgress) {
			LoadingInfo.bAssetsInProgress = false;
			// -------- logs
			SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Loading assets has finished, CommonProgress = %f"), *GetName(), GetProgress());
		}
		return true;
	}

	if (!LoadingInfo.bAssetsInProgress) {
		LoadingInfo.bAssetsInProgress = true;
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Loading assets has started:\nAssetsToLoad = %s"),
			*GetName(), *UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.AssetsList));

		StreamableHandlePtr = AssetManager->LoadAssetList(AssetsLoadingInfo.AssetsList, FStreamableDelegate::CreateLambda([this]() {
			//////------------------------ Lambda (OnFinishedLoading)
			int32 GoodLoad = 0, BadLoad = 0;
			if (StreamableHandlePtr && StreamableHandlePtr.IsValid()) {
				TArray<UObject*> LoadedAssets;
				StreamableHandlePtr.Get()->GetLoadedAssets(LoadedAssets);
				GoodLoad = LoadedAssets.Num();
				BadLoad = LoadingInfo.AssetsAmount - LoadedAssets.Num();
				LoadingInfo.AssetsProgress = StreamableHandlePtr.Get()->GetProgress();
			}
			else {
				GoodLoad = 0;
				BadLoad = LoadingInfo.AssetsAmount;
				LoadingInfo.AssetsProgress = 1.f;
			}
			LoadingInfo.AssetsLoaded = LoadingInfo.AssetsAmount;
			// -------- logs
			SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL,
				TEXT("[%s] Streamable handle has loaded assets: GoodLoaded = %i, BadLoaded = %i"), *GetName(), GoodLoad, BadLoad);
			//////------------------------
		}));
		if (!StreamableHandlePtr.IsValid()) {
			LoadingInfo.AssetsLoaded = LoadingInfo.AssetsAmount;
			LoadingInfo.AssetsProgress = 1.f;
			return false;
		}
		else {
			StreamableHandlePtr.Get()->BindUpdateDelegate(FStreamableUpdateDelegate::CreateLambda([this](TSharedRef<struct FStreamableHandle> StreamableHandle) {
				//////------------------------ Lambda (OnAssetLoaded)
				LoadingInfo.AssetsLoaded++;
				LoadingInfo.AssetsProgress = StreamableHandlePtr.Get()->GetProgress();
				// -------- logs
				SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] Loading assets: AssetsAmount = %i, LoadedAmount = %i, Progress = %f"),
					*GetName(),
					LoadingInfo.AssetsAmount,
					LoadingInfo.AssetsLoaded,
					LoadingInfo.AssetsProgress
				);
				//////------------------------
			}));
		}
	}
	return false;
}

bool UAssetsLoadingManagerComponent::DoLoadPrimaryAssets() {
	if (LoadingInfo.PrimaryAssetsLoaded == LoadingInfo.PrimaryAssetsAmount) {
		if (LoadingInfo.bPrimaryAssetsInProgress) {
			LoadingInfo.bPrimaryAssetsInProgress = false;
			// -------- logs
			SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Loading primary assets has finished, CommonProgress = %f"), *GetName(), GetProgress());
		}
		return true;
	}

	if (!LoadingInfo.bPrimaryAssetsInProgress) {
		LoadingInfo.bPrimaryAssetsInProgress = true;
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Loading primary assets has started:\nPrimaryAssetsToLoad = %s"),
			*GetName(), *UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.PrimaryAssetsList));

		StreamableHandlePtr = AssetManager->LoadPrimaryAssets(AssetsLoadingInfo.PrimaryAssetsList, AssetsLoadingInfo.BundleNames, FStreamableDelegate::CreateLambda([this]() {
			//////------------------------ Lambda (OnFinishedLoading)
			int32 GoodLoad = 0, BadLoad = 0;
			if (StreamableHandlePtr && StreamableHandlePtr.IsValid()) {
				TArray<UObject*> LoadedAssets;
				StreamableHandlePtr.Get()->GetLoadedAssets(LoadedAssets);
				GoodLoad = LoadedAssets.Num();
				BadLoad = LoadingInfo.PrimaryAssetsAmount - LoadedAssets.Num();
				LoadingInfo.PrimaryAssetsProgress = StreamableHandlePtr.Get()->GetProgress();
			}
			else {
				GoodLoad = 0;
				BadLoad = LoadingInfo.AssetsAmount;
				LoadingInfo.PrimaryAssetsProgress = 1.f;
			}
			LoadingInfo.PrimaryAssetsLoaded = LoadingInfo.PrimaryAssetsAmount;
			// -------- logs
			SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL,
				TEXT("[%s] Streamable handle has loaded assets: GoodLoaded = %i, BadLoaded = %i"), *GetName(), GoodLoad, BadLoad);
			//////------------------------
		}));
		if (!StreamableHandlePtr.IsValid()) {
			LoadingInfo.PrimaryAssetsLoaded = LoadingInfo.PrimaryAssetsAmount;
			LoadingInfo.PrimaryAssetsProgress = 1.f;
			return false;
		}
		else {
			StreamableHandlePtr.Get()->BindUpdateDelegate(FStreamableUpdateDelegate::CreateLambda([this](TSharedRef<struct FStreamableHandle> StreamableHandle) {
				//////------------------------ Lambda (OnPrimaryAssetLoaded)
				LoadingInfo.PrimaryAssetsLoaded++;
				LoadingInfo.PrimaryAssetsProgress = StreamableHandlePtr.Get()->GetProgress();
				// -------- logs
				SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::FULL, TEXT("[%s] Loading primary assets: AssetsAmount = %i, LoadedAmount = %i, Progress = %f"),
					*GetName(), 
					LoadingInfo.PrimaryAssetsAmount,
					LoadingInfo.PrimaryAssetsLoaded,
					LoadingInfo.PrimaryAssetsProgress
				);
				//////------------------------
			}));
		}
	}
	return false;
}

bool UAssetsLoadingManagerComponent::DoUnloadPrimaryAssets() {
	if (AssetsLoadingInfo.UnloadPrimaryAssetsList.Num() > 0) {
		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Unloading primary assets has started: PrimaryAssetsToLoad = %s"),
			*GetName(), *UGameCommonUtils::ArrayAsString(AssetsLoadingInfo.UnloadPrimaryAssetsList));

		LoadingInfo.PrimaryAssetsUnloaded = AssetManager->UnloadPrimaryAssets(AssetsLoadingInfo.UnloadPrimaryAssetsList);

		// -------- logs
		SHOW_GAME_LOG(ELogsGroup::LOADING_ASSETS, ELogsLevel::DEBUG, TEXT("[%s] Unloading primary assets has finished, UnloadedAmount = %i"), *GetName(), LoadingInfo.PrimaryAssetsUnloaded);

		LoadingInfo.UnloadPrimaryAssetsProgress = 1.f;
	}
	return true;
}

/** Updates loading info if loading was not started */
bool UAssetsLoadingManagerComponent::AssignLoadingInfo(FAssetsLoadingManagerComponentInfo $LoadingInfo) {
	if (!CheckInProgress()) {
		AssetsLoadingInfo = $LoadingInfo;
		return true;
	}
	return false;
}

/** Checks does loading manager have objects to load or not */
bool UAssetsLoadingManagerComponent::CheckHasObjectsToLoad_Implementation() {
	return AssetsLoadingInfo.AssetsList.Num() > 0 || AssetsLoadingInfo.PrimaryAssetsList.Num() > 0 || AssetsLoadingInfo.UnloadPrimaryAssetsList.Num() > 0;
}

/** Returns loading progress */
float UAssetsLoadingManagerComponent::GetProgress_Implementation() {
	return LoadingInfo.RequestedGroups > 0 ?
		LoadingInfo.AssetsProgress / (float)LoadingInfo.RequestedGroups 
		+ LoadingInfo.PrimaryAssetsProgress / (float)LoadingInfo.RequestedGroups 
		+ LoadingInfo.UnloadPrimaryAssetsProgress / (float)LoadingInfo.RequestedGroups
		: 0.f;
}

