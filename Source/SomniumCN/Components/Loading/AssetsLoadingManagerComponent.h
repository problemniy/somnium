// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Loading/Abstract/LoadingManagerComponent.h"
#include "AssetsLoadingManagerComponent.generated.h"

/**
* @descr Structure with loading info for assets loading manager component
* @author problemniy
*/
USTRUCT(BlueprintType)
struct FAssetsLoadingManagerComponentInfo {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify assets paths to load */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FSoftObjectPath> AssetsList;

	/** Specify primary assets paths to load */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FPrimaryAssetId> PrimaryAssetsList;

	/** Specify required bundle names for primary assets */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FName> BundleNames;

	/** Specify primary assets paths to unload */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FPrimaryAssetId> UnloadPrimaryAssetsList;

};

/**
* @base ULoadingManagerComponent
* @descr Component class which manages assets loading
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UAssetsLoadingManagerComponent : public ULoadingManagerComponent {

	GENERATED_BODY()

private:
	struct AssetsProgressInfoStruct : ProgressInfoStruct {
		int32 AssetsAmount, AssetsLoaded;
		int32 PrimaryAssetsAmount, PrimaryAssetsLoaded;
		int32 UnloadPrimaryAssetsAmount, PrimaryAssetsUnloaded;
		int32 CommonAmount;

		int RequestedGroups;

		bool bAssetsInProgress;
		bool bPrimaryAssetsInProgress;

		float AssetsProgress;
		float PrimaryAssetsProgress;
		float UnloadPrimaryAssetsProgress;

		void Reset() {
			AssetsAmount = 0, AssetsLoaded = 0;
			PrimaryAssetsAmount = 0, PrimaryAssetsLoaded = 0;
			UnloadPrimaryAssetsAmount = 0, PrimaryAssetsUnloaded = 0;
			CommonAmount = 0;

			RequestedGroups = 0;

			bAssetsInProgress = false;
			bPrimaryAssetsInProgress = false;

			AssetsProgress = 0.f;
			PrimaryAssetsProgress = 0.f;
			UnloadPrimaryAssetsProgress = 0.f;
		}
	} LoadingInfo;

	TSharedPtr<FStreamableHandle> StreamableHandlePtr;

protected:
	/** Specify assets loading info */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Prerequisites")
		FAssetsLoadingManagerComponentInfo AssetsLoadingInfo;

protected:
	virtual bool DoLoadAssets();
	virtual bool DoLoadPrimaryAssets();
	virtual bool DoUnloadPrimaryAssets();

protected:
	/** Overridable function which checks loading for availability */
	virtual bool CheckIsLoadingAllowed_Implementation() override;

	/** Overridable function which is invoked on loading start */
	virtual void PrepareLoading_Implementation() override;

	/** Overridable function which starts loading */
	virtual bool DoLoad_Implementation() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Checks does loading manager have objects to load or not */
	virtual bool CheckHasObjectsToLoad_Implementation() override;

	/** Returns loading progress */
	virtual float GetProgress_Implementation() override;

public:
	/**
	* @descr Updates loading info if loading was not started
	* @param LoadingInfo - Assets loading info
	* @return true if the info was updated otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Events")
		bool AssignLoadingInfo(FAssetsLoadingManagerComponentInfo LoadingInfo);
		
};

