// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Processors/Menu/Root/AbstractMenuProcessor.h"
#include "Data/DataTables/Rows/MenuProcessorsTRows.h"
#include "Engine/DataTable.h"
#include "MenuWidgetComponent.generated.h"

/**
* @descr Struct for Widget Component which specifies buttons widgets for openning
* @author problemniy
*/
USTRUCT()
struct FWidgetComponentButtonWidgetStruct {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify button's tag to search under widget */
	UPROPERTY(EditAnywhere)
		EUIElementTag ButtonTag;
	/** Specify widget class to switch */
	UPROPERTY(EditAnywhere)
		TSubclassOf<UGameMenuWidget> Widget;

};

/**
* @descr Widget Component class which can manages menu logic
* @author problemniy
*/
UCLASS(ClassGroup = Components_)
class SOMNIUMCN_API UMenuWidgetComponent : public UWidgetComponent {

	GENERATED_BODY()

public:
	UMenuWidgetComponent();
	
private:
	// Holds previous menu widgets (are used for 'Back' buttons tag)
	UPROPERTY()
		TArray<UGameMenuWidget*> PreviousMenuWidgets;

	// 'UProperty' is needed to prevent cleaning widgets by garbage collector
	UPROPERTY()
		TMap<TSubclassOf<UGameWidget>, UGameWidget*> ReadyWidgets;

	/* --------------------- Action processors */

	// Binded menu processors to validate widget change action by registration data table
	UPROPERTY()
		TArray<UAbstractMenuProcessor*> ValidateWidgetChangeProcessors;

	// Binded menu processors to widget changed action by registration data table
	UPROPERTY()
		TArray<UAbstractMenuProcessor*> WidgetChangedProcessors;

	// Binded menu processors to element pointed action by registration data table
	UPROPERTY()
		TArray<UAbstractMenuProcessor*> ElementPointedProcessors;

	// Binded menu processors to element clicked action by registration data table
	UPROPERTY()
		TArray<UAbstractMenuProcessor*> ElementClickedProcessors;

	// Binded menu processors to menu tick action by registration data table
	UPROPERTY()
		TArray<UAbstractMenuProcessor*> MenuTickProcessors;

private:
	// Called when any widget button will be released
	UFUNCTION()
		void OnButtonReleasedDelegate(UGameButton* GameButton);
	// Called when any widget button will be hovered
	UFUNCTION()
		void OnButtonHoveredDelegate(UGameButton* GameButton);
	// Called when any widget button will be unhovered
	UFUNCTION()
		void OnButtonUnhoveredDelegate(UGameButton* GameButton);

	// Called when line selector of any widget will be clicked
	UFUNCTION()
		void OnSelectorClickedDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag);
	// Called when line selector of any widget will be hovered
	UFUNCTION()
		void OnSelectorHoveredDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag);
	// Called when line selector of any widget will be unhovered
	UFUNCTION()
		void OnSelectorUnhoveredDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag);

	// Called when mouse will be clicked on menu widget
	UFUNCTION()
		void OnWidgetMouseClickedDelegate(UGameMenuWidget* ClickedWidget);

private:
	void PrepareMenuWidgets();
	void PrepareMenuProcessors();
	const EUIElementTag GetActualElementTag(UVisual* UIElement) const;
	void ChangeWidget(UGameMenuWidget* PreviousWidget, UGameMenuWidget* NextWidget, const EUIElementTag ButtonTag);
	bool IsChangeWidgetAllowed(UGameMenuWidget* PreviousWidget, UGameMenuWidget* NextWidget, const EUIElementTag ButtonTag) const;

	// common preparing delegates method
	void PrepareDelegates(UGameMenuWidget* MenuWidget);

	/* --------- concrete preparing delegates methods ---- */
	void PrepareWidgetDelegates(UGameMenuWidget* MenuWidget);
	void PrepareButtonDelegates(UGameMenuWidget* MenuWidget);
	void PrepareLineSelectorDelegates(UGameMenuWidget* MenuWidget);

	// Sorts processor rows by positions and adds them to received array
	void SortAndFillMenuProcessors(const TMap<FMenuProcessorsRegistrationTRow*, UAbstractMenuProcessor*>& AvailableProcessors, TArray<FMenuProcessorsRegistrationTRow*>& RowsToSort, TArray<UAbstractMenuProcessor*>& ArrayToFill);
	
	/* ---------- Invokes menu processors ------- */

	// Invokes menu changed method in registered processors
	void InvokeMenuChangedProcessors(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag);
	// Invokes button pointed method in registered processors
	void InvokeElementPointedProcessors(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered);
	// Invokes button clicked method in registered processors
	void InvokeElementClickedProcessors(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag);

protected:
	/** Specify data table name of menu processors registration (table rows should have 'MenuProcessorsRegistrationTRow' structure) */
	UPROPERTY(EditDefaultsOnly, Category = "Common Configuration", meta = (DisplayName = "Menu Processors DTable"))
		UDataTable* MenuProcessorsRegTable;

	/**
	* Specify widgets to button's tags to switch.
	* Don't use 'Back' tag because it's used for changing to previous widget only (will do by default)
	*/
	UPROPERTY(EditDefaultsOnly, Category = "Common Configuration")
		TArray<FWidgetComponentButtonWidgetStruct> WidgetsToChange;

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
};
