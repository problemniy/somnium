// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidgetComponent.h"
#include "Providers/ServicesProvider.h"
#include "Kismet/KismetSystemLibrary.h"

UMenuWidgetComponent::UMenuWidgetComponent() {
	// Register with the viewport for hardware input from the true mouse and keyboard
	bReceiveHardwareInput = true;
	// Set this component to call TickComponent()
	PrimaryComponentTick.bCanEverTick = true;
	// Set default widget size (full hd)
	DrawSize = FIntPoint(1920, 1080);
}

// Called when the game starts or when spawned
void UMenuWidgetComponent::BeginPlay() {
	Super::BeginPlay();
	PrepareMenuWidgets();
	PrepareMenuProcessors();
}

void UMenuWidgetComponent::PrepareMenuWidgets() {
	APlayerController* Controller = GetWorld() ? GetWorld()->GetFirstPlayerController() : nullptr;
	if (Controller && Widget) {
		UGameMenuWidget* RootWidget = Cast<UGameMenuWidget>(Widget);
		if (RootWidget) {
			ReadyWidgets.Empty();
			TArray<UClass*> AlreadyAddedClasses;
			// create specified widgets
			for (const FWidgetComponentButtonWidgetStruct WidgetStruct : WidgetsToChange) {
				if (WidgetStruct.Widget && !AlreadyAddedClasses.Contains(WidgetStruct.Widget.Get())) {
					AlreadyAddedClasses.Add(WidgetStruct.Widget.Get());
					UGameMenuWidget* MenuWidget = CreateWidget<UGameMenuWidget>(Controller, WidgetStruct.Widget.Get());
					// need to invoke a native construct for widget
					MenuWidget->TakeWidget();
					// prepare buttons delegates for specified widget
					PrepareDelegates(MenuWidget);
					// prepare delegates for sub-widgets
					for (UGameWidget* SubWidget : MenuWidget->GameWidgets) {
						if (SubWidget->IsA(UGameMenuWidget::StaticClass())) {
							PrepareDelegates(Cast<UGameMenuWidget>(SubWidget));
						}
					}
					ReadyWidgets.Add(WidgetStruct.Widget, MenuWidget);
				}
			}
			// prepare buttons delegates for root widget
			TArray<UGameWidget*> ReadyValueWidgets;
			ReadyWidgets.GenerateValueArray(ReadyValueWidgets);
			if (!ReadyValueWidgets.Contains(RootWidget)) {
				// need to invoke a native construct for widget
				RootWidget->TakeWidget();
				// also invoke preparing delegates to component root widget
				PrepareDelegates(RootWidget);
			}
		}
	}
}

const EUIElementTag UMenuWidgetComponent::GetActualElementTag(UVisual* UIElement) const {
	return UKismetSystemLibrary::DoesImplementInterface(UIElement, UUISpecificElement::StaticClass())
		? IUISpecificElement::Execute_GetUITag(UIElement) : EUIElementTag::EMPTY;
}

void UMenuWidgetComponent::PrepareDelegates(UGameMenuWidget* MenuWidget) {
	PrepareWidgetDelegates(MenuWidget);
	PrepareButtonDelegates(MenuWidget);
	PrepareLineSelectorDelegates(MenuWidget);
}

void UMenuWidgetComponent::PrepareWidgetDelegates(UGameMenuWidget* MenuWidget) {
	// widget mouse clicked delegate
	FWidgetMouseClickedDelegate WidgetMouseClickedDelegate;
	WidgetMouseClickedDelegate.BindUFunction(this, FName(TEXT("OnWidgetMouseClickedDelegate")));
	MenuWidget->BindMouseClickedDelegate(WidgetMouseClickedDelegate);
}

void UMenuWidgetComponent::PrepareButtonDelegates(UGameMenuWidget* MenuWidget) {
	for (UGameButton* Button : MenuWidget->GameButtons) {
		FButtonOwnerEventDelegate OnButtonReleasedDelegate, OnButtonHoveredDelegate, OnButtonUnhoveredDelegate;
		// released delegate
		OnButtonReleasedDelegate.BindUFunction(this, FName(TEXT("OnButtonReleasedDelegate")));
		Button->BindOwnerReleasedDelegate(OnButtonReleasedDelegate);
		// hovered delegate
		OnButtonHoveredDelegate.BindUFunction(this, FName(TEXT("OnButtonHoveredDelegate")));
		Button->BindOwnerHoveredDelegate(OnButtonHoveredDelegate);
		// unhovered delegate
		OnButtonUnhoveredDelegate.BindUFunction(this, FName(TEXT("OnButtonUnhoveredDelegate")));
		Button->BindOwnerUnhoveredDelegate(OnButtonUnhoveredDelegate);
	}
}

void UMenuWidgetComponent::PrepareLineSelectorDelegates(UGameMenuWidget* MenuWidget) {
	for (ULineSelector* LineSelector : MenuWidget->LineSelectors) {
		FLineSelectorEventDelegate OnClickedDelegate, OnHoveredDelegate, OnUnhoveredDelegate;
		// on selector clicked delegate
		OnClickedDelegate.BindUFunction(this, FName(TEXT("OnSelectorClickedDelegate")));
		LineSelector->BindClickedDelegate(OnClickedDelegate);
		// on selector hovered delegate
		OnHoveredDelegate.BindUFunction(this, FName(TEXT("OnSelectorHoveredDelegate")));
		LineSelector->BindHoveredDelegate(OnHoveredDelegate);
		// on selector unhovered delegate
		OnUnhoveredDelegate.BindUFunction(this, FName(TEXT("OnSelectorUnhoveredDelegate")));
		LineSelector->BindUnhoveredDelegate(OnUnhoveredDelegate);
	}
}

void UMenuWidgetComponent::PrepareMenuProcessors() {
	// load data table with menu processors registration
	if (MenuProcessorsRegTable && Widget->IsA(UGameMenuWidget::StaticClass())) {
		TArray<FMenuProcessorsRegistrationTRow*> RegRows, ValidateWidgetChangeProcessorsToBind, WidgetChangedProcessorsToBind,
			ElementPointedProcessorsToBind, ElementClickedProcessorsToBind, MenuTickProcessorsToBind;
		MenuProcessorsRegTable->GetAllRows<FMenuProcessorsRegistrationTRow>(FString(), RegRows);
		TMap<FMenuProcessorsRegistrationTRow*, UAbstractMenuProcessor*> AvailableProcessors;
		if (RegRows.Num() > 0) {
			// detect menu processors rows and sort by actions
			for (FMenuProcessorsRegistrationTRow* TableRow : RegRows) {
				UAbstractMenuProcessor* Processor = NewObject<UAbstractMenuProcessor>(TableRow->Processor.GetDefaultObject(), TableRow->Processor.Get());
				AvailableProcessors.Add(TableRow, Processor);
				for (const EUIMenuAction Action : TableRow->Actions) {
					switch (Action) {
					case EUIMenuAction::WIDGET_CHANGED:
						WidgetChangedProcessorsToBind.Add(TableRow);
						break;
					case EUIMenuAction::ELEMENT_POINTED:
						ElementPointedProcessorsToBind.Add(TableRow);
						break;
					case EUIMenuAction::ELEMENT_CLICKED:
						ElementClickedProcessorsToBind.Add(TableRow);
						break;
					case EUIMenuAction::VALIDATE_WIDGET_CHANGE:
						ValidateWidgetChangeProcessorsToBind.Add(TableRow);
						break;
					case EUIMenuAction::MENU_TICK:
						MenuTickProcessorsToBind.Add(TableRow);
						break;
					}
				}
			}
			// sort binded processors by positions
			SortAndFillMenuProcessors(AvailableProcessors, ValidateWidgetChangeProcessorsToBind, ValidateWidgetChangeProcessors);
			SortAndFillMenuProcessors(AvailableProcessors, WidgetChangedProcessorsToBind, WidgetChangedProcessors);
			SortAndFillMenuProcessors(AvailableProcessors, ElementPointedProcessorsToBind, ElementPointedProcessors);
			SortAndFillMenuProcessors(AvailableProcessors, ElementClickedProcessorsToBind, ElementClickedProcessors);
			SortAndFillMenuProcessors(AvailableProcessors, MenuTickProcessorsToBind, MenuTickProcessors);

			// send started widget to menu processors
			InvokeMenuChangedProcessors(nullptr, Cast<UGameMenuWidget>(Widget), EUIElementTag::EMPTY);
		}
	}
}

// Sorts processor rows by positions and adds them to received array
void UMenuWidgetComponent::SortAndFillMenuProcessors(const TMap<FMenuProcessorsRegistrationTRow*, UAbstractMenuProcessor*>& AvailableProcessors, TArray<FMenuProcessorsRegistrationTRow*>& RowsToSort, TArray<UAbstractMenuProcessor*>& ArrayToFill) {
	RowsToSort.Sort([&](const FMenuProcessorsRegistrationTRow& A, const FMenuProcessorsRegistrationTRow& B) {
		return B.Position > A.Position;
	});
	for (FMenuProcessorsRegistrationTRow* TableRow : RowsToSort) {
		UAbstractMenuProcessor* Processor = *AvailableProcessors.Find(TableRow);
		ArrayToFill.Add(Processor);
	}
}

// Invokes menu changed method in registered processors
void UMenuWidgetComponent::InvokeMenuChangedProcessors(UGameMenuWidget* WidgetToClose, UGameMenuWidget* WidgetToOpen, const EUIElementTag ButtonTag) {
	for (UAbstractMenuProcessor* Processor : WidgetChangedProcessors) {
		Processor->WidgetChanged(WidgetToClose, WidgetToOpen, ButtonTag);
	}
}

// Invokes button pointed method in registered processors
void UMenuWidgetComponent::InvokeElementPointedProcessors(UGameMenuWidget* CurrentWidget, UVisual* PointedElement, const EUIElementTag ElementTag, bool bIsHovered) {
	for (UAbstractMenuProcessor* Processor : ElementPointedProcessors) {
		Processor->ElementPointed(CurrentWidget, PointedElement, ElementTag, bIsHovered);
	}
}

// Invokes button clicked method in registered processors
void UMenuWidgetComponent::InvokeElementClickedProcessors(UGameMenuWidget* CurrentWidget, UVisual* ClickedElement, const EUIElementTag ElementTag) {
	for (UAbstractMenuProcessor* Processor : ElementClickedProcessors) {
		Processor->ElementClicked(CurrentWidget, ClickedElement, ElementTag);
	}
}

// Called every frame
void UMenuWidgetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	 // delegates tick to menu tick processors
	for (UAbstractMenuProcessor* MenuProcessors : MenuTickProcessors) {
		MenuProcessors->MenuTick(DeltaTime);
	}
}

// Called when any widget button will be released
void UMenuWidgetComponent::OnButtonReleasedDelegate(UGameButton* GameButton) {
	UGameMenuWidget* PreMenuWidget = Cast<UGameMenuWidget>(Widget);
	// if element is button then send change widget event
	EUIElementTag ButtonTag = IUISpecificElement::Execute_GetUITag(GameButton);
	// prepared widgets to change
	UGameMenuWidget* NextMenuWidget = nullptr;
	bool bBackWasPushed = false;
	// change to specified widget
	if (EUIElementTag::EMPTY != ButtonTag && EUIElementTag::BACK != ButtonTag) {
		for (const FWidgetComponentButtonWidgetStruct WidgetStruct : WidgetsToChange) {
			if (ButtonTag == WidgetStruct.ButtonTag) {
				PreviousMenuWidgets.Add(PreMenuWidget);
				NextMenuWidget = Cast<UGameMenuWidget>(*ReadyWidgets.Find(WidgetStruct.Widget));
				break;
			}
		}
	}
	// change to previous widget if 'Back' tag is detected
	else if (EUIElementTag::BACK == ButtonTag && PreviousMenuWidgets.Num() > 0) {
		bBackWasPushed = true;
		NextMenuWidget = PreviousMenuWidgets.Last();
	}
	// change widget
	if (NextMenuWidget && IsChangeWidgetAllowed(PreMenuWidget, NextMenuWidget, IUISpecificElement::Execute_GetUITag(GameButton))) {
		ChangeWidget(PreMenuWidget, NextMenuWidget, IUISpecificElement::Execute_GetUITag(GameButton));
		if (bBackWasPushed) {
			// remove saved previous widget
			PreviousMenuWidgets.Remove(NextMenuWidget);
		}
	}
	// send click element event
	InvokeElementClickedProcessors(PreMenuWidget, GameButton, IUISpecificElement::Execute_GetUITag(GameButton));
}

bool UMenuWidgetComponent::IsChangeWidgetAllowed(UGameMenuWidget* PreviousWidget, UGameMenuWidget* NextWidget, const EUIElementTag ButtonTag) const {
	for (UAbstractMenuProcessor* Processor : ValidateWidgetChangeProcessors) {
		if (!Processor->ValidateWidgetChange(PreviousWidget, NextWidget, ButtonTag)) {
			return false;
		}
	}
	return true;
}

void UMenuWidgetComponent::ChangeWidget(UGameMenuWidget* PreviousWidget, UGameMenuWidget* NextWidget, const EUIElementTag ButtonTag) {
	SetWidget(NextWidget);
	// send widgets to menu processors
	InvokeMenuChangedProcessors(PreviousWidget, NextWidget, ButtonTag);
}

// Called when any widget button will be hovered
void UMenuWidgetComponent::OnButtonHoveredDelegate(UGameButton* GameButton) {
	InvokeElementPointedProcessors(Cast<UGameMenuWidget>(Widget), GameButton, IUISpecificElement::Execute_GetUITag(GameButton), true);
}

// Called when any widget button will be unhovered
void UMenuWidgetComponent::OnButtonUnhoveredDelegate(UGameButton* GameButton) {
	InvokeElementPointedProcessors(Cast<UGameMenuWidget>(Widget), GameButton, IUISpecificElement::Execute_GetUITag(GameButton), false);
}

// Called when line selector of any widget will be clicked
void UMenuWidgetComponent::OnSelectorClickedDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag) {
	InvokeElementClickedProcessors(Cast<UGameMenuWidget>(Widget), LineSelector, ElementTag);
}

// Called when line selector of any widget will be hovered
void UMenuWidgetComponent::OnSelectorHoveredDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag) {
	InvokeElementPointedProcessors(Cast<UGameMenuWidget>(Widget), LineSelector, ElementTag, true);
}

// Called when line selector of any widget will be unhovered
void UMenuWidgetComponent::OnSelectorUnhoveredDelegate(ULineSelector* LineSelector, const EUIElementTag ElementTag) {
	InvokeElementPointedProcessors(Cast<UGameMenuWidget>(Widget), LineSelector, ElementTag, false);
}

// Called when mouse will be clicked on menu widget
void UMenuWidgetComponent::OnWidgetMouseClickedDelegate(UGameMenuWidget* ClickedWidget) {
	InvokeElementClickedProcessors(Cast<UGameMenuWidget>(Widget), ClickedWidget, GetActualElementTag(ClickedWidget));
}

