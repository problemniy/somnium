// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuGameMode.h"

/**
* Initialize the game.
* The GameMode's InitGame() event is called before any other functions (including PreInitializeComponents() )
* and is used by the GameMode to initialize parameters and spawn its helper classes.
* @warning: this is called before actors' PreInitializeComponents.
*/
void AMainMenuGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) {
	Super::InitGame(MapName, Options, ErrorMessage);

	// We need to disable slate fast widget path for menu because slider mouse capture doesn't work properly with such paths in widget component
	GSlateFastWidgetPath = false;
}

