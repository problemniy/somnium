// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameMods/Root/SomniumCNGameMode.h"
#include "IntroGameMode.generated.h"

/**
* @descr Game mode class destined to 'Intro' level
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API AIntroGameMode : public ASomniumCNGameMode {

	GENERATED_BODY()
	
	
};
