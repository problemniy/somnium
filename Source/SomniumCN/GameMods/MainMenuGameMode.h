// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameMods/Root/SomniumCNGameMode.h"
#include "MainMenuGameMode.generated.h"

/**
* @descr Game mode class destined to 'Main menu' level
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API AMainMenuGameMode : public ASomniumCNGameMode {

	GENERATED_BODY()

public:

	/**
	 * Initialize the game.
	 * The GameMode's InitGame() event is called before any other functions (including PreInitializeComponents() )
	 * and is used by the GameMode to initialize parameters and spawn its helper classes.
	 * @warning: this is called before actors' PreInitializeComponents.
	 */
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage);

};
