// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "SomniumCNGameMode.generated.h"

/**
* @descr The main game mode class
* @author problemniy
*/
UCLASS(ClassGroup = Game_Root_)
class SOMNIUMCN_API ASomniumCNGameMode : public AGameModeBase {

	GENERATED_BODY()

	
};
