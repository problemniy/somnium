// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/PrimaryDataAssets/AbstractLoadingInformation.h"
#include "UMG/Widgets/Abstract/GameLoadingWidget.h"
#include "WorldLoadingInformation.generated.h"

/**
* @descr Primary data asset which represents game World Loading Information
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_API UWorldLoadingInformation : public UAbstractLoadingInformation {

	GENERATED_BODY()

public:
	/** Specify loading widget that should be displayed */
	UPROPERTY(EditDefaultsOnly, meta = (DisplayName = "Loading Widget"))
		TSubclassOf<UGameLoadingWidget> LoadingWidgetClass;

	/** Specify persistent map that should be loaded */
	UPROPERTY(EditDefaultsOnly, Category = "Loading", meta = (AssetBundles = "Persistent Level"))
		TAssetPtr<UWorld> PersistentMap;

	/** Specify streamable levels that should be loaded */
	UPROPERTY(EditDefaultsOnly, Category = "Loading", meta = (AssetBundles = "Streamable Levels"))
		TArray<TAssetPtr<UWorld>> StreamableLevels;

	/** Specify asset bundle names that should be took into account while loading */
	UPROPERTY(EditDefaultsOnly, Category = "Loading")
		TArray<FName> AssetBundleNames;

	/** Specify primary assets ids that should be unloaded */
	UPROPERTY(EditDefaultsOnly, Category = "Unloading", meta = (DisplayName = "Primary Asset Ids To Unload"))
		TArray<FPrimaryAssetId> UnloadPrimaryAssetsList;

	/** Specify streamable levels that should be unloaded */
	UPROPERTY(EditDefaultsOnly, Category = "Unloading", meta = (DisplayName = "Streamable Levels To Unload"))
		TArray<TAssetPtr<UWorld>> UnloadStreamableLevels;

};

