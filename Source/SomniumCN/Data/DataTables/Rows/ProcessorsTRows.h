// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Abstract/Processors/AbstractProcessor.h"
#include "ProcessorsTRows.generated.h"

/**
* @descr Struct (data table row) which represents all processors with position numbers
* @author problemniy
*/
USTRUCT(Blueprintable)
struct SOMNIUMCN_API FProcessorsRegistrationTRow : public FTableRowBase {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify processor's class */
	UPROPERTY(EditDefaultsOnly, Category = "Basic")
		TSubclassOf<UAbstractProcessor> Processor;

	/** Specify a position number to processor */
	UPROPERTY(EditDefaultsOnly, Category = "Basic", meta = (ClampMin = 1))
		int32 Position = 1;

};

