// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataTables/Rows/ProcessorsTRows.h"
#include "Misc/Enums/UserInterfaceEnums.h"
#include "MenuProcessorsTRows.generated.h"

/**
* @descr Struct (data table row) which represents all menu processors with position numbers
* @author problemniy
*/
USTRUCT()
struct SOMNIUMCN_API FMenuProcessorsRegistrationTRow : public FProcessorsRegistrationTRow {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify processor's actions */
	UPROPERTY(EditDefaultsOnly, Category = "Basic")
		TArray<EUIMenuAction> Actions;

};

