// Fill out your copyright notice in the Description page of Project Settings.

#include "GameButton.h"

UGameButton::UGameButton() {
	// specify default button tag
	UITag = EUIElementTag::EMPTY;
}

/** Binds delegate to button's 'OnHovered' event with sending point to button as delegate's parameter */
void UGameButton::BindOwnerHoveredDelegate(const FButtonOwnerEventDelegate& Delegate) {
	OnOwnerHovered.Clear();
	OnHovered.Clear();
	OnOwnerHovered = Delegate;
	FScriptDelegate ScriptDelegate;
	ScriptDelegate.BindUFunction(this, FName(TEXT("OwnHoveredEvent")));
	OnHovered.Add(ScriptDelegate);
}

/** Binds delegate to button's 'OnUnhovered' event with sending point to button as delegate's parameter */
void UGameButton::BindOwnerUnhoveredDelegate(const FButtonOwnerEventDelegate& Delegate) {
	OnOwnerUnhovered.Clear();
	OnUnhovered.Clear();
	OnOwnerUnhovered = Delegate;
	FScriptDelegate ScriptDelegate;
	ScriptDelegate.BindUFunction(this, FName(TEXT("OwnUnhoveredEvent")));
	OnUnhovered.Add(ScriptDelegate);
}

/** Binds delegate to button's 'OnReleased' event with sending point to button as delegate's parameter */
void UGameButton::BindOwnerReleasedDelegate(const FButtonOwnerEventDelegate& Delegate) {
	OnOwnerClicked.Clear();
	OnReleased.Clear();
	OnOwnerClicked = Delegate;
	FScriptDelegate ScriptDelegate;
	ScriptDelegate.BindUFunction(this, FName(TEXT("OwnReleasedEvent")));
	OnReleased.Add(ScriptDelegate);
}

// Called when hovered is detected with sending own object as parameter
void UGameButton::OwnHoveredEvent() {
	OnOwnerHovered.Execute(this);
}

// Called when unhovered is detected with sending own object as parameter
void UGameButton::OwnUnhoveredEvent() {
	OnOwnerUnhovered.Execute(this);
}

// Called when released is detected with sending own object as parameter
void UGameButton::OwnReleasedEvent() {
	OnOwnerClicked.Execute(this);
}

/** Returns specific ui element tag */
EUIElementTag UGameButton::GetUITag_Implementation() {
	return UITag;
}

