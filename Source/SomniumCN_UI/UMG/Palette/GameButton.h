// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Misc/Enums/UserInterfaceEnums.h"
#include "Interfaces/UI/UISpecificElement.h"
#include "GameButton.generated.h"

/**
* @descr Widget class representing Button
* @author problemniy
*/
DECLARE_DYNAMIC_DELEGATE_OneParam(FButtonOwnerEventDelegate, UGameButton*, GameButton);
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameButton : public UButton, public IUISpecificElement {

	GENERATED_BODY()

public:
	UGameButton();

private:
	// Another Hovered and Unhovered delegates wich sends own object as parameter
	FButtonOwnerEventDelegate OnOwnerHovered, OnOwnerUnhovered, OnOwnerClicked;

private:
	// Called when hovered is detected with sending own object as parameter
	UFUNCTION()
		void OwnHoveredEvent();
	// Called when unhovered is detected with sending own object as parameter
	UFUNCTION()
		void OwnUnhoveredEvent();
	// Called when released is detected with sending own object as parameter
	UFUNCTION()
		void OwnReleasedEvent();

protected:
	/** Specify a ui tag for button*/
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Common Configuration", meta = (DisplayName = "UI Tag"))
		EUIElementTag UITag;

public:
	/**
	* @descr Binds delegate to button's 'OnHovered' event with sending point to button as delegate's parameter
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Button Events")
		void BindOwnerHoveredDelegate(const FButtonOwnerEventDelegate& Delegate);
	/**
	* @descr Binds delegate to button's 'OnUnhovered' event with sending point to button as delegate's parameter
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Button Events")
		void BindOwnerUnhoveredDelegate(const FButtonOwnerEventDelegate& Delegate);
	/**
	* @descr Binds delegate to button's 'OnReleased' event with sending point to button as delegate's parameter
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Button Events")
		void BindOwnerReleasedDelegate(const FButtonOwnerEventDelegate& Delegate);

	/**
	* @descr Returns specific ui element tag
	* @return Element tag
	*/
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "UI Element Basic")
		EUIElementTag GetUITag();
		virtual EUIElementTag GetUITag_Implementation() override;
	
};
