// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ProgressBar.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "GameProgressBar.generated.h"

/**
* @descr Widget class representing a Progress Bar
* @author problemniy
*/
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameProgressBar : public UProgressBar {

	GENERATED_BODY()
	
};
