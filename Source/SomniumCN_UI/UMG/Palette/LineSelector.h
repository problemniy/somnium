// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PanelWidget.h"
#include "Interfaces/UI/UISpecificElement.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Misc/Enums/UserInterfaceEnums.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Text/STextBlock.h"
#include "LineSelector.generated.h"

/**
* @descr Structure with button styles which are used by Line Selector
* @author problemniy
*/
USTRUCT(NoExport)
struct SOMNIUMCN_UI_API FLineSelectorButtonStyles {

public:
	/** Specify a style for left arrow button */
	UPROPERTY(EditInstanceOnly)
		FButtonStyle LeftArrowButtonStyle;

	/** Specify a style for middle button */
	UPROPERTY(EditInstanceOnly)
		FButtonStyle MiddleButtonStyle;

	/** Specify a style for right arrow button */
	UPROPERTY(EditInstanceOnly)
		FButtonStyle RightArrowButtonStyle;
};

/**
* @descr Structure wich represents option for Line Selector
* @author problemniy
*/
USTRUCT(BlueprintType)
struct SOMNIUMCN_UI_API FLineSelectorOption {

	GENERATED_USTRUCT_BODY()

public:
	/** Specify an option name */
	UPROPERTY(EditInstanceOnly)
		FText Name;

	/** Specify an option tag */
	UPROPERTY(EditInstanceOnly)
		EUIElementTag UITag;

};

/**
* @descr The panel widget class representing a Line Selector
* @author problemniy
*/
DECLARE_DYNAMIC_DELEGATE_TwoParams(FLineSelectorEventDelegate, ULineSelector*, LineSelector, const EUIElementTag, ElementTag);
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API ULineSelector : public UPanelWidget, public IUISpecificElement {

	GENERATED_BODY()

	ULineSelector();

private:
	// Own event delegates
	FSimpleDelegate OnLeftArrowClickedDelegate, OnOptionClickedDelegate, OnRightArrowClickedDelegate,
		OnLeftArrowHoveredDelegate, OnOptionHoveredDelegate, OnRightArrowHoveredDelegate,
		OnLeftArrowUnhoveredDelegate, OnOptionUnhoveredDelegate, OnRightArrowUnhoveredDelegate;
	// Binded event delegates
	FLineSelectorEventDelegate OnClicked, OnHovered, OnUnhovered;

	// UI element tag of selected option
	EUIElementTag ActualOptionUITag;

private:
	TSharedPtr<SButton> LeftButton, MiddleButton, RightButton;
	TSharedPtr<STextBlock> LeftButtonArrowText, MiddleButtonText, RightButtonArrowText;
	TArray<TPair<EUIElementTag, FText>> NameToTagOptions;

private:
	void FillNameToTagOptions();
	// Specifies default properties to button style
	void AsDefaultButtonStyle(int8 Size, FButtonStyle* ButtonStyle...);
	void CreateDelegates();
	// Returns next option when is clicked (bRightStep determines direction of step)
	const FLineSelectorOption* GetNextOption(const FText& CurrentOptionName, bool bRightStep);
	const EUIElementTag GetCurrentOptionTag() const;

private:
	// Called when user clicked left arrow
	UFUNCTION()
		void OwnLeftArrowClickedEvent();
	// Called when user clicked current option
	UFUNCTION()
		void OwnOptionClickedEvent();
	// Called when user clicked right arrow
	UFUNCTION()
		void OnRightArrowClickedEvent();

	// Called when user hovered on left arrow
	UFUNCTION()
		void OwnLeftArrowHoveredEvent();
	// Called when user hovered on current option
	UFUNCTION()
		void OwnOptionHoveredEvent();
	// Called when user hovered on right arrow
	UFUNCTION()
		void OnRightArrowHoveredEvent();

	// Called when user unhovered on left arrow
	UFUNCTION()
		void OwnLeftArrowUnhoveredEvent();
	// Called when user unhovered on current option
	UFUNCTION()
		void OwnOptionUnhoveredEvent();
	// Called when user unhovered on right arrow
	UFUNCTION()
		void OnRightArrowUnhoveredEvent();

protected:
	virtual TSharedRef<SWidget> RebuildWidget() override;

protected:
	/** 
	* Specify options to line selector.
	* Do not use same names for options!
	*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content", meta = (DisplayName = "Line Options"))
		TArray<FLineSelectorOption> LineSelectorOptions;

	/** Specify the visibility for arrow buttons */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
		bool bArrowButtonsVisible = true;

	/** Specify the width to arrow button */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance", meta = (ClampMin = "5", ClampMax = "100"))
		float ArrowWidth = 30.f;

	/** Specify a color and opacity for labels */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
		FSlateColor TextColorAndOpacity;

	/** Specify a color and opacity for buttons */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
		FSlateColor ButtonColorAndOpacity;

	/** Specify styles to buttons */
	UPROPERTY(EditAnywhere, Category = "Appearance")
		FLineSelectorButtonStyles ButtonStyles;

public:
	/** Adds arrows to selecting button */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content")
		bool bShowArrows;

	/** Specify a font for line selector */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
		FSlateFontInfo Font;

public:
	/**
	* @descr Binds 'OnReleased' event delegate to selector buttons
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Events")
		void BindClickedDelegate(const FLineSelectorEventDelegate& Delegate);

	/**
	* @descr Binds 'OnHovered' event delegate to selector buttons
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Events")
		void BindHoveredDelegate(const FLineSelectorEventDelegate& Delegate);

	/**
	* @descr Binds 'OnUnhovered' event delegate to selector buttons
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Events")
		void BindUnhoveredDelegate(const FLineSelectorEventDelegate& Delegate);

	/**
	* @descr Sets option to selector if it will be found by ui tag
	* @param Delegate - Delegate to bind
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Appearance")
		void SetOptionByUITag(const EUIElementTag UITag);

	/**
	* Dynamically set the font size for left arrow
	* @param Size - The new font size
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Appearance")
		void SetLeftArrowFontSize(const int32 Size);

	/**
	* Dynamically set the font size for option
	* @param Size - The new font size
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Appearance")
		void SetOptionFontSize(const int32 Size);

	/**
	* Dynamically set the font size for right arrow
	* @param Size - The new font size
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Appearance")
		void SetRightArrowFontSize(const int32 Size);

	/**
	* Dynamically set visibility fow row buttons
	* @param bVisibility - true if should be visible otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Line Selector Behavior")
		void SetArrowsVisibility(bool bVisibility);

	/**
	* @descr Returns specific ui element tag
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI Element Basic")
		EUIElementTag GetUITag();
	virtual EUIElementTag GetUITag_Implementation() override;
	
};
