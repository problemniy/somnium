// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Slider.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "GameSlider.generated.h"

/**
* @descr Widget class representing Slider
* @author problemniy
*/
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameSlider : public USlider {

	GENERATED_BODY()
	
};
