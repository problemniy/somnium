// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Image.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Interfaces/UI/UISpecificElement.h"
#include "GameImage.generated.h"

/**
* @descr Widget class representing Image
* @author problemniy
*/
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameImage : public UImage, public IUISpecificElement {

	GENERATED_BODY()

private:
	// Necessary fields for the work of opacity animation
	struct {
		int32 Count;
		float OpacityIterator;
		float PauseIterator;
		bool bBackOpacity;
	} OpacityAnimationStruct;

	// Holds original render opacity value which was before any changes
	float OriginalRenderOpacity;

protected:
	//~ Begin UWidget Interface
	virtual TSharedRef<SWidget> RebuildWidget() override;

protected:

	/** Specify a ui tag for image */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Common Configuration", meta = (DisplayName = "UI Tag"))
		EUIElementTag UITag;

	//----------------- Opacity Animation (method 'MakeOpacityAnimation' must be called to perform animation)

	/** Specify iteration count for animation, count < 0 means is endless, count = 0 means is disabled  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Animations | Opacity Animation")
		int32 OpacityCount;

	/** Specify a temporary delay for image visibility when opacity will be completely filled (equals to 1)  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Animations | Opacity Animation", meta = (ClampMin = 0))
		float OpacityVisibleSecondsDelay;

	/** Specify rate of opacity change while animation is performing */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Animations | Opacity Animation", meta = (ClampMin = 0.0001, ClampMax = 0.1))
		float OpacityChangeRate = 0.005f;

	/** Should image be visible at the end of animation or not  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Animations | Opacity Animation")
		bool bAtTheEndVisible;

public:
	/**
	* @descr Applies opacity animation to image
	* @param DeltaTime - Delta time between frames in seconds
	* @return true if animation is finished (always false if animation is endless or disabled)
	*/
	UFUNCTION(BlueprintCallable, Category = "Image Behavior")
		bool MakeOpacityAnimation(float DeltaTime);

	/**
	* @descr Resets opacity animation. It leads to performing animation again
	*/
	UFUNCTION(BlueprintCallable, Category = "Image Behavior")
		void ResetOpacityAnimation();

	/**
	* @descr Returns specific ui element tag
	* @return Element tag
	*/
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "UI Element Basic")
	EUIElementTag GetUITag();
	virtual EUIElementTag GetUITag_Implementation() override;
	
};

