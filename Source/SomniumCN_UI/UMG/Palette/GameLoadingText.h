// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UMG/Palette/GameText.h"
#include "GameLoadingText.generated.h"

/**
* @base UGameText
* @descr Widget class representing a Loading Static Text
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_UI_API UGameLoadingText : public UGameText {

	GENERATED_BODY()

private:
	UGameLoadingText();

private:
	// Necessary fields for the work of dots animation
	struct {
		FText OriginalText;
		int32 IterationStep;
		float PauseIterator;
	} DotsLoadingAnimationStruct;

private:
	// Adds dots to root text
	void MakeTextWithDots(int32 $DotsCount);

protected:
	//~ Begin UWidget Interface
	virtual TSharedRef<SWidget> RebuildWidget() override;

protected:
	/** Specify delay time between dot's changes */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations | Loading Dot's Animation", meta = (ClampMin = 0))
		float DotsDelayTime = 0.5f;
	/** Specify maximum count of dot's */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations | Loading Dot's Animation", meta = (ClampMin = 0, ClampMax = 3))
		int32 DotsCount = 3;
	/** Specify text to show after loading is complete */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common Configuration")
		FText CompletedLoadingText;
	
public:
	/**
	* @descr Applies dots animation to text
	* @param DeltaTime - Delta time between frames in seconds
	*/
	UFUNCTION(BlueprintCallable, Category = "Text Behavior")
		void ApplyLoadingDotsAnimation(float DeltaTime);
	
	
};
