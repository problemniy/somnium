// Fill out your copyright notice in the Description page of Project Settings.

#include "GameText.h"

/** Applies opacity animation to text */
void UGameText::MakeOpacityAnimation(float DeltaTime, float Iteration /*= 0.005f*/) {
	// appeared opacity
	if (!OpacityAnimationStruct.bBackOpacity) {
		if (OpacityAnimationStruct.OpacityIterator < 1) {
			OpacityAnimationStruct.OpacityIterator += Iteration;
		}
		else if (OpacityAnimationStruct.PauseIterator <= OpacityVisibleSecondsDelay) {
			OpacityAnimationStruct.PauseIterator += DeltaTime;
		}
		else {
			OpacityAnimationStruct.bBackOpacity = true;
		}
	}
	// disappeared opacity
	else if (OpacityAnimationStruct.OpacityIterator > 0) {
		OpacityAnimationStruct.OpacityIterator -= Iteration;
	}
	// endless opacity
	else {
		OpacityAnimationStruct.bBackOpacity = false;
		OpacityAnimationStruct.PauseIterator = 0.f;
		OpacityAnimationStruct.OpacityIterator = 0.f;
	}
	SetRenderOpacity(OpacityAnimationStruct.OpacityIterator);
}

/** Changes text font size */
void UGameText::SetFontSize(const int32 Size) {
	FSlateFontInfo CurrentFont = Font;
	Font.Size = Size;
	SetFont(Font);
}

