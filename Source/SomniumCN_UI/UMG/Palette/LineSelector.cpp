// Fill out your copyright notice in the Description page of Project Settings.

#include "LineSelector.h"
#include "Widgets/SBoxPanel.h"

ULineSelector::ULineSelector() {
	Font.Size = 40;
	ButtonColorAndOpacity = FSlateColor(FLinearColor(0.3f, 0.3f, 0.3f, 1)); // set grey button background color as default
	TextColorAndOpacity = FSlateColor(FLinearColor(1, 1, 1, 1)); // set white text color as default
	AsDefaultButtonStyle(3, &ButtonStyles.LeftArrowButtonStyle, &ButtonStyles.MiddleButtonStyle, &ButtonStyles.RightArrowButtonStyle);
	CreateDelegates();
	bShowArrows = true;
}

// Specifies default properties to button style
void ULineSelector::AsDefaultButtonStyle(int8 Size, FButtonStyle* ButtonStyle...) {
	for (int8 i = 0; i < Size; i++) {
		FButtonStyle& Style = (*(ButtonStyle + i));
		Style.Normal.DrawAs = ESlateBrushDrawType::Box;
		Style.Hovered.DrawAs = ESlateBrushDrawType::Box;
		Style.Pressed.DrawAs = ESlateBrushDrawType::Box;
	}
}

void ULineSelector::CreateDelegates() {
	// clicked
	OnLeftArrowClickedDelegate.BindUFunction(this, FName(TEXT("OwnLeftArrowClickedEvent")));
	OnOptionClickedDelegate.BindUFunction(this, FName(TEXT("OwnOptionClickedEvent")));
	OnRightArrowClickedDelegate.BindUFunction(this, FName(TEXT("OnRightArrowClickedEvent")));
	// hovered
	OnLeftArrowHoveredDelegate.BindUFunction(this, FName(TEXT("OwnLeftArrowHoveredEvent")));
	OnOptionHoveredDelegate.BindUFunction(this, FName(TEXT("OwnOptionHoveredEvent")));
	OnRightArrowHoveredDelegate.BindUFunction(this, FName(TEXT("OnRightArrowHoveredEvent")));
	// unhovered
	OnLeftArrowUnhoveredDelegate.BindUFunction(this, FName(TEXT("OwnLeftArrowUnhoveredEvent")));
	OnOptionUnhoveredDelegate.BindUFunction(this, FName(TEXT("OwnOptionUnhoveredEvent")));
	OnRightArrowUnhoveredDelegate.BindUFunction(this, FName(TEXT("OnRightArrowUnhoveredEvent")));
}

TSharedRef<SWidget> ULineSelector::RebuildWidget() {
	TSharedRef<SWidget> Content = SNew(SHorizontalBox)
		+ SHorizontalBox::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Fill)
		.Padding(FMargin(0, 0, 25.f, 0))
		.FillWidth(0.15f)
		[
			SAssignNew(LeftButton, SButton)
			.HAlign(HAlign_Center)
		    .VAlign(VAlign_Center)
		    .ButtonColorAndOpacity(ButtonColorAndOpacity)
		    .ButtonStyle(&ButtonStyles.LeftArrowButtonStyle)
		    .IsFocusable(true)
		    .OnReleased(OnLeftArrowClickedDelegate)
			.OnHovered(OnLeftArrowHoveredDelegate)
			.OnUnhovered(OnLeftArrowUnhoveredDelegate)
		    .Visibility(bArrowButtonsVisible ? EVisibility::Visible : EVisibility::Hidden)
		    .Content()
		    [
				SAssignNew(LeftButtonArrowText, STextBlock)
				.Text(FText::FromString(bShowArrows ? TEXT("<") : TEXT("")))
		        .Font(Font)
		        .MinDesiredWidth(ArrowWidth)
		        .ColorAndOpacity(TextColorAndOpacity)
			]
		]
	    + SHorizontalBox::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.FillWidth(0.3f).AutoWidth()
		[
			SAssignNew(MiddleButton, SButton)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.ButtonColorAndOpacity(ButtonColorAndOpacity)
			.ButtonStyle(&ButtonStyles.MiddleButtonStyle)
			.IsFocusable(true)
			.OnReleased(OnOptionClickedDelegate)
			.OnHovered(OnOptionHoveredDelegate)
			.OnUnhovered(OnOptionUnhoveredDelegate)
			.Content()
			[
				SAssignNew(MiddleButtonText, STextBlock)
				// the first element always will be as default option
				.Text(LineSelectorOptions.Num() > 0 ? LineSelectorOptions[0].Name : FText::FromName(FName()))
			    .Font(Font)
			    .ColorAndOpacity(TextColorAndOpacity)
			]
		]
		+ SHorizontalBox::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Fill)
		.Padding(FMargin(25.f, 0, 0, 0))
		.FillWidth(0.15f)
		[
			SAssignNew(RightButton, SButton)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.ButtonColorAndOpacity(ButtonColorAndOpacity)
			.ButtonStyle(&ButtonStyles.RightArrowButtonStyle)
			.IsFocusable(true)
			.OnReleased(OnRightArrowClickedDelegate)
			.OnHovered(OnRightArrowHoveredDelegate)
			.OnUnhovered(OnRightArrowUnhoveredDelegate)
			.Visibility(bArrowButtonsVisible ? EVisibility::Visible : EVisibility::Hidden)
			.Content()
			[
				SAssignNew(RightButtonArrowText, STextBlock)
				.Text(FText::FromString(bShowArrows ? TEXT(">") : TEXT("")))
			    .Font(Font)
			    .MinDesiredWidth(ArrowWidth)
			    .ColorAndOpacity(TextColorAndOpacity)
			]
		];
		// fill options map
		FillNameToTagOptions();

		// set background color to buttons
		LeftButton.Get()->SetBorderBackgroundColor(ButtonColorAndOpacity);
		MiddleButton.Get()->SetBorderBackgroundColor(ButtonColorAndOpacity);
		RightButton.Get()->SetBorderBackgroundColor(ButtonColorAndOpacity);

	return Content;
}

void ULineSelector::FillNameToTagOptions() {
	bool bFirstDefaultSet = false;
	for (const FLineSelectorOption& Option : LineSelectorOptions) {
		NameToTagOptions.Add(TPair<EUIElementTag, FText>(Option.UITag, Option.Name));
		// set first value to ActualOptionUITag
		if (!bFirstDefaultSet) {
			ActualOptionUITag = Option.UITag;
			bFirstDefaultSet = true;
		}
	}
}

// Returns next option when is clicked (bRightStep determines direction of step)
const FLineSelectorOption* ULineSelector::GetNextOption(const FText& CurrentOptionName, bool bRightStep) {
	FLineSelectorOption* Result = nullptr;
	if (LineSelectorOptions.Num() == 2) {
		Result = LineSelectorOptions[0].Name.ToString() == CurrentOptionName.ToString() ? &LineSelectorOptions[1] : &LineSelectorOptions[0];
	}
	else if(LineSelectorOptions.Num() > 2) {
		for (int8 i = 0; i < LineSelectorOptions.Num(); i++) {
			int8 next = bRightStep ? (i + 1) == LineSelectorOptions.Num() ? 0 : (i + 1) : (i - 1) < 0 ? LineSelectorOptions.Num() - 1 : (i - 1);
			if (LineSelectorOptions[i].Name.ToString() == CurrentOptionName.ToString()) {
				Result = &LineSelectorOptions[next];
				break;
			}
		}
	}
	return Result;
}

const EUIElementTag ULineSelector::GetCurrentOptionTag() const {
	// try to find current option
	EUIElementTag Result = EUIElementTag::EMPTY;
	const FText CurrentOptionName = MiddleButtonText.Get()->GetText();
	for (TPair<EUIElementTag, FText> OptionPair : NameToTagOptions) {
		if (OptionPair.Value.ToString() == CurrentOptionName.ToString()) {
			Result = OptionPair.Key;
			break;
		}
	}
	return Result;
}

/** Sets option to selector if it will be found by ui tag*/
void ULineSelector::SetOptionByUITag(const EUIElementTag UITag) {
	for (TPair<EUIElementTag, FText> OptionPair : NameToTagOptions) {
		if (OptionPair.Key == UITag) {
			MiddleButtonText.Get()->SetText(OptionPair.Value);
			ActualOptionUITag = OptionPair.Key;
			break;
		}
	}
}

/** Dynamically set the font size for left arrow */
void ULineSelector::SetLeftArrowFontSize(const int32 Size) {
	FSlateFontInfo CurrentInfo = Font;
	CurrentInfo.Size = Size;
	LeftButtonArrowText.Get()->SetFont(CurrentInfo);
}

/** Dynamically set the font size for option */
void ULineSelector::SetOptionFontSize(const int32 Size) {
	FSlateFontInfo CurrentInfo = Font;
	CurrentInfo.Size = Size;
	MiddleButtonText.Get()->SetFont(CurrentInfo);
}

/** Dynamically set the font size for right arrow */
void ULineSelector::SetRightArrowFontSize(const int32 Size) {
	FSlateFontInfo CurrentInfo = Font;
	CurrentInfo.Size = Size;
	RightButtonArrowText.Get()->SetFont(CurrentInfo);
}

/** Dynamically set visibility fow row buttons */
void ULineSelector::SetArrowsVisibility(bool bVisibility) {
	LeftButton.Get()->SetVisibility(bVisibility ? EVisibility::Visible : EVisibility::Hidden);
	RightButton.Get()->SetVisibility(bVisibility ? EVisibility::Visible : EVisibility::Hidden);
}

/*---------- Delegates ---*/

// Called when user clicked on left arrow
void ULineSelector::OwnLeftArrowClickedEvent() {
	// try to find next option
	const FLineSelectorOption* NextOption = GetNextOption(MiddleButtonText.Get()->GetText(), false);
	if (NextOption) {
		MiddleButtonText.Get()->SetText(NextOption->Name);
	}
	ActualOptionUITag = GetCurrentOptionTag();
	// send event to delegate
	if (OnClicked.IsBound()) {
		OnClicked.Execute(this, EUIElementTag::LEFT_ARROW);
	}
}

// Called when user clicked on current option
void ULineSelector::OwnOptionClickedEvent() {
	if (OnClicked.IsBound()) {
		// send event to delegate if current option was found
		OnClicked.Execute(this, ActualOptionUITag);
	}
}

// Called when user clicked on right arrow
void ULineSelector::OnRightArrowClickedEvent() {
	// try to find next option
	const FLineSelectorOption* NextOption = GetNextOption(MiddleButtonText.Get()->GetText(), true);
	if (NextOption) {
		MiddleButtonText.Get()->SetText(NextOption->Name);
	}
	ActualOptionUITag = GetCurrentOptionTag();
	// send event to delegate
	if (OnClicked.IsBound()) {
		OnClicked.Execute(this, EUIElementTag::RIGHT_ARROW);
	}
}

// Called when user hovered on left arrow
void ULineSelector::OwnLeftArrowHoveredEvent() {
	// send event to delegate
	if (OnHovered.IsBound()) {
		OnHovered.Execute(this, EUIElementTag::LEFT_ARROW);
	}
}

// Called when user hovered on current option
void ULineSelector::OwnOptionHoveredEvent() {
	if (OnHovered.IsBound()) {
		const EUIElementTag CurrentOtionTag = GetCurrentOptionTag();
		// send event to delegate if current option was found
		OnHovered.Execute(this, CurrentOtionTag);
	}
}

// Called when user hovered on right arrow
void ULineSelector::OnRightArrowHoveredEvent() {
	// send event to delegate
	if (OnHovered.IsBound()) {
		OnHovered.Execute(this, EUIElementTag::RIGHT_ARROW);
	}
}

// Called when user unhovered on left arrow
void ULineSelector::OwnLeftArrowUnhoveredEvent() {
	// send event to delegate
	if (OnUnhovered.IsBound()) {
		OnUnhovered.Execute(this, EUIElementTag::LEFT_ARROW);
	}
}

// Called when user unhovered on current option
void ULineSelector::OwnOptionUnhoveredEvent() {
	if (OnUnhovered.IsBound()) {
		const EUIElementTag CurrentOtionTag = GetCurrentOptionTag();
		// send event to delegate if current option was found
		OnUnhovered.Execute(this, ActualOptionUITag);
	}
}

// Called when user unhovered on right arrow
void ULineSelector::OnRightArrowUnhoveredEvent() {
	// send event to delegate
	if (OnUnhovered.IsBound()) {
		OnUnhovered.Execute(this, EUIElementTag::RIGHT_ARROW);
	}
}

/** Binds 'OnReleased' event delegate to selector buttons */
void ULineSelector::BindClickedDelegate(const FLineSelectorEventDelegate& Delegate) {
	OnClicked.Clear();
	OnClicked = Delegate;
}

/** Binds 'OnHovered' event delegate to selector buttons */
void ULineSelector::BindHoveredDelegate(const FLineSelectorEventDelegate& Delegate) {
	OnHovered.Clear();
	OnHovered = Delegate;
}

/** Binds 'OnUnhovered' event delegate to selector buttons */
void ULineSelector::BindUnhoveredDelegate(const FLineSelectorEventDelegate& Delegate) {
	OnUnhovered.Clear();
	OnUnhovered = Delegate;
}

/** Returns specific ui element tag */
EUIElementTag ULineSelector::GetUITag_Implementation() {
	return ActualOptionUITag;
}

