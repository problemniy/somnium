// Fill out your copyright notice in the Description page of Project Settings.

#include "GameImage.h"

//~ Begin UWidget Interface
TSharedRef<SWidget> UGameImage::RebuildWidget() {
	OpacityAnimationStruct.Count = OpacityCount;
	OriginalRenderOpacity = RenderOpacity;
	return Super::RebuildWidget();
}

/** Applies opacity animation to image */
bool UGameImage::MakeOpacityAnimation(float DeltaTime) {
	if (OpacityCount == 0) return true;
	float ActualOpacity = GetRenderOpacity();

	if (!OpacityAnimationStruct.bBackOpacity) {
		// make appearance
		if (ActualOpacity < 1) {
			ActualOpacity += OpacityChangeRate;
		}
		// make pause when image is full visible
		else if (OpacityAnimationStruct.PauseIterator <= OpacityVisibleSecondsDelay) {
			OpacityAnimationStruct.PauseIterator += DeltaTime;
		}
		else if (OpacityAnimationStruct.Count > 1 || !bAtTheEndVisible && OpacityAnimationStruct.Count > 0 || OpacityCount < 0) {
			OpacityAnimationStruct.bBackOpacity = true;
		}
		// bAtTheEndVisible = true therefore animation should be finished
		else {
			OpacityAnimationStruct.PauseIterator = 0.f;
			ActualOpacity = 0.f;
			return true;
		}
	}
	// make disappearance
	else if (ActualOpacity > 0) {
		ActualOpacity -= OpacityChangeRate;
	}
	// endless opacity
	else if (OpacityCount < 0 || OpacityAnimationStruct.Count > 0) {
		OpacityAnimationStruct.bBackOpacity = OpacityCount < 0 ? false : !((OpacityAnimationStruct.Count - 1) > 0);
		OpacityAnimationStruct.PauseIterator = 0.f;
		ActualOpacity = 0.f;
		if (OpacityAnimationStruct.Count > 0) {
			OpacityAnimationStruct.Count--;
		}
	}
	// finish opacity animation
	else {
		OpacityAnimationStruct.PauseIterator = 0.f;
		ActualOpacity = 0.f;
		return true;
	}

	SetRenderOpacity(ActualOpacity);
	return false;
}

/** Returns specific ui element tag */
EUIElementTag UGameImage::GetUITag_Implementation() {
	return UITag;
}

/** Resets opacity animation. It leads to performing animation again */
void UGameImage::ResetOpacityAnimation() {
	SetRenderOpacity(OriginalRenderOpacity);

	OpacityAnimationStruct.Count = OpacityCount;
	OpacityAnimationStruct.PauseIterator = 0.f;
	OpacityAnimationStruct.bBackOpacity = false;
}

