// Fill out your copyright notice in the Description page of Project Settings.

#include "GameLoadingText.h"

UGameLoadingText::UGameLoadingText() {
	CompletedLoadingText = FText::FromString(TEXT("Press any key to continue"));
}

//~ Begin UWidget Interface
TSharedRef<SWidget> UGameLoadingText::RebuildWidget() {
	DotsLoadingAnimationStruct.OriginalText = GetText();
	return Super::RebuildWidget();
}

// Adds dots to root text
void UGameLoadingText::MakeTextWithDots(int32 $DotsCount) {
	FString DotsText;
	for (int32 i = 0; i < $DotsCount; i++) {
		DotsText.Append(TEXT("."));
	}
	FString OriginalText = FString(DotsLoadingAnimationStruct.OriginalText.ToString());
	FString NewText = OriginalText.Append(DotsText);
	SetText(FText::FromString(NewText));
}

/** Applies dots animation to text */
void UGameLoadingText::ApplyLoadingDotsAnimation(float DeltaTime) {
	MakeTextWithDots(DotsLoadingAnimationStruct.IterationStep);
	if (DotsLoadingAnimationStruct.PauseIterator < DotsDelayTime) {
		DotsLoadingAnimationStruct.PauseIterator += DeltaTime;
	}
	else {
		DotsLoadingAnimationStruct.PauseIterator = 0.f;
		if (DotsLoadingAnimationStruct.IterationStep++ >= DotsCount) {
			DotsLoadingAnimationStruct.IterationStep = 0;
		}
	}
}



