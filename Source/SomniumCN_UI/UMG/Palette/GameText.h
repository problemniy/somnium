// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TextBlock.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "GameText.generated.h"

/**
* @descr Widget class representing a Static Text
* @author problemniy
*/
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameText : public UTextBlock {

	GENERATED_BODY()
	
private:
	// Necessary fields for the work of opacity animation
	struct {
		float OpacityIterator;
		float PauseIterator;
		bool bBackOpacity;
	} OpacityAnimationStruct;

protected:
	/** Specify a temporary delay for image visibility when opacity will be completely filled (equals to 1)  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations | Opacity Animation", meta = (ClampMin = 0))
		float OpacityVisibleSecondsDelay;

public:
	/**
	* @descr Applies opacity animation to text
	* @param DeltaTime - Delta time between frames in seconds
	* @param Iteration - Controls animation speed
	*/
	UFUNCTION(BlueprintCallable, Category = "Text Behavior")
		void MakeOpacityAnimation(float DeltaTime, float Iteration = 0.005f);

	/**
	* @descr Changes text font size
	* @param Size - Font size
	*/
	UFUNCTION(BlueprintCallable, Category = "Text Transform")
		void SetFontSize(const int32 Size);
	
};
