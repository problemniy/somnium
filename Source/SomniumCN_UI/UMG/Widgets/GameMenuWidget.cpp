// Fill out your copyright notice in the Description page of Project Settings.

#include "GameMenuWidget.h"
#include "Layout/WidgetPath.h"

/**
* The system calls this method to notify the widget that a mouse button was pressed within it. This event is bubbled.
*
* @param MyGeometry The Geometry of the widget receiving the event
* @param MouseEvent Information about the input event
* @return Whether the event was handled along with possible requests for the system to take action.
 */
FReply UGameMenuWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) {
	FReply Result = Super::NativeOnPreviewMouseButtonDown(InGeometry, InMouseEvent);
	SWidget& LastWidget = InMouseEvent.GetEventPath()->GetLastWidget().Get();
	FName ParentType;
	if (LastWidget.GetParentWidget().IsValid()) {
		ParentType = LastWidget.GetParentWidget().Get()->GetType();
	}
	// invoke mouse clicked delegate if button was not clicked (checked by parent of last clicked widget)
	if (MouseClickedDelegate.IsBound() && FName(TEXT("SButton")) != ParentType) {
		MouseClickedDelegate.Execute(this);
	}
	return Result;
}

/**
* @descr Binds delegate to mouse clicked events on widget
* @param Handler - Reference to delegate
*/
void UGameMenuWidget::BindMouseClickedDelegate(const FWidgetMouseClickedDelegate& Delegate) {
	if (MouseClickedDelegate.IsBound()) {
		MouseClickedDelegate.Clear();
	}
	MouseClickedDelegate = Delegate;
}

/** Returns specific ui element tag */
EUIElementTag UGameMenuWidget::GetUITag_Implementation() {
	return UITag;
}
