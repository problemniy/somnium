// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "UMG/Palette/GameButton.h"
#include "UMG/Palette/GameText.h"
#include "UMG/Palette/GameImage.h"
#include "UMG/Palette/LineSelector.h"
#include "GameWidget.generated.h"

/**
* @descr Game class representing User Widget
* @author problemniy
*/
UCLASS(ClassGroup = User_Interface_)
class SOMNIUMCN_UI_API UGameWidget : public UUserWidget {

	GENERATED_BODY()

private:
	// Searches game widget components under widget and fills arrays
	void SpecifyGameWidgetComponents();

public:
	/** Available game buttons under widget */
	UPROPERTY(BlueprintReadOnly, Category = "Widget Data")
		TArray<UGameButton*> GameButtons;
	/** Available game texts under widget */
	UPROPERTY(BlueprintReadOnly, Category = "Widget Data")
		TArray<UGameText*> GameTexts;
	/** Available game images under widget */
	UPROPERTY(BlueprintReadOnly, Category = "Widget Data")
		TArray<UGameImage*> GameImages;
	/** Available game user widgets under current widget */
	UPROPERTY(BlueprintReadOnly, Category = "Widget Data")
		TArray<UGameWidget*> GameWidgets;
	/** Available line selectors under current widget */
	UPROPERTY(BlueprintReadOnly, Category = "Widget Data")
		TArray<ULineSelector*> LineSelectors;

protected:
	virtual void NativeConstruct() override;
	virtual TSharedRef<SWidget> RebuildWidget() override;

};
