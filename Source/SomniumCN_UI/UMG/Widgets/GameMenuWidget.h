// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UMG/Widgets/GameWidget.h"
#include "Interfaces/UI/UISpecificElement.h"
#include "GameMenuWidget.generated.h"

/**
* @base UGameWidget
* @descr Game class representing Menu Widget
* @author problemniy
*/
DECLARE_DYNAMIC_DELEGATE_OneParam(FWidgetMouseClickedDelegate, UGameWidget*, GameWidget);
UCLASS()
class SOMNIUMCN_UI_API UGameMenuWidget : public UGameWidget, public IUISpecificElement {

	GENERATED_BODY()

private:
	FWidgetMouseClickedDelegate MouseClickedDelegate;

protected:
	/** Specify a ui tag for widget */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Common Configuration", meta = (DisplayName = "UI Tag"))
		EUIElementTag UITag;

protected:
	/**
	* The system calls this method to notify the widget that a mouse button was pressed within it. This event is bubbled.
	*
	* @param MyGeometry The Geometry of the widget receiving the event
	* @param MouseEvent Information about the input event
	* @return Whether the event was handled along with possible requests for the system to take action.
	*/
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;


public:
	/**
	* @descr Returns specific ui element tag
	* @return Element tag
	*/
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "UI Element Basic")
		EUIElementTag GetUITag();
	virtual EUIElementTag GetUITag_Implementation() override;

	/**
	* @descr Binds delegate to mouse clicked events on widget
	* @param Handler - Reference to delegate
	*/
	void BindMouseClickedDelegate(const FWidgetMouseClickedDelegate& Delegate);
	
};
