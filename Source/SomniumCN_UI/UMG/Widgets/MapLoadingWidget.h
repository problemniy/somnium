// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UMG/Widgets/Abstract/GameLoadingWidget.h"
#include "MapLoadingWidget.generated.h"

/**
* @descr Game class representing Map Loading Widget
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_UI_API UMapLoadingWidget : public UGameLoadingWidget {

	GENERATED_BODY()
	
};

