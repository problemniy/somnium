// Fill out your copyright notice in the Description page of Project Settings.

#include "GameWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Widget.h"

void UGameWidget::NativeConstruct() {
	Super::NativeConstruct();
}

TSharedRef<SWidget> UGameWidget::RebuildWidget() {
	TSharedRef<SWidget> Result = Super::RebuildWidget();
	SpecifyGameWidgetComponents();
	return Result;
}

// Searches game widget components under widget and fills arrays
void UGameWidget::SpecifyGameWidgetComponents() {
	// clear arrays before
	GameButtons.Empty();
	GameTexts.Empty();
	GameImages.Empty();
	// specify arrays of components
	WidgetTree->ForEachWidget([&](UWidget* Widget) {
		check(Widget);
		if (Widget->IsA(UGameButton::StaticClass())) {
			GameButtons.Add(Cast<UGameButton>(Widget));
		}
		else if (Widget->IsA(UGameText::StaticClass())) {
			GameTexts.Add(Cast<UGameText>(Widget));
		}
		else if (Widget->IsA(UGameImage::StaticClass())) {
			GameImages.Add(Cast<UGameImage>(Widget));
		}
		else if (Widget->IsA(UGameWidget::StaticClass())) {
			GameWidgets.Add(Cast<UGameWidget>(Widget));
		}
		else if (Widget->IsA(ULineSelector::StaticClass())) {
			LineSelectors.Add(Cast<ULineSelector>(Widget));
		}
	});
}

