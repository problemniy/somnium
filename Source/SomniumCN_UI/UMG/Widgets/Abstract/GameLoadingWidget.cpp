// Fill out your copyright notice in the Description page of Project Settings.

#include "GameLoadingWidget.h"

void UGameLoadingWidget::NativeConstruct() {
	Super::NativeConstruct();
}

void UGameLoadingWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) {
	Super::NativeTick(MyGeometry, InDeltaTime);
}

/** Provides invoking the tick function to external objects */
void UGameLoadingWidget::OnExternalTick_Implementation(float DeltaTime) {
	// unimplemented();
}

/** Overridable function which determines ready to close widget condition */
bool UGameLoadingWidget::IsReady_Implementation() {
	return IsLoadingInProgress();
}

/** @descr Sets progress value to loading progress bar */
void UGameLoadingWidget::SetLoadingProgress(float Progress) {
	LoadingProgressBar->SetPercent(Progress);
}

/** Checks loading for active progress */
bool UGameLoadingWidget::IsLoadingInProgress() {
	return !bStopAction && LoadingProgressBar->bIsEnabled && LoadingProgressBar->Percent < 1;
}

