// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UMG/Widgets/GameWidget.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "UMG/Palette/GameProgressBar.h"
#include "GameLoadingWidget.generated.h"

/**
* @base UGameWidget
* @descr Game class representing Loading Widget
* @author problemniy
*/
UCLASS(Abstract)
class SOMNIUMCN_UI_API UGameLoadingWidget : public UGameWidget {

	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
		UGameProgressBar* LoadingProgressBar;

protected:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	/**
	* @descr Overridable function which provides invoking the tick function to external objects
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game Loading Management")
		void OnExternalTick(float DeltaTime);

	/**
	* @descr Overridable function which determines ready to close widget condition
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game Loading Management")
		bool IsReady();

public:
	/**
	* @descr Sets progress value to loading progress bar
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Loading Management")
		void SetLoadingProgress(float Progress);

	/**
	* @descr Checks loading for active progress
	* @return true if progress is active otherwise false
	*/
	UFUNCTION(BlueprintPure, Category = "Game Loading Management")
		bool IsLoadingInProgress();

};

