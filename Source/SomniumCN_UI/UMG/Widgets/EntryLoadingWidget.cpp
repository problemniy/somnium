// Fill out your copyright notice in the Description page of Project Settings.


#include "EntryLoadingWidget.h"

void UEntryLoadingWidget::NativeConstruct() {
	Super::NativeConstruct();
	GamePlayerController = Cast<AGamePlayerController>(GetWorld()->GetFirstPlayerController());
	PrepareAnyPressedActionEventDelegate();
}

void UEntryLoadingWidget::NativeDestruct() {
	Super::NativeDestruct();
	ReleaseAnyPressedActionEventDelegate();
}

void UEntryLoadingWidget::PrepareAnyPressedActionEventDelegate() {
	if (IsValid(GamePlayerController)) {
		FGamePlayerControllerActionDelegate Delegate;
		Delegate.BindDynamic(this, &UEntryLoadingWidget::OnAnyPressedAction);
		GamePlayerController->BindActionEventDelegate(EInputActionEvent::ANY_PRESSED, EInputEvent::IE_Released, Delegate);
	}
}

void UEntryLoadingWidget::ReleaseAnyPressedActionEventDelegate() {
	GamePlayerController->RemoveActionEventDelegate(EInputActionEvent::ANY_PRESSED);
}

/** Overridable function which determines ready to close widget condition */
bool UEntryLoadingWidget::IsReady_Implementation() {
	return bIsReady;
}

// Called on 'Any Pressed' action
void UEntryLoadingWidget::OnAnyPressedAction() {
	bIsReady = LoadingProgressBar->Percent >= 1.f;
	if (bIsReady) {
		RemoveFromParent();
		ReleaseAnyPressedActionEventDelegate();
		// enable mouse cursor
		if (IsValid(GamePlayerController)) {
			GamePlayerController->bShowMouseCursor = true;
			FInputModeUIOnly InputMode;
			InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			GamePlayerController->SetInputMode(InputMode);
		}
	}
}

