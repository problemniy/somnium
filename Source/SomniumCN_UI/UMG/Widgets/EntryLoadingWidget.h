// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UMG/Widgets/Abstract/GameLoadingWidget.h"
#include "EngineData/Controllers/GamePlayerController.h"
#include "EntryLoadingWidget.generated.h"

/**
* @descr Game class representing Entry Loading Widget
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_UI_API UEntryLoadingWidget : public UGameLoadingWidget {

	GENERATED_BODY()

private:
	UPROPERTY()
		AGamePlayerController* GamePlayerController;

private:
	bool bIsReady;

private:
	void PrepareAnyPressedActionEventDelegate();
	void ReleaseAnyPressedActionEventDelegate();

protected:
	/** Overridable function which determines ready to close widget condition */
	virtual bool IsReady_Implementation() override;

protected:
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;

	// Called on 'Any Pressed' action
	UFUNCTION(Category = "Delegates")
		void OnAnyPressedAction();
	
};

