// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SomniumCNTarget : TargetRules
{
	public SomniumCNTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "SomniumCN", "SomniumCN_UI", "SomniumCN_Management", "SomniumCN_Registry" } );
	}
}
