// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SomniumCNEditorTarget : TargetRules
{
	public SomniumCNEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "SomniumCN", "SomniumCN_UI", "SomniumCN_Management", "SomniumCN_Registry" } );
	}
}
