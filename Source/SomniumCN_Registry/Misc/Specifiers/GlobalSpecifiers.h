// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
* @descr Static values for Unreal specifiers
* @author problemniy
*/

/** -------- Class Groups -------- */

#define Game_Root_ "Game Root";
#define Manager_Actors_ "Manager Actors";
#define Player_Actors_ "Player Actors";
#define Static_Actors_ "Static Actors";
#define Components_ "Components";
#define Constants_ "Constants";
#define Services_ "Services";
#define Processors_ "Processors";
#define Utility_Tools_ "Utility Tools";
#define User_Interface_ "User Interface";
#define Transfer_Data_ "Transfer Data";
#define Save_Game_Data_ "Save Game Data";
#define Primary_Data_Asset_ "Primary Data Asset"

/** -------- Asset Bundles -------- */

#define Mandatory_ "Mandatory"