// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameWorldEnums.generated.h"

/**
* @descr Enums which represents groups of levels
* @author problemniy
*/
UENUM(BlueprintType)
enum class EGameLevelTag : uint8 {
	INTRO UMETA(DisplayName = "Intro"),
	MENU UMETA(DisplayName = "Menu")
};

/**
* @descr Enums which represents asset types
* @author problemniy
*/
UENUM(BlueprintType)
enum class EAssetType : uint8 {
	MAPS UMETA(DisplayName = "Maps"),
	FONTS UMETA(DisplayName = "Fonts"),
	WIDGETS UMETA(DisplayName = "Widgets"),
	DTABLES UMETA(DisplayName = "Data Tables")
};

