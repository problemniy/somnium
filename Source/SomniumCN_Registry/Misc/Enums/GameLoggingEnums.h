// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameLoggingEnums.generated.h"

/**
* @descr Enums which represents log group specifiers
* @author problemniy
*/
UENUM(meta = (ScriptName = "LogsGroup"))
enum class ELogsGroup : uint8 {
	LOADING_ASSETS UMETA(DisplayName = "Loading Assets")
};

/**
* @descr Enums which represents log levels for game
* @author problemniy
*/
UENUM(meta = (ScriptName = "LogsGroup"))
enum class ELogsLevel : uint8 {
	ERROR UMETA(DisplayName = "Error"),
	WARNING UMETA(DisplayName = "Warning"),
	INFO UMETA(DisplayName = "Info"),
	DEBUG UMETA(DisplayName = "Debug"),
	FULL UMETA(DisplayName = "Full")
};

