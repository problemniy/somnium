// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UserInterfaceEnums.generated.h"

/**
* @descr Enums which represents ui element tags
* @author problemniy
*/
UENUM(BlueprintType, meta = (ScriptName = "UI Element Tag"))
enum class EUIElementTag : uint8 {
	EMPTY UMETA(DisplayName = "Empty"),

	// common
	SETTINGS UMETA(DisplayName = "Settings"),

	// widgets
	CONFIRM_SCHANGES UMETA(DisplayName = "Confirm Changes (Settings)"),
	CHECK_BRIGHTNESS UMETA(DisplayName = "Check Brightness"),
	CONFIRM_START UMETA(DisplayName = "Confirm Start"),

	// buttons
	CONTINUE UMETA(DisplayName = "Continue"),
	NEW_GAME UMETA(DisplayName = "New Game"),
	CHAPTER UMETA(DisplayName = "Chapter Selection"),
	SLOT_1 UMETA(DisplayName = "Slot 1"),
	SLOT_2 UMETA(DisplayName = "Slot 2"),
	SLOT_3 UMETA(DisplayName = "Slot 3"),
	BACK UMETA(DisplayName = "Back"),
	NEXT UMETA(DisplayName = "Next"),
	GO UMETA(DisplayName = "Go"),
	EXIT UMETA(DisplayName = "Exit"),
	CONFIRM_SCHANGES_YES UMETA(DisplayName = "Confirm Changes (Settings): Yes"),
	CONFIRM_SCHANGES_NO UMETA(DisplayName = "Confirm Changes (Settings): No"),

	// images
	OVERLAP_IMG UMETA(DisplayName = "Overlap (Image)"),

	// line selectors
	LEFT_ARROW UMETA(DisplayName = "Left Arrow"),
	RIGHT_ARROW UMETA(DisplayName = "Right Arrow"),
	CONTROLLER_YES UMETA(DisplayName = "Controller: Yes"),
	CONTROLLER_NO UMETA(DisplayName = "Controller: No"),
	VSYNC_YES UMETA(DisplayName = "VSync: Yes"),
	VSYNC_NO UMETA(DisplayName = "VSync: No"),
	FSCREEN_YES UMETA(DisplayName = "FScreen: Yes"),
	FSCREEN_NO UMETA(DisplayName = "FScreen: No"),
	RES_1680_1050 UMETA(DisplayName = "Resolution: 1680x1050"),
	RES_1920_1080 UMETA(DisplayName = "Resolution: 1920x1080"),
	QUALITY_HIGH UMETA(DisplayName = "Quality: High"),
	QUALITY_LOW UMETA(DisplayName = "Quality: Low"),
	LANGUAGE_ENG UMETA(DisplayName = "Language: English"),
	LANGUAGE_RUS UMETA(DisplayName = "Language: Russian")
};

/**
* @descr Enums which represents menu action
* @author problemniy
*/
UENUM(BlueprintType, meta = (ScriptName = "UIMenuAction"))
enum class EUIMenuAction : uint8 {
	EMPTY UMETA(DisplayName = "Empty"),
	VALIDATE_WIDGET_CHANGE UMETA(DisplayName = "Validate Widget Change"),
	WIDGET_CHANGED UMETA(DisplayName = "Widget Changed"),
	ELEMENT_POINTED UMETA(DisplayName = "Element Pointed"),
	ELEMENT_CLICKED UMETA(DisplayName = "Element Clicked"),
	MENU_TICK UMETA(DisplayName = "Menu Tick")
};

