// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameSaveEnums.generated.h"

/**
* @descr Enums which represents save slot identities
* @author problemniy
*/
UENUM(meta = (ScriptName = "GameSlotIdentity"))
enum class EGameSlotIdentity : uint8 {
	SLOT_1 UMETA(DisplayName = "Slot 1"),
	SLOT_2 UMETA(DisplayName = "Slot 2"),
	SLOT_3 UMETA(DisplayName = "Slot 3")
};

/**
* @descr Enums which represents game save data identities
* @author problemniy
*/
UENUM(meta = (ScriptName = "GameSaveDataIdentity"))
enum class EGameSaveDataIdentity : uint8 {
	GAME_SETTINGS UMETA(DisplayName = "Game Settings")
};
