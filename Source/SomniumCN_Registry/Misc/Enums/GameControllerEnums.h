// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameControllerEnums.generated.h"

/**
* @descr Enums which represents action events of player controller
* @author problemniy
*/
UENUM(meta = (ScriptName = "InputACtionEventType"))
enum class EInputActionEvent : uint8 {
	SKIP_INTRO UMETA(DisplayName = "Skip Intro"),
	ANY_PRESSED UMETA(DisplayName = "Any Pressed")
};
