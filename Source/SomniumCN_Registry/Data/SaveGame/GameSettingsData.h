// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Abstract/SaveGame/AbstractSaveGameData.h"
#include "Misc/Enums/UserInterfaceEnums.h"
#include "GameSettingsData.generated.h"

/**
* @descr Class to save game settings
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UGameSettingsData : public UAbstractSaveGameData {

	GENERATED_BODY()

	UGameSettingsData();

public:
	/** Is controller enabled  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag ControllerEnabled;

	/** Is vertical synchronization enabled  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag VerticalSync;

	/** Is full screen enabled  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag FullScreen;

	/** Specified resolution  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag Resolution;

	/** Specified quality */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag Quality;

	/** Specified language */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		EUIElementTag Language;

	/** Specified brightness */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Data")
		float Brightness;

public:
	/**
	* @descr Checks if the data values match the incoming data
	* @param DataToCheck - Reference to data object to check
	* @return true if the data matches otherwise false
	*/
	UFUNCTION(BlueprintPure, Category = "Game Settings Data")
		bool AreDataEqualsTo(const UGameSettingsData* DataToCheck);
	
};
