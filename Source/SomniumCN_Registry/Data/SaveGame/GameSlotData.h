// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Abstract/SaveGame/AbstractSaveGameData.h"
#include "GameSlotData.generated.h"

/**
* @descr Class to save a game which represents Game Slot Data
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UGameSlotData : public UAbstractSaveGameData {

	GENERATED_BODY()

public:
	/** Date of last save (or first creation) */
	UPROPERTY(VisibleAnywhere, Category = "Basic")
		FDateTime SaveDate;
	
};
