// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSettingsData.h"

UGameSettingsData::UGameSettingsData() {
	// set default values
	ControllerEnabled = EUIElementTag::CONTROLLER_NO;
	VerticalSync = EUIElementTag::VSYNC_NO;
	FullScreen = EUIElementTag::FSCREEN_YES;
	Resolution = EUIElementTag::RES_1920_1080;
	Quality = EUIElementTag::QUALITY_LOW;
	Language = EUIElementTag::LANGUAGE_ENG;
	Brightness = 0.4f;
}

/** Checks if the data values match the incoming data */
bool UGameSettingsData::AreDataEqualsTo(const UGameSettingsData* DataToCheck) {
	return this->ControllerEnabled == DataToCheck->ControllerEnabled
		&& this->VerticalSync == DataToCheck->VerticalSync
		&& this->FullScreen == DataToCheck->FullScreen
		&& this->Resolution == DataToCheck->Resolution
		&& this->Quality == DataToCheck->Quality
		&& this->Language == DataToCheck->Language;
}