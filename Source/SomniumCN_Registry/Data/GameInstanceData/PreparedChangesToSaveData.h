// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/GameInstanceData/AbstractGameInstanceData.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "PreparedChangesToSaveData.generated.h"

/**
* @descr Class of Prepared Changes To Save Data
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UPreparedChangesToSaveData : public UAbstractGameInstanceData {

	GENERATED_BODY()

public:
	UGameSettingsData* GameSettingsDataToSave;

public:
	/** Resets data class properties to default */
	UFUNCTION()
		void Reset_Implementation() override;
	
};
