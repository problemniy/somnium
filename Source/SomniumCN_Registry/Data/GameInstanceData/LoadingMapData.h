// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/GameInstanceData/AbstractGameInstanceData.h"
#include "LoadingMapData.generated.h"

/**
* @descr Class of Loading Map Data
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API ULoadingMapData : public UAbstractGameInstanceData {

	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, Category = "Assets")
		TArray<FSoftObjectPath> AssetsList;

	UPROPERTY(BlueprintReadWrite, Category = "Assets")
		TArray<FPrimaryAssetId> PrimaryAssetsList;

	UPROPERTY(BlueprintReadWrite, Category = "Assets")
		TArray<FName> BundleNamesList;

	UPROPERTY(BlueprintReadWrite, Category = "Assets")
		TArray<FPrimaryAssetId> UnloadPrimaryAssetsList;

	UPROPERTY(BlueprintReadWrite, Category = "Stream")
		TArray<FName> StreamableLevelsList;

	UPROPERTY(BlueprintReadWrite, Category = "Stream")
		TArray<FName> UnloadStreamableLevelsList;

	UPROPERTY(BlueprintReadWrite)
		UUserWidget* TransferWidgetObject;

	UPROPERTY(BlueprintReadWrite)
		float Progress;

public:
	/** Resets data class properties to default */
	UFUNCTION()
		void Reset_Implementation() override;
	
};
