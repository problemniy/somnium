// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/GameInstanceData/AbstractGameInstanceData.h"
#include "Components/InputComponent.h"
#include "InputEventOwnersData.generated.h"

/**
* @descr Class of Input Event Owners Data
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UInputEventOwnersData : public UAbstractGameInstanceData {

	GENERATED_BODY()
	
public:
	/** Consist of objects which are using action event binds */
	TMap<FName, TPair<UInputComponent*, int32>> ActionEventOwners;

public:
	/** Resets data class properties to default */
	UFUNCTION()
		void Reset_Implementation() override;
	
	
};
