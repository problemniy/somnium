// Fill out your copyright notice in the Description page of Project Settings.

#include "PreparedChangesToSaveData.h"

/** Resets data class properties to default */
void UPreparedChangesToSaveData::Reset_Implementation() {
	GameSettingsDataToSave = nullptr;
}
