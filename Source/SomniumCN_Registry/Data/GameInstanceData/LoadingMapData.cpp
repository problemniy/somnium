// Fill out your copyright notice in the Description page of Project Settings.

#include "LoadingMapData.h"

/** Resets data class properties to default */
void ULoadingMapData::Reset_Implementation() {
	AssetsList.Empty();
	PrimaryAssetsList.Empty();
	BundleNamesList.Empty();
	UnloadPrimaryAssetsList.Empty();
	StreamableLevelsList.Empty();
	UnloadStreamableLevelsList.Empty();
	
	TransferWidgetObject = nullptr;
	Progress = 0.f;
}

