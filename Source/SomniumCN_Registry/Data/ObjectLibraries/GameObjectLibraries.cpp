// Fill out your copyright notice in the Description page of Project Settings.

#include "GameObjectLibraries.h"
#include "Components/Widget.h"
#include "Engine/Font.h"
#include "Engine/DataTable.h"
#include "Constants/ResourceDirectories.h"
#include "Engine.h"

UGameObjectLibraries::UGameObjectLibraries() {
	Maps = UObjectLibrary::CreateLibrary(UWorld::StaticClass(), true, true);
	Fonts = UObjectLibrary::CreateLibrary(UFont::StaticClass(), false, true);
	Widgets = UObjectLibrary::CreateLibrary(UWidget::StaticClass(), true, true);
	DataTables = UObjectLibrary::CreateLibrary(UDataTable::StaticClass(), false, true);
}

/**
* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
*/
void UGameObjectLibraries::PostInitProperties() {
	Super::PostInitProperties();
	Maps->LoadAssetDataFromPath(Cast<UResourceDirectories>(UResourceDirectories::StaticClass()->GetDefaultObject())->Maps_Directory);
	Fonts->LoadAssetDataFromPath(Cast<UResourceDirectories>(UResourceDirectories::StaticClass()->GetDefaultObject())->Fonts_Directory);
	Widgets->LoadBlueprintAssetDataFromPath(Cast<UResourceDirectories>(UResourceDirectories::StaticClass()->GetDefaultObject())->Widgets_Directory);
	DataTables->LoadAssetDataFromPath(Cast<UResourceDirectories>(UResourceDirectories::StaticClass()->GetDefaultObject())->DataTables_Directory);
}

