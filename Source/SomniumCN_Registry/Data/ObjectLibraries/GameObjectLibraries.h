// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Engine/ObjectLibrary.h"
#include "GameObjectLibraries.generated.h"

/**
* @descr Class of Game Object Libraries
* @author problemniy
*/
UCLASS(Blueprintable, ClassGroup = Game_Root_)
class SOMNIUMCN_REGISTRY_API UGameObjectLibraries : public UObject {

	GENERATED_BODY()

private:
	UGameObjectLibraries();

public:
	/**
	* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
	* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
	*/
	virtual void PostInitProperties() override;
	
public:
	/** Maps library */
	UPROPERTY()
		UObjectLibrary* Maps;
	/** Fonts library */
	UPROPERTY()
		UObjectLibrary* Fonts;
	/** Images library */
	UPROPERTY()
		UObjectLibrary* Images;
	/** Media players library */
	UPROPERTY()
		UObjectLibrary* MediaPlayers;
	/** Maps sources library */
	UPROPERTY()
		UObjectLibrary* MediaSources;
	/** Maps materials library */
	UPROPERTY()
		UObjectLibrary* MediaMaterials;
	/** Maps textures library */
	UPROPERTY()
		UObjectLibrary* MediaTextures;
	/** Actors library */
	UPROPERTY()
		UObjectLibrary* Actors;
	/** Widgets library */
	UPROPERTY()
		UObjectLibrary* Widgets;
	/** Data tables library */
	UPROPERTY()
		UObjectLibrary* DataTables;
	
};
