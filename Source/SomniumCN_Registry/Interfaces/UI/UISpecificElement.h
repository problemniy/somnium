// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Misc/Enums/UserInterfaceEnums.h"
#include "UISpecificElement.generated.h"

/**
* @descr Class interface which specifies UI Own Binded Element properties
* @author problemniy
*/
UINTERFACE(BlueprintType)
class SOMNIUMCN_REGISTRY_API UUISpecificElement : public UInterface {

	GENERATED_BODY()

};

class SOMNIUMCN_REGISTRY_API IUISpecificElement {

	GENERATED_BODY()

public:
	/**
	* @descr Returns specific ui element tag
	* @return Element tag
	*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UI Element Basic")
		EUIElementTag GetUITag();

};
