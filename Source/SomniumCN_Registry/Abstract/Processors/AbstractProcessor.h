// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractProcessor.generated.h"

/**
* @descr Abstract class which specifies Processor
* @author problemniy
*/
UCLASS(Abstract, Blueprintable, BLueprintType, ClassGroup = Processors_)
class SOMNIUMCN_REGISTRY_API UAbstractProcessor : public UObject {

	GENERATED_BODY()

	
};
