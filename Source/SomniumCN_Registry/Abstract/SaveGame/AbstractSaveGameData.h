// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractSaveGameData.generated.h"

/**
* @descr Abstract class which specifies Save Game Data
* @author problemniy
*/
UCLASS(Abstract, ClassGroup = Transfer_Data_)
class SOMNIUMCN_REGISTRY_API UAbstractSaveGameData : public USaveGame {

	GENERATED_BODY()

public:
	/** Name of game save data to identity */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Identity")
		FString SaveDataName;
	/** User index */
	UPROPERTY(VisibleAnywhere, Category = "Identity")
		uint32 UserIndex;
	
};
