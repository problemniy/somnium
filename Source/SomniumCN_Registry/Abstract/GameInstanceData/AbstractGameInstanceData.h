// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractGameInstanceData.generated.h"

/**
* @descr Abstract class which specifies Game Instance Data
* @author problemniy
*/
UCLASS(Abstract, Blueprintable, ClassGroup = Transfer_Data_)
class SOMNIUMCN_REGISTRY_API UAbstractGameInstanceData : public UObject {

	GENERATED_BODY()
	
public:
	/**
	* @descr Overridable function to reset data class properties to default (should be implemented by child)
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game Instance Data Management")
		void Reset();
	
};
