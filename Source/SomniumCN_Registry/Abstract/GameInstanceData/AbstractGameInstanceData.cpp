// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractGameInstanceData.h"

/** Overridable function to reset data class properties to default (should be implemented by child) */
void UAbstractGameInstanceData::Reset_Implementation() {
	unimplemented();
}

