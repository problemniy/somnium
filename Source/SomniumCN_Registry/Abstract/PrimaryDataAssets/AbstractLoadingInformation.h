// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractLoadingInformation.generated.h"

/**
* @descr Abstract class which specifies Loading Information primary data asset
* @author problemniy
*/
UCLASS(Abstract, ClassGroup = Primary_Data_Asset_)
class SOMNIUMCN_REGISTRY_API UAbstractLoadingInformation : public UPrimaryDataAsset {

	GENERATED_BODY()

	
};
