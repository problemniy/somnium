// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractUtils.generated.h"

/**
* @descr Abstract class which specifies Utils
* @author problemniy
*/
UCLASS(CustomConstructor, Const, ClassGroup = Utility_Tools_)
class SOMNIUMCN_REGISTRY_API UAbstractUtils : public UObject {

	GENERATED_BODY()
	
	
};
