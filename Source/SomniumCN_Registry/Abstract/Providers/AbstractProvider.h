// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractProvider.generated.h"

/**
* @descr Abstract class which specifies Provider
* @author problemniy
*/
UCLASS(Abstract, CustomConstructor, ClassGroup = Utility_Tools_)
class SOMNIUMCN_REGISTRY_API UAbstractProvider : public UObject {

	GENERATED_BODY()
	
	
	
};
