// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractBean.h"
#include "AbstractResettableBean.generated.h"

/**
* @descr Abstract class which specifies Resettable Bean
* @author problemniy
*/
UCLASS(Abstract)
class SOMNIUMCN_REGISTRY_API UAbstractResettableBean : public UAbstractBean {

	GENERATED_BODY()
	
public:
	/**
	* @descr Overridable function to reset bean properties to default (should be implemented by child)
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Beans Management")
		void ResetProperties();
	
};
