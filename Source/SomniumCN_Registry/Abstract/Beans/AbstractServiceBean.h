// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractBean.h"
#include "AbstractServiceBean.generated.h"

/**
* @descr Abstract class which specifies Service Bean
* @author problemniy
*/
UCLASS(Abstract, ClassGroup = Services_)
class SOMNIUMCN_REGISTRY_API UAbstractServiceBean : public UAbstractBean {

	GENERATED_BODY()
	
	
};
