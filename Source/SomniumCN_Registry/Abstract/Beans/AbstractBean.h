// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "AbstractBean.generated.h"

/**
* @descr Abstract class which specifies Bean
* @author problemniy
*/
UCLASS(Abstract, Blueprintable, ClassGroup = Game_Root_)
class SOMNIUMCN_REGISTRY_API UAbstractBean : public UObject {

	GENERATED_BODY()
	
	
};
