// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractBean.h"
#include "AbstractConstantsBean.generated.h"

/**
* @descr Abstract class which specifies Constants Bean
* @author problemniy
*/
UCLASS(Abstract, Const, ClassGroup = Constants_)
class SOMNIUMCN_REGISTRY_API UAbstractConstantsBean : public UAbstractBean {

	GENERATED_BODY()

};
