// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractResettableBean.h"

/** Overridable function to reset bean properties to default (should be implemented by child) */
void UAbstractResettableBean::ResetProperties_Implementation() {
	unimplemented();
}

