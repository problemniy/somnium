// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "ResourceNames.generated.h"

/**
* @descr Bean class which contains Resource Names constants
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UResourceNames : public UAbstractConstantsBean {

	GENERATED_BODY()

protected:
	UResourceNames();

private:
	/** -------------- Filling methods */

	void FillPrimaryAssetNames();
	void FillMapNames();
	void FillAssetBundleNames();

	// Registers resource name
	void RegAssetName(FName& Var, const TCHAR* Name);

private:
	TArray<FName> AllResourceNames;
	
public:
	/** Specify a primary type name for maps */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Primary Assets")
		FName PrimaryType_Map;
	/** Specify a primary type name for main menu assets */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Primary Assets")
		FName PrimaryType_MainMenuAssets;

	/** Specify a primary type name for 'Intro' map */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Levels")
		FName Map_Intro;
	/** Specify a primary type name for 'MainMenu' map */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Levels")
		FName Map_Menu;
	/** Specify a primary type name for 'MenuCharacter' level */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Levels")
		FName Stream_MenuCharacter;
	/** Specify a primary type name for 'MenuEnvironment' level */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Levels")
		FName Stream_MenuEnvironment;

	/** Specify a bundle name for mandatory bundles */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Asset Bundles")
		FName AssetBundle_Mandatory;
	
};
