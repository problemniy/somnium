// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "GameDefaultConfiguration.generated.h"

/**
* @descr Bean class which contains constants of Game Default Configuration
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UGameDefaultConfiguration : public UAbstractConstantsBean {

	GENERATED_BODY()

private:
	UGameDefaultConfiguration();
	
private:
	void SpecifyConstants();

#if WITH_EDITOR
public:
	/** Called when a property on this object has been modified externally */
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:

	/**
	* Specify available range for brightness value
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings | Brightness", meta = (DisplayName = "Range"))
		FVector2D BrightnessAvailableRange;

	/**
	* Calculated average brightness according to brightness range.
	* This value will be the same for auto-exposure min. and auto-exposure max.
	*/
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Settings | Brightness", meta = (DisplayName = "Average (default)"))
		float DefaultBrightness;

	/** Specify available range for auto exposure min value */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings | Auto-Exposure Brightness", meta = (DisplayName = "Range (min)"))
		FVector2D AutoExposureMinRange;

	/** Specify available range for auto exposure max value */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings | Auto-Exposure Brightness", meta = (DisplayName = "Range (max)"))
		FVector2D AutoExposureMaxRange;

};

