// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDefaultConfiguration.h"

UGameDefaultConfiguration::UGameDefaultConfiguration() {
	SpecifyConstants();
}

void UGameDefaultConfiguration::SpecifyConstants() {
	// brightness
	BrightnessAvailableRange.Set(0.2f, 0.6f);
	DefaultBrightness = (BrightnessAvailableRange.Y + BrightnessAvailableRange.X) / 2;
	// auto-exposure brightness calibration
	AutoExposureMinRange.Set(0.01f, 0.6f);
	AutoExposureMaxRange.Set(0.2f, 3.6f);
}

#if WITH_EDITOR
/** Called when a property on this object has been modified externally */
void UGameDefaultConfiguration::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) {
	Super::PostEditChangeProperty(PropertyChangedEvent);
	// calculate average brightness
	DefaultBrightness = (BrightnessAvailableRange.Y + BrightnessAvailableRange.X) / 2;
}
#endif

