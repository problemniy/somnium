// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSaveConfigValues.h"
#include "Misc/Paths.h"

UGameSaveConfigValues::UGameSaveConfigValues() {
	SpecifyConstants();
	ChangeGameSaveDirectory();
}

void UGameSaveConfigValues::SpecifyConstants() {
	// specify directories
	SaveDirectory = TEXT("Saved\\SaveGames");

	// specify slot names
	SaveSlotName_1 = TEXT("Slot_1");
	SaveSlotName_2 = TEXT("Slot_2");
	SaveSlotName_3 = TEXT("Slot_3");

	// specify configuration slot names
	GameSettingsDataName = TEXT("Game_Settings");
}

// Changes the save directory according to specified constant
void UGameSaveConfigValues::ChangeGameSaveDirectory() {
	/* @todo Implement changing directory */
}

#if WITH_EDITOR
/** Called when a property on this object has been modified externally */
void UGameSaveConfigValues::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) {
	Super::PostEditChangeProperty(PropertyChangedEvent);
	ChangeGameSaveDirectory();
}
#endif


