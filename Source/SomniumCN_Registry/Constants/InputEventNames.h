// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "InputEventNames.generated.h"

/**
* @descr Bean class which contains Input Event Names constants
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UInputEventNames : public UAbstractConstantsBean {

	GENERATED_BODY()

private:
	UInputEventNames();
	
// Do not use constants to bind delegates to input events only. Please use 'InputServiceManager' to register owner object before binding
public:
	/** Specify 'Any Pressed' action name */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Action Names")
		FName Action_AnyPressed;
	/** Specify 'Skip Intro' action name */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Action Names")
		FName Action_SkipIntro;
	
	
};
