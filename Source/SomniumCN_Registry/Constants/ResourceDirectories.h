// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "ResourceDirectories.generated.h"

/**
* @descr Bean class which contains Resource Directories constants
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UResourceDirectories : public UAbstractConstantsBean {

	GENERATED_BODY()

private:
	UResourceDirectories();

protected:
	// Creates default directories constants
	void FillDefaultDirectories();
	// Propagates received name to field
	void RegDefaultDirectory(FString& Property, FString Directory);

public:
	/** Specify directory path to maps */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Common")
		FString Maps_Directory;
	/** Specify directory path to fonts */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Common")
		FString Fonts_Directory;
	/** Specify directory path to images */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Common")
		FString Images_Directory;

	/** Specify directory path to media players */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Media")
		FString MediaPlayers_Directory;
	/** Specify directory path to media sources */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Media")
		FString MediaSources_Directory;
	/** Specify directory path to media materials */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Media")
		FString MediaMaterials_Directory;
	/** Specify directory path to media textures */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Media")
		FString MediaTextures_Directory;

	/** Specify directory path to actors */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Blueprints")
		FString Actors_Directory;
	/** Specify directory path to widgets */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Blueprints")
		FString Widgets_Directory;
	/** Specify data tables path to widgets */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Blueprints")
		FString DataTables_Directory;


};
