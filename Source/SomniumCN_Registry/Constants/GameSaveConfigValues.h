// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "GameSaveConfigValues.generated.h"

/**
* @descr Bean class which contains constants of Game Save Configuration Values
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UGameSaveConfigValues : public UAbstractConstantsBean {

	GENERATED_BODY()

private:
	UGameSaveConfigValues();

private:
	void SpecifyConstants();
	// Changes the save directory according to specified constant
	void ChangeGameSaveDirectory();

#if WITH_EDITOR
public:
	/** Called when a property on this object has been modified externally */
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	
public:
	/** Specify a directory to save files */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Directories")
		FString SaveDirectory;

	/** Specify a name to first save slot */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot Names")
		FString SaveSlotName_1;
	/** Specify a name to second save slot */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot Names")
		FString SaveSlotName_2;
	/** Specify a name to third save slot */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slot Names")
		FString SaveSlotName_3;
	
	/** Specify a name to game settings data */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data Names")
		FString GameSettingsDataName;
};
