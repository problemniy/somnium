// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractConstantsBean.h"
#include "GameLoggingAccessibility.generated.h"

/**
* @descr Bean class which contains logging accessibility triggers to activate or disabled them
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_REGISTRY_API UGameLoggingAccessibility : public UAbstractConstantsBean {

	GENERATED_BODY()

public:
	/** Activates loading assets logs with verbosity level 'Error' */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (DisplayName = "Loading Assets Error Logs"))
		bool bLoadingAssetsErrorLogsEnabled = true;
	/** Activates loading assets logs with verbosity level 'Warning' */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (DisplayName = "Loading Assets Warning Logs"))
		bool bLoadingAssetsWarningLogsEnabled = true;
	/** Activates loading assets logs with verbosity level 'Log' */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (DisplayName = "Loading Assets Info Logs"))
		bool bLoadingAssetsInfoLogsEnabled;
	/** Activates loading assets logs with verbosity level 'Verbose' */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (DisplayName = "Loading Assets Debug Logs"))
		bool bLoadingAssetsDebugLogsEnabled;
	/** Activates loading assets logs with verbosity level 'VeryVerbose' */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (DisplayName = "Loading Assets Full Logs"))
		bool bLoadingAssetsFullLogsEnabled;
	
};

