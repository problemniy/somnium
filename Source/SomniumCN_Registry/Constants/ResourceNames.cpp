// Fill out your copyright notice in the Description page of Project Settings.

#include "ResourceNames.h"

UResourceNames::UResourceNames() {
	FillPrimaryAssetNames();
	FillMapNames();
	FillAssetBundleNames();
}

void UResourceNames::FillPrimaryAssetNames() {
	RegAssetName(PrimaryType_Map, TEXT("Map"));
	RegAssetName(PrimaryType_MainMenuAssets, TEXT("MainMenuPrimaryDataAsset"));
}

void UResourceNames::FillMapNames() {
	// persistent
	RegAssetName(Map_Intro, TEXT("Map_Intro"));
	RegAssetName(Map_Menu, TEXT("Map_Menu"));
	// sub-levels
	RegAssetName(Stream_MenuCharacter, TEXT("Stream_MenuCharacter"));
	RegAssetName(Stream_MenuEnvironment, TEXT("Stream_MenuEnvironment"));
}

void UResourceNames::FillAssetBundleNames() {
	RegAssetName(AssetBundle_Mandatory, TEXT(Mandatory_));
}

// Registers resource name
void UResourceNames::RegAssetName(FName& Var, const TCHAR* Name) {
	Var = FName(Name);
	AllResourceNames.Add(Var);
}
