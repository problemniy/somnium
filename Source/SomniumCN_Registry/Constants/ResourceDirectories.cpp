// Fill out your copyright notice in the Description page of Project Settings.

#include "ResourceDirectories.h"

UResourceDirectories::UResourceDirectories() {
	FillDefaultDirectories();
}

// Creates default directories constants
void UResourceDirectories::FillDefaultDirectories() {
	RegDefaultDirectory(Maps_Directory, TEXT("/Game/Maps"));
	RegDefaultDirectory(Fonts_Directory, TEXT("/Game/Assets/Fonts"));
	RegDefaultDirectory(Images_Directory, TEXT("/Game/Assets/Images"));
	RegDefaultDirectory(MediaPlayers_Directory, TEXT("/Game/Assets/Media/Players"));
	RegDefaultDirectory(MediaSources_Directory, TEXT("/Game/Assets/Media/Sources"));
	RegDefaultDirectory(MediaMaterials_Directory, TEXT("/Game/Assets/Media/Materials"));
	RegDefaultDirectory(MediaTextures_Directory, TEXT("/Game/Assets/Media/Textures"));
	RegDefaultDirectory(Actors_Directory, TEXT("/Game/Blueprints/Actors"));
	RegDefaultDirectory(Widgets_Directory, TEXT("/Game/Blueprints/UI"));
	RegDefaultDirectory(DataTables_Directory, TEXT("/Game/Blueprints"));
}

// Propagates received name to field
void UResourceDirectories::RegDefaultDirectory(FString& Property, FString Directory) {
	if (Property.IsEmpty()) {
		Property = *Directory;
	}
}

