// Fill out your copyright notice in the Description page of Project Settings.

#include "InputEventNames.h"

UInputEventNames::UInputEventNames() {
	Action_AnyPressed = FName(TEXT("AnyPressedAction"));
	Action_SkipIntro = FName(TEXT("SkipIntroAction"));
}
