// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

//----------- Loading Assets Logs

SOMNIUMCN_MANAGEMENT_API DECLARE_LOG_CATEGORY_EXTERN(LoadingAssetsErrorsLogs, Error, Error);
SOMNIUMCN_MANAGEMENT_API DECLARE_LOG_CATEGORY_EXTERN(LoadingAssetsWarningLogs, Warning, Warning);
SOMNIUMCN_MANAGEMENT_API DECLARE_LOG_CATEGORY_EXTERN(LoadingAssetsInfoLogs, Log, Log);
SOMNIUMCN_MANAGEMENT_API DECLARE_LOG_CATEGORY_EXTERN(LoadingAssetsDebugLogs, Verbose, Verbose);
SOMNIUMCN_MANAGEMENT_API DECLARE_LOG_CATEGORY_EXTERN(LoadingAssetsFullLogs, VeryVerbose, VeryVerbose);

