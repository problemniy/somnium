// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Utils/GameLogsUtils.h"
#include "CoreMinimal.h"

/** @descr Shows game log according to specified group, level and verbosity parameters */
#define SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, Verbosity, Format, ...) \
	if (UGameLogsUtils::AreLogsEnabled(LogsGroup, LogsLevel)) { \
		switch(LogsGroup) { \
			case ELogsGroup::LOADING_ASSETS: \
				if (ELogsLevel::ERROR == LogsLevel) { UE_LOG(LoadingAssetsErrorsLogs, Verbosity, Format, ##__VA_ARGS__); } \
				else if (ELogsLevel::WARNING == LogsLevel) { UE_LOG(LoadingAssetsWarningLogs, Verbosity, Format, ##__VA_ARGS__); } \
				else if (ELogsLevel::INFO == LogsLevel) { UE_LOG(LoadingAssetsInfoLogs, Verbosity, Format, ##__VA_ARGS__); } \
				else if (ELogsLevel::DEBUG == LogsLevel) { UE_LOG(LoadingAssetsDebugLogs, Verbosity, Format, ##__VA_ARGS__); } \
				else if (ELogsLevel::FULL == LogsLevel) { UE_LOG(LoadingAssetsFullLogs, Verbosity, Format, ##__VA_ARGS__); } \
				break; \
		} \
	} \

/** @descr Shows game log according to specified group and level (verbosity level is chose by default) */
#define SHOW_GAME_LOG(LogsGroup, LogsLevel, Format, ...) \
	switch(LogsLevel) { \
		case ELogsLevel::ERROR: SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, Error, Format, ##__VA_ARGS__); break; \
		case ELogsLevel::WARNING: SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, Warning, Format, ##__VA_ARGS__); break; \
		case ELogsLevel::INFO: SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, Log, Format, ##__VA_ARGS__); break; \
		case ELogsLevel::DEBUG: SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, Verbose, Format, ##__VA_ARGS__); break; \
		case ELogsLevel::FULL: SHOW_CUSTOM_GAME_LOG(LogsGroup, LogsLevel, VeryVerbose, Format, ##__VA_ARGS__); break; \
	} \

/** @descr Shows game error log according to specified group (verbosity level is chose by default) */
#define GAME_ERROR(LogsGroup, Format, ...) \
	if (UGameLogsUtils::AreLogsEnabled(LogsGroup, ELogsLevel::ERROR)) { \
			switch (LogsGroup) { \
				case ELogsGroup::LOADING_ASSETS: SHOW_GAME_LOG(LogsGroup, ELogsLevel::ERROR, Format, ##__VA_ARGS__); break; \
			} \
	} \

