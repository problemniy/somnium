// Fill out your copyright notice in the Description page of Project Settings.

#include "GameLogDefinitions.h"

//----------- Loading Assets Logs

DEFINE_LOG_CATEGORY(LoadingAssetsErrorsLogs);
DEFINE_LOG_CATEGORY(LoadingAssetsWarningLogs);
DEFINE_LOG_CATEGORY(LoadingAssetsInfoLogs);
DEFINE_LOG_CATEGORY(LoadingAssetsDebugLogs);
DEFINE_LOG_CATEGORY(LoadingAssetsFullLogs);

