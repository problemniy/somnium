// Fill out your copyright notice in the Description page of Project Settings.

#include "MainGameInstance.h"
#include "EngineData/Singletons/BeansContextSingleton.h"
#include "Utils/GameCommonUtils.h"
#include "Engine.h"

/**
* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
*/
void UMainGameInstance::PostInitProperties() {
	Super::PostInitProperties();
	UGameCommonUtils::AssignPointer(GameInstanceDataSingleton, &GameInstanceData);
};

/** Called when the game instance is started either normally or through PIE. */
void UMainGameInstance::OnStart() {
	Super::OnStart();

}

/** virtual function to allow custom GameInstances an opportunity to do cleanup when shutting down */
void UMainGameInstance::Shutdown() {
	Super::Shutdown();
	// reset all beans when game is ended
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine->GameSingleton);
	if (BeansContext || BeansContext->IsValidLowLevel()) {
		BeansContext->ResetBeansProperties();
	}
}