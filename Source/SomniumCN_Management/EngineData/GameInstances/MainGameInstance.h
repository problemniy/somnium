// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "MainGameInstance.generated.h"

/**
* @descr Game class of Engine Game Instance
* @author problemniy
*/
UCLASS(ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API UMainGameInstance : public UGameInstance {

	GENERATED_BODY()
	
protected:
	/** Called when the game instance is started either normally or through PIE. */
	virtual void OnStart() override;

public:
	/**
	 * Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
	 * mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
	 */
	virtual void PostInitProperties() override;

	/** virtual function to allow custom GameInstances an opportunity to do cleanup when shutting down */
	virtual void Shutdown() override;

protected:
	/** Specify game instance data singleton to create */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Common Configuration")
		TSubclassOf<UObject> GameInstanceDataSingleton;

public:
	/** Created object by 'GameInstanceDataSingleton' */
	UPROPERTY(BlueprintReadOnly, Category = "Game Instance Data")
		UObject* GameInstanceData;

};
