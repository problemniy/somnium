// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Abstract/Beans/AbstractResettableBean.h"
#include "Constants/ResourceNames.h"
#include "Constants/ResourceDirectories.h"
#include "Constants/InputEventNames.h"
#include "Constants/GameSaveConfigValues.h"
#include "Constants/GameDefaultConfiguration.h"
#include "Constants/GameLoggingAccessibility.h"
#include "Services/AssetManagementService.h"
#include "Services/WorldManagementService.h"
#include "Services/SaveGameService.h"
#include "Services/GameInstanceManagementService.h"
#include "Services/PlayerControllerManageService.h"
#include "Services/GameSettingsService.h"
#include "BeansContextSingleton.generated.h"

/**
* @descr Class Singleton which contains beans
* @author problemniy
*/
UCLASS(Blueprintable, ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API UBeansContextSingleton : public UObject {

	GENERATED_BODY()
	
	/** -------- Accepted classes */

	friend class UConstantsProvider;
	friend class UServicesProvider;

private:
	// Contains resettable beans 
	TArray<UAbstractResettableBean*> AllResettableBeans;

	template <class TClassA>
	void RegResettableBean(TClassA* Bean);

	template <class TClassA>
	void CreateBean(const TSubclassOf<TClassA>& Subclass, TClassA** Bean);

/** -------- Beans */
private:
	UPROPERTY()
		UResourceNames* ResourceNamesBean;
	UPROPERTY()
		UResourceDirectories* ResourceDirectoriesBean;
	UPROPERTY()
		UInputEventNames* InputEventNamesBean;
	UPROPERTY()
		UGameSaveConfigValues* GameSaveConfigValuesBean;
	UPROPERTY()
		UGameDefaultConfiguration* GameDefaultConfigurationBean;
	UPROPERTY()
		UGameLoggingAccessibility* GameLoggingAccessibilityBean;
	UPROPERTY()
		UAssetManagementService* AssetManagementServiceBean;
	UPROPERTY()
		UWorldManagementService* WorldManagementServiceBean;
	UPROPERTY()
		UGameInstanceManagementService* GameInstanceManagementServiceBean;
	UPROPERTY()
		UPlayerControllerManageService* PlayerControllerManageServiceBean;
	UPROPERTY()
		USaveGameService* SaveGameServiceBean;
	UPROPERTY()
		UGameSettingsService* GameSettingsServiceBean;

/** -------- Specified classes for beans */
protected:
	/** Specify Asset Names Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UResourceNames> ResourceNamesConstants;

	/** Specify Resource Directory Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UResourceDirectories> ResourceDirectoriesConstants;

	/** Specify Asset Specifiers Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UInputEventNames> InputEventNamesConstants;

	/** Specify Game Save Configuration Values Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UGameSaveConfigValues> GameSaveConfigValuesConstants;

	/** Specify Game Default Configuration Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UGameDefaultConfiguration> GameDefaultConfigurationConstants;

	/** Specify Game Logging Accessibility Constants Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Constants")
		TSubclassOf<UGameLoggingAccessibility> GameLoggingAccessibilityConstants;

	/** Specify Asset Management Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<UAssetManagementService> AssetManagementService;

	/** Specify World Management Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<UWorldManagementService> WorldManagementService;

	/** Specify Game Instance Management Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<UGameInstanceManagementService> GameInstanceManagementService;

	/** Specify Player Controller Management Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<UPlayerControllerManageService> PlayerControllerManagementService;

	/** Specify Save Game Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<USaveGameService> SaveGameService;

	/** Specify Game Settings Service Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Services")
		TSubclassOf<UGameSettingsService> GameSettingsService;

public:
	/**
	 * Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
	 * mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
	 */
	virtual void PostInitProperties() override;

public:
	/**
	* @descr Invokes reset function for all resettable beans
	*/
	UFUNCTION(BlueprintCallable, Category = "Beans Management")
		void ResetBeansProperties();

};
