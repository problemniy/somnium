// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Data/GameInstanceData/LoadingMapData.h"
#include "Data/GameInstanceData/InputEventOwnersData.h"
#include "Data/GameInstanceData/PreparedChangesToSaveData.h"
#include "GameInstanceDataContextSingleton.generated.h"

/**
* @descr Class Singleton which contains data classes for Game Instance
* @author problemniy
*/
UCLASS(Blueprintable, ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API UGameInstanceDataContextSingleton : public UObject {

	GENERATED_BODY()

	/** -------- Accepted classes */

	friend class UGameInstanceDataProvider;
	
private:
	template <class TClassA>
	void CreateData(const TSubclassOf<TClassA>& DataClass, TClassA** DapaPtr);

public:
	/**
	* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
	* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
	*/
	virtual void PostInitProperties() override;

/** -------- Pointers to created data objects */
private:
	UPROPERTY()
		ULoadingMapData* LoadingMapDataPtr;
	UPROPERTY()
		UInputEventOwnersData* InputEventOwnersDataPtr;
	UPROPERTY()
		UPreparedChangesToSaveData* PreparedChangesToSaveDataPtr;

/** -------- Specified data classes to create objects */
protected:
	/** Specify Loading Map Data Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data Classes")
		TSubclassOf<ULoadingMapData> LoadingMapData;
	/** Specify Input Event Owners Data Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data Classes")
		TSubclassOf<UInputEventOwnersData> InputEventOwnersData;
	/** Specify Prepared Changes To Save Data Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data Classes")
		TSubclassOf<UPreparedChangesToSaveData> PreparedChangesToSaveData;
	
};
