// Fill out your copyright notice in the Description page of Project Settings.

#include "BeansContextSingleton.h"
#include "Utils/GameCommonUtils.h"

/**
* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
*/
void UBeansContextSingleton::PostInitProperties() {
	Super::PostInitProperties();
	// create beans
	CreateBean(ResourceNamesConstants, &ResourceNamesBean);
	CreateBean(ResourceDirectoriesConstants, &ResourceDirectoriesBean);
	CreateBean(InputEventNamesConstants, &InputEventNamesBean);
	CreateBean(GameSaveConfigValuesConstants, &GameSaveConfigValuesBean);
	CreateBean(GameDefaultConfigurationConstants, &GameDefaultConfigurationBean);
	CreateBean(GameLoggingAccessibilityConstants, &GameLoggingAccessibilityBean);
	CreateBean(AssetManagementService, &AssetManagementServiceBean);
	CreateBean(WorldManagementService, &WorldManagementServiceBean);
	CreateBean(GameInstanceManagementService, &GameInstanceManagementServiceBean);
	CreateBean(PlayerControllerManagementService, &PlayerControllerManageServiceBean);
	CreateBean(SaveGameService, &SaveGameServiceBean);
	CreateBean(GameSettingsService, &GameSettingsServiceBean);
}

template <class TClassA>
void UBeansContextSingleton::CreateBean(const TSubclassOf<TClassA>& Subclass, TClassA** Bean) {
	UGameCommonUtils::AssignPointerOrCreateDefault(Subclass, Bean);
	RegResettableBean(*Bean);
}

template <class TClassA>
void UBeansContextSingleton::RegResettableBean(TClassA* Bean) {
	if (TClassA::StaticClass()->IsChildOf((UAbstractResettableBean::StaticClass()))) {
		AllResettableBeans.Add(Cast<UAbstractResettableBean>(Bean));
	}
}

/** Invokes reset function for all resettable beans */
void UBeansContextSingleton::ResetBeansProperties() {
	for (UAbstractResettableBean* Bean : AllResettableBeans) {
		Bean->ResetProperties();
	}
}
