// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInstanceDataContextSingleton.h"
#include "Utils/GameCommonUtils.h"

/**
* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
*/
void UGameInstanceDataContextSingleton::PostInitProperties() {
	Super::PostInitProperties();
	// create data objects
	CreateData(LoadingMapData, &LoadingMapDataPtr);
	CreateData(InputEventOwnersData, &InputEventOwnersDataPtr);
	CreateData(PreparedChangesToSaveData, &PreparedChangesToSaveDataPtr);
}

template <class TClassA>
void UGameInstanceDataContextSingleton::CreateData(const TSubclassOf<TClassA>& DataClass, TClassA** DapaPtr) {
	UGameCommonUtils::AssignPointerOrCreateDefault(DataClass, DapaPtr);
}
