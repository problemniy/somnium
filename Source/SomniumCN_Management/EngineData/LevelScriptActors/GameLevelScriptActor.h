// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "GameLevelScriptActor.generated.h"

/**
* @descr Game class of Engine Level Script Actor
* @author problemniy
*/
UCLASS(ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API AGameLevelScriptActor : public ALevelScriptActor {

	GENERATED_BODY()

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the game ends or when is being removed from a level
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	// Called every frame
	void Tick(float DeltaSeconds) override;


};
