// Fill out your copyright notice in the Description page of Project Settings.

#include "GameLevelScriptActor.h"

// Called when the game starts or when spawned
void AGameLevelScriptActor::BeginPlay() {
	Super::BeginPlay();

}

// Called when the game ends or when is being removed from a level
void AGameLevelScriptActor::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);

}

// Called every frame
void AGameLevelScriptActor::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

}
