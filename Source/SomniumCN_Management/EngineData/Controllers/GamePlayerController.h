// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Misc/Enums/GameControllerEnums.h"
#include "GamePlayerController.generated.h"

/**
* @descr Game class of Engine Player Controller
* @author problemniy
*/
DECLARE_DYNAMIC_DELEGATE(FGamePlayerControllerActionDelegate);
UCLASS(ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API AGamePlayerController : public APlayerController {

	GENERATED_BODY()

public:
	AGamePlayerController();

private:
	TMap<const EInputActionEvent, void (AGamePlayerController::*)()> ControllerDelegatePtrs;
	TMap<const EInputActionEvent, const FGamePlayerControllerActionDelegate> RegisteredDelegates;

private:
	/** ------------------- Common methods */

	// Fills controller delegate pointers
	void SpecifyControllerDelegatePointers();
	// Binds function handler
	bool BindEventHandler(const FName& ActionEventName, const EInputEvent& InputEventType, void (AGamePlayerController::*DelegatePtr)());

	/** ------------------- Handlers */

	void OnSkipIntroEventAction();
	void OnAnyPressedEventAction();

public:
	/**
	* @descr Binds action event delegate
	* @param InputActionEventType - Input action event type to bind
	* @param InputEventType - Input event type to bind
	* @param Delegate - Delegate which should be bind
	* @return true if binding is successful otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Input Controller Delegates")
		bool BindActionEventDelegate(const EInputActionEvent InputActionEventType, const EInputEvent InputEventType, const FGamePlayerControllerActionDelegate& Delegate);

	/**
	* @descr Removes action event delegate from binding
	* @param InputActionEventType - Input action event type to remove
	*/
	UFUNCTION(BlueprintCallable, Category = "Input Controller Delegates")
		void RemoveActionEventDelegate(const EInputActionEvent InputActionEventType);
	
};
