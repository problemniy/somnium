// Fill out your copyright notice in the Description page of Project Settings.

#include "GamePlayerController.h"
#include "Providers/ConstantsProvider.h"
#include "Providers/ServicesProvider.h"
#include "Classes/Engine/InputDelegateBinding.h"

AGamePlayerController::AGamePlayerController() {
	SpecifyControllerDelegatePointers();
}

// Fills controller delegate pointers
void AGamePlayerController::SpecifyControllerDelegatePointers() {
	ControllerDelegatePtrs.Add(EInputActionEvent::SKIP_INTRO, &AGamePlayerController::OnSkipIntroEventAction);
	ControllerDelegatePtrs.Add(EInputActionEvent::ANY_PRESSED, &AGamePlayerController::OnAnyPressedEventAction);
}

/** Binds action event delegate */
bool AGamePlayerController::BindActionEventDelegate(const EInputActionEvent InputActionEventType, const EInputEvent InputEventType, const FGamePlayerControllerActionDelegate& Delegate) {
	if (!RegisteredDelegates.Contains(InputActionEventType)) {
		if (BindEventHandler(UServicesProvider::GetPlayerControllerManageService()->GetInputActionEventName(InputActionEventType), InputEventType, *ControllerDelegatePtrs.Find(InputActionEventType))) {
			RegisteredDelegates.Add(InputActionEventType, Delegate);
			return true;
		}
	}
	return false;
}

/** Removes action event delegate from binding */
void AGamePlayerController::RemoveActionEventDelegate(const EInputActionEvent InputActionEventType) {
	if (RegisteredDelegates.Contains(InputActionEventType)) {
		FName EventName = UServicesProvider::GetPlayerControllerManageService()->GetInputActionEventName(InputActionEventType);
		int32 EventPosition = UServicesProvider::GetGameInstanceManagementService()->GetActionEventPosition(EventName, InputComponent);
		InputComponent->RemoveActionBinding(EventPosition);
		UServicesProvider::GetGameInstanceManagementService()->UnregActionEventOwner(EventName, this);
		FGamePlayerControllerActionDelegate Delegate = *RegisteredDelegates.Find(InputActionEventType);
		Delegate.Clear();
		RegisteredDelegates.Remove(InputActionEventType);
	}
}

// Binds function handler
bool AGamePlayerController::BindEventHandler(const FName& ActionEventName, const EInputEvent& InputEvent, void (AGamePlayerController::*DelegatePtr)()) {
	if (!UServicesProvider::GetGameInstanceManagementService()->IsActionEventRegistered(ActionEventName, this)) {
		UServicesProvider::GetGameInstanceManagementService()->RegActionEventOwner(ActionEventName, InputComponent);
		FInputActionBinding& InputActionBinding = InputComponent->BindAction(ActionEventName, InputEvent, this, DelegatePtr);
		return true;
	}
	return false;
}

/** ------------------- Handlers */

void AGamePlayerController::OnSkipIntroEventAction() {
	RegisteredDelegates.Find(EInputActionEvent::SKIP_INTRO)->Execute();
}

void AGamePlayerController::OnAnyPressedEventAction() {
	RegisteredDelegates.Find(EInputActionEvent::ANY_PRESSED)->Execute();
}