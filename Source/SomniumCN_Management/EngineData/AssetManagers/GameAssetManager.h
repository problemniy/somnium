// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "Misc/Specifiers/GlobalSpecifiers.h"
#include "Data/ObjectLibraries/GameObjectLibraries.h"
#include "GameAssetManager.generated.h"

/**
* @descr Game class of Engine Asset Manager
* @author problemniy
*/
UCLASS(Blueprintable, ClassGroup = Game_Root_)
class SOMNIUMCN_MANAGEMENT_API UGameAssetManager : public UAssetManager {

	GENERATED_BODY()

private:
	// Created by 'ObjectLibrariesClass'
	UPROPERTY()
		UGameObjectLibraries* ObjectLibraries;

protected:
	/** Specify object library class to create */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Object Libraries")
		TSubclassOf<UGameObjectLibraries> ObjectLibrariesClass;

protected:
	/**
	 * Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
	 * mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
	 */
	virtual void PostInitProperties() override;

public:
	/**
	* @descr Async loads primary assets in memory by primary asset names
	* @param PrimaryAssetNames - Names of primary assets to load
	* @param PrimaryAssetTypeNames - Names of primary asset types for what we should use to search
	* @param LoadBundles - Bundle names to load with primary assets
	* @param StreamableDelegate - Delegate on loading finished action
	* @return Streamable handle
	*/
	TSharedPtr<FStreamableHandle> LoadPrimaryAssetsByNames(const TArray<FName>& PrimaryAssetNames, const TArray<FName>& PrimaryAssetTypeNames, const TArray<FName>& LoadBundles = TArray<FName>(), FStreamableDelegate StreamableDelegate = FStreamableDelegate());

	/**
	* @descr Async unloads primary assets from memory by primary asset names
	* @param PrimaryAssetNames - Names of primary assets to unload
	* @param PrimaryAssetTypeNames - Names of primary asset types for what we should use to search
	* @return Count of unloaded assets
	*/
	int32 UnloadPrimaryAssetsByNames(const TArray<FName>& PrimaryAssetNames, const TArray<FName>& PrimaryAssetTypeNames);

public:
	/**
	* @descr Returns object librararies
	* @return Object libraries pointer
	*/
	UFUNCTION(BlueprintPure, Category = "Object Libraries")
		UGameObjectLibraries* GetObjectLibraries();
	
};
