// Fill out your copyright notice in the Description page of Project Settings.

#include "GameAssetManager.h"
#include "Providers/ServicesProvider.h"
#include "Utils/AssetManagementUtils.h"

/**
* Called after the C++ constructor and after the properties have been initialized, including those loaded from config.
* mainly this is to emulate some behavior of when the constructor was called after the properties were initialized.
*/
void UGameAssetManager::PostInitProperties() {
	Super::PostInitProperties();
	// init object libraries
	if (ObjectLibrariesClass) {
		ObjectLibraries = NewObject<UGameObjectLibraries>(ObjectLibrariesClass->GetDefaultObject(), UGameObjectLibraries::StaticClass());
	}
	else {
		ObjectLibraries = NewObject<UGameObjectLibraries>(UGameObjectLibraries::StaticClass());
	}
}

/** Async loads primary assets in memory by primary asset names */
TSharedPtr<FStreamableHandle> UGameAssetManager::LoadPrimaryAssetsByNames(const TArray<FName>& PrimaryAssetNames, const TArray<FName>& PrimaryAssetTypeNames, const TArray<FName>& LoadBundles /*= TArray<FName>()*/, FStreamableDelegate StreamableDelegate /*= FStreamableDelegate()*/) {
	TArray<FPrimaryAssetId> PrimaryAssetIds;
	UServicesProvider::GetAssetManagementService()->GetPrimaryAssetIdsByNames(PrimaryAssetNames, PrimaryAssetTypeNames, PrimaryAssetIds);
	return LoadPrimaryAssets(PrimaryAssetIds, LoadBundles, StreamableDelegate);
}

/** Async unloads primary assets from memory by primary asset names */
int32 UGameAssetManager::UnloadPrimaryAssetsByNames(const TArray<FName>& PrimaryAssetNames, const TArray<FName>& PrimaryAssetTypeNames) {
	TArray<FPrimaryAssetId> PrimaryAssetIds;
	UServicesProvider::GetAssetManagementService()->GetPrimaryAssetIdsByNames(PrimaryAssetNames, PrimaryAssetTypeNames, PrimaryAssetIds);
	return UnloadPrimaryAssets(PrimaryAssetIds);
}

/** Returns object librararies */
UGameObjectLibraries* UGameAssetManager::GetObjectLibraries() {
	return ObjectLibraries;
}

