// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInstanceDataProvider.h"
#include "EngineData/GameInstances/MainGameInstance.h"
#include "Engine/World.h"
#include "Engine.h"

// Get singleton of game instance data
UGameInstanceDataContextSingleton* UGameInstanceDataProvider::GetGameInstanceDataContext(UObject* ContextObject) {
	if (GEngine) {
		UMainGameInstance* MainGameInstance = Cast<UMainGameInstance>(GEngine->GetWorldFromContextObject(ContextObject, EGetWorldErrorMode::ReturnNull)->GetGameInstance());
		if (MainGameInstance) {
			return Cast<UGameInstanceDataContextSingleton>(MainGameInstance->GameInstanceData);
		}
	}
	return nullptr;
}

/** Returns object containing loading map data */
ULoadingMapData* UGameInstanceDataProvider::GetLoadingMapData(UObject* ContextObject) {
	UGameInstanceDataContextSingleton* DataContext = GetGameInstanceDataContext(ContextObject);
	return DataContext ? DataContext->LoadingMapDataPtr : nullptr;
}

/** Returns object containing input event owners data */
UInputEventOwnersData* UGameInstanceDataProvider::GetInputEventOwnersData(UObject* ContextObject) {
	UGameInstanceDataContextSingleton* DataContext = GetGameInstanceDataContext(ContextObject);
	return DataContext ? DataContext->InputEventOwnersDataPtr : nullptr;
}

/** Returns object containing prepared changes to save data */
UPreparedChangesToSaveData* UGameInstanceDataProvider::GetPreparedChangesToSaveData(UObject* ContextObject) {
	UGameInstanceDataContextSingleton* DataContext = GetGameInstanceDataContext(ContextObject);
	return DataContext ? DataContext->PreparedChangesToSaveDataPtr : nullptr;
}


