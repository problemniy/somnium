// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Providers/AbstractProvider.h"
#include "EngineData/Singletons/GameInstanceDataContextSingleton.h"
#include "GameInstanceDataProvider.generated.h"

/**
* @descr Class provider for Game Instance Data classes
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UGameInstanceDataProvider : public UAbstractProvider {

	GENERATED_BODY()

private:
	// Get singleton of game instance data
	static UGameInstanceDataContextSingleton* GetGameInstanceDataContext(UObject* ContextObject);
	
public:
	/**
	* @descr Returns object containing loading map data
	* @param ContextObject - Object which will be used for searching game instance
	* @return Pointer to object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Game Instance Data")
		static ULoadingMapData* GetLoadingMapData(UObject* ContextObject);

	/**
	* @descr Returns object containing input event owners data
	* @param ContextObject - Object which will be used for searching game instance
	* @return Pointer to object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Game Instance Data")
		static UInputEventOwnersData* GetInputEventOwnersData(UObject* ContextObject);

	/**
	* @descr Returns object containing prepared changes to save data
	* @param ContextObject - Object which will be used for searching game instance
	* @return Pointer to object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Game Instance Data")
		static UPreparedChangesToSaveData* GetPreparedChangesToSaveData(UObject* ContextObject);
	
	
};
