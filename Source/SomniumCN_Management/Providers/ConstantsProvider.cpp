// Fill out your copyright notice in the Description page of Project Settings.

#include "ConstantsProvider.h"
#include "EngineData/Singletons/BeansContextSingleton.h"
#include "Engine.h"

/** Returns bean containing resource names constants */
UResourceNames* UConstantsProvider::GetResourceNames() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->ResourceNamesBean : nullptr;
}

/** Returns bean containing resource directories constants */
UResourceDirectories* UConstantsProvider::GetResourceDirectories() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->ResourceDirectoriesBean : nullptr;
}

/** Returns bean containing input event names constants */
UInputEventNames* UConstantsProvider::GetInputEventNames() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->InputEventNamesBean : nullptr;
}

/** Returns bean containing game save configuration values constants */
UGameSaveConfigValues* UConstantsProvider::GetGameSaveConfigValues() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->GameSaveConfigValuesBean : nullptr;
}

/** Returns bean containing game default configuration constants */
UGameDefaultConfiguration* UConstantsProvider::GetGameDefaultConfiguration() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->GameDefaultConfigurationBean : nullptr;
}

/** Returns bean containing game logging accessibility constants */
UGameLoggingAccessibility* UConstantsProvider::GetGameLoggingAccessibility() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->GameLoggingAccessibilityBean : nullptr;
}

