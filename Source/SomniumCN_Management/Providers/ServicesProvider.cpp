// Fill out your copyright notice in the Description page of Project Settings.

#include "ServicesProvider.h"
#include "EngineData/Singletons/BeansContextSingleton.h"
#include "Engine.h"

/** Returns asset management service */
UAssetManagementService * UServicesProvider::GetAssetManagementService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->AssetManagementServiceBean : nullptr;
}

/** Returns world management service*/
UWorldManagementService * UServicesProvider::GetWorldManagementService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->WorldManagementServiceBean : nullptr;
}

/** Returns game instance management service */
UGameInstanceManagementService* UServicesProvider::GetGameInstanceManagementService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->GameInstanceManagementServiceBean : nullptr;
}

/** Returns player controller management service */
UPlayerControllerManageService* UServicesProvider::GetPlayerControllerManageService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->PlayerControllerManageServiceBean : nullptr;
}

/** Returns save game service */
USaveGameService* UServicesProvider::GetSaveGameService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->SaveGameServiceBean : nullptr;
}

/** Returns game settings service */
UGameSettingsService* UServicesProvider::GetGameSettingsService() {
	UBeansContextSingleton* BeansContext = Cast<UBeansContextSingleton>(GEngine ? GEngine->GameSingleton : nullptr);
	return BeansContext ? BeansContext->GameSettingsServiceBean : nullptr;
}

