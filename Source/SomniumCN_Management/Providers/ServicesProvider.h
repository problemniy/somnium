// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Providers/AbstractProvider.h"
#include "Services/AssetManagementService.h"
#include "Services/WorldManagementService.h"
#include "Services/GameInstanceManagementService.h"
#include "Services/PlayerControllerManageService.h"
#include "Services/SaveGameService.h"
#include "Services/GameSettingsService.h"
#include "ServicesProvider.generated.h"

/**
* @descr Class provider for Services classes
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UServicesProvider : public UAbstractProvider {

	GENERATED_BODY()
	
public:
	/**
	* @descr Returns asset management service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static UAssetManagementService* GetAssetManagementService();

	/**
	* @descr Returns world management service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static UWorldManagementService* GetWorldManagementService();

	/**
	* @descr Returns game instance management service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static UGameInstanceManagementService* GetGameInstanceManagementService();

	/**
	* @descr Returns player controller management service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static UPlayerControllerManageService* GetPlayerControllerManageService();

	/**
	* @descr Returns save game service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static USaveGameService* GetSaveGameService();

	/**
	* @descr Returns game settings service
	* @return Pointer to service object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Services")
		static UGameSettingsService* GetGameSettingsService();

};

