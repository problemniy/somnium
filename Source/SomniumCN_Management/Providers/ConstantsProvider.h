// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Providers/AbstractProvider.h"
#include "Constants/ResourceDirectories.h"
#include "Constants/ResourceNames.h"
#include "Constants/InputEventNames.h"
#include "Constants/GameSaveConfigValues.h"
#include "Constants/GameDefaultConfiguration.h"
#include "Constants/GameLoggingAccessibility.h"
#include "ConstantsProvider.generated.h"

/**
* @descr Class provider for Constants classes
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UConstantsProvider : public UAbstractProvider {

	GENERATED_BODY()
	
public:
	/**
	* @descr Returns bean containing resource names constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UResourceNames* GetResourceNames();

	/**
	* @descr Returns bean containing resource directories constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UResourceDirectories* GetResourceDirectories();

	/**
	* @descr Returns bean containing input event names constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UInputEventNames* GetInputEventNames();

	/**
	* @descr Returns bean containing game save configuration values constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UGameSaveConfigValues* GetGameSaveConfigValues();

	/**
	* @descr Returns bean containing game default configuration constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UGameDefaultConfiguration* GetGameDefaultConfiguration();

	/**
	* @descr Returns bean containing game logging accessibility constants
	* @return Pointer to bean object (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "Constants")
		static UGameLoggingAccessibility* GetGameLoggingAccessibility();
	
};
