// Fill out your copyright notice in the Description page of Project Settings.

#include "AssetManagementUtils.h"

/** Filters primary asset ids by primary asset names */
void UAssetManagementUtils::FilterAssetIdsByNames(const TArray<FName> PrimaryAssetNames, TArray<FPrimaryAssetId>& PrimaryAssetIdsToFilter, bool bCleanIfEmpty /*= false*/) {
	TArray<FPrimaryAssetId> FilteredNames;
	FilteredNames = PrimaryAssetIdsToFilter.FilterByPredicate([PrimaryAssetNames](const FPrimaryAssetId& PrimaryAssetId) {
		return PrimaryAssetNames.Contains(PrimaryAssetId.PrimaryAssetName);
	});
	if (bCleanIfEmpty || FilteredNames.Num() > 0) {
		PrimaryAssetIdsToFilter = FilteredNames;
	}
}

/** Change Localization at runtime */
void UAssetManagementUtils::ChangeGameLocale(const FString LocaleCultureName) {
	FInternationalization::Get().SetCurrentCulture(LocaleCultureName);
}

/** Checks locale culture name for correctness */
bool UAssetManagementUtils::IsLocaleCultureNameCorrect(const FString LocaleCultureName) {
	TArray<FString> AvailableCultureNames;
	FInternationalization::Get().GetCultureNames(AvailableCultureNames);
	return AvailableCultureNames.Contains(LocaleCultureName);
}
