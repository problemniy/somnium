// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Utils/AbstractUtils.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "SaveGameUtils.generated.h"

/**
* @descr Class of Save Game Utils
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API USaveGameUtils : public UAbstractUtils {

	GENERATED_BODY()

public:
	/**
	* @descr Search settings data field for specified ui element tag
	* @param SettingsData - Settings data for scan
	* @param UITag - UI element tag for search
	* @param DefaultUITag - Default ui tag for case when nothing is found
	* @return Found field (will be with default ui tag if nothing is found)
	*/
	UFUNCTION(BlueprintCallable, Category = "Save Game Utils")
		static EUIElementTag& GetSettingsDataField(UGameSettingsData* SettingsData, EUIElementTag UITagForSearch, EUIElementTag& DefaultUITag);
	
};
