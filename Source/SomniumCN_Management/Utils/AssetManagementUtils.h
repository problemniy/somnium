// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Utils/AbstractUtils.h"
#include "AssetManagementUtils.generated.h"

/**
* @descr Class of Asset Management Utils
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UAssetManagementUtils : public UAbstractUtils {

	GENERATED_BODY()
	
public:	
	/**
	* @descr Filters primary asset ids by primary asset names
	* @param PrimaryAssetNames - Names of primary assets
	* @param PrimaryAssetIdsToFilter - Primary asset ids to filter
	* @param bCleanIfEmpty - If true then array of asset ids will be cleared when there is no match
	*/
	UFUNCTION(BlueprintCallable, Category = "Asset Management Utils")
		static void FilterAssetIdsByNames(const TArray<FName> PrimaryAssetNames, TArray<FPrimaryAssetId>& PrimaryAssetIdsToFilter, bool bCleanIfEmpty = false);

	/**
	* @descr Change Localization at runtime
	* @param LocaleCultureName - Culture name of locale
	*/
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Change Localization"), Category = "Locale")
		static void ChangeGameLocale(const FString LocaleCultureName);

	/**
	* @descr Checks locale culture name for correctness
	* @param LocaleCultureName - Culture name of locale
	*/
	UFUNCTION(BlueprintPure, Category = "Locale")
		static bool IsLocaleCultureNameCorrect(const FString LocaleCultureName);

};
