// Fill out your copyright notice in the Description page of Project Settings.

#include "SaveGameUtils.h"

/** Search settings data field for specified ui element tag */
EUIElementTag& USaveGameUtils::GetSettingsDataField(UGameSettingsData* SettingsData, EUIElementTag UITagForSearch, EUIElementTag& DefaultUITag) {
	EUIElementTag* Result = nullptr;
	if (UITagForSearch == EUIElementTag::CONTROLLER_YES || UITagForSearch == EUIElementTag::CONTROLLER_NO) {
		Result = &SettingsData->ControllerEnabled;
	} 
	else if (UITagForSearch == EUIElementTag::VSYNC_YES || UITagForSearch == EUIElementTag::VSYNC_NO) {
		Result = &SettingsData->VerticalSync;
	}
	else if (UITagForSearch == EUIElementTag::FSCREEN_YES || UITagForSearch == EUIElementTag::FSCREEN_NO) {
		Result = &SettingsData->FullScreen;
	}
	else if (UITagForSearch == EUIElementTag::RES_1680_1050 || UITagForSearch == EUIElementTag::RES_1920_1080) {
		Result = &SettingsData->Resolution;
	}
	else if (UITagForSearch == EUIElementTag::QUALITY_LOW || UITagForSearch == EUIElementTag::QUALITY_HIGH) {
		Result = &SettingsData->Quality;
	}
	else if (UITagForSearch == EUIElementTag::LANGUAGE_ENG || UITagForSearch == EUIElementTag::LANGUAGE_RUS) {
		Result = &SettingsData->Language;
	}
	else {
		Result = &DefaultUITag;
	}
	return *Result;
}