// Fill out your copyright notice in the Description page of Project Settings.

#include "GameCommonUtils.h"

/** Converts enum value to string name */
FString UGameCommonUtils::EnumAsString(const TCHAR* EnumTypeName, uint8 EnumValue) {
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, EnumTypeName, true);
	return EnumPtr ? EnumPtr->GetNameStringByIndex(EnumValue) : FString("Enum wasn't found");
}

/** Converts array containing object paths to string representation */
FString UGameCommonUtils::ArrayAsString(TArray<FSoftObjectPath> Array) {
	FString Result("[");
	FString LastAssetName = Array.Num() > 0 ? Array.Last().GetAssetName() : FString();
	for (FSoftObjectPath ObjectPath : Array) {
		Result.Append(ObjectPath.GetAssetName());
		if (ObjectPath.GetAssetName() != LastAssetName)
			Result.Append(", ");
	}
	Result.Append("]");
	return Result;
}

/** Converts array containing primary assets ids to string representation */
FString UGameCommonUtils::ArrayAsString(TArray<FPrimaryAssetId> Array) {
	FString Result("[");
	FString LastPrimaryId = Array.Num() > 0 ? Array.Last().ToString() : FString();
	for (FPrimaryAssetId PrimaryId : Array) {
		Result.Append(PrimaryId.ToString());
		if (PrimaryId.ToString() != LastPrimaryId)
			Result.Append(", ");
	}
	Result.Append("]");
	return Result;
}

/** Converts array containing 'Name' type to string representation */
FString UGameCommonUtils::ArrayAsString(TArray<FName> Array) {
	FString Result("[");
	FString LastName = Array.Num() > 0 ? Array.Last().ToString() : FString();
	for (FName Name : Array) {
		Result.Append(Name.ToString());
		if (Name.ToString() != LastName)
			Result.Append(", ");
	}
	Result.Append("]");
	return Result;
}

