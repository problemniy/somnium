// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Utils/AbstractUtils.h"
#include "GameCommonUtils.generated.h"

/**
* @descr Converts array containing 'FName' type to string representation
*/
#define ENUM_AS_TEXT(enumtypename, enumvalue) *UGameCommonUtils::EnumAsString(enumtypename, enumvalue)

/**
* @descr Class of Game Common Utils
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UGameCommonUtils : public UAbstractUtils {

	GENERATED_BODY()
	
public:
	/**
	* @descr Assigns pointer to custom sub-class or created parent class as default if sub-class is null
	* @param Subclass - Subclass to check
	* @param Ptr - Pointer for assigning
	*/
	template <class TClassA>
	static void AssignPointerOrCreateDefault(const TSubclassOf<TClassA> &Subclass, TClassA** Ptr) {
		*Ptr = Subclass ? NewObject<TClassA>(Subclass.GetDefaultObject(), Subclass.Get()) : NewObject<TClassA>(TClassA::StaticClass());
	};

	/**
	* @descr Assigns pointer to custom sub-class
	* @param Subclass - Subclass to assign
	* @param Ptr - Pointer for assigning
	*/
	template <class TClassA>
	static void AssignPointer(const TSubclassOf<TClassA> &Subclass, TClassA** Ptr) {
		if (Subclass) {
			*Ptr = NewObject<TClassA>(Subclass.GetDefaultObject(), Subclass.Get());
		}
	};

	/**
	* @descr Add not null object to array
	* @param Subclass - Array for add
	* @param Object - Object for add
	*/
	template <class TClassA>
	static void AddNotNull(TArray<TClassA*> &Array, TClassA* Object) {
		if (Object) {
			Array.Add(Object);
		}
	};

	/**
	* @descr Converts enum value to string name
	* @param EnumTypeName - Enum type name
	* @param EnumValue - Enum value to check
	* @return Enum value as string
	*/
	static FString EnumAsString(const TCHAR* EnumTypeName, uint8 EnumValue);

	/**
	* @descr Converts array containing asset pointers to string representation
	* @param Array - Array with asset pointers
	* @return Representative string
	*/
	template <class TClassA>
	static FString ArrayAsString(TArray<TAssetPtr<TClassA>> Array) {
		FString Result("[");
		FString LastAssetName = Array.Num() > 0 ? Array.Last().GetAssetName() : FString();
		for (TAssetPtr<TClassA> Asset : Array) {
			Result.Append(Asset.GetAssetName());
			if (Asset.GetAssetName() != LastAssetName)
				Result.Append(", ");
		}
		Result.Append("]");
		return Result;
	}

	/**
	* @descr Converts array containing object paths to string representation
	* @param Array - Array with object paths
	* @return Representative string
	*/
	static FString ArrayAsString(TArray<FSoftObjectPath> Array);

	/**
	* @descr Converts array containing primary assets ids to string representation
	* @param Array - Array with primary assets ids
	* @return Representative string
	*/
	static FString ArrayAsString(TArray<FPrimaryAssetId> Array);

	/**
	* @descr Converts array containing 'Name' type to string representation
	* @param Array - Array with FName
	* @return Representative string
	*/
	static FString ArrayAsString(TArray<FName> Array);
	
};

