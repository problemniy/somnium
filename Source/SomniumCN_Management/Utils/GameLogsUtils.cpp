// Fill out your copyright notice in the Description page of Project Settings.


#include "GameLogsUtils.h"
#include "Providers/ConstantsProvider.h"

/** Checks logs for enabled according to game configuration */
bool UGameLogsUtils::AreLogsEnabled(ELogsGroup LogsGroup, ELogsLevel LogsLevel) {
	UGameLoggingAccessibility* $LogConfig = UConstantsProvider::GetGameLoggingAccessibility();
	bool bIsEnabled = false;
	if ($LogConfig) {
		switch (LogsGroup) {
		case ELogsGroup::LOADING_ASSETS:
			if (ELogsLevel::ERROR == LogsLevel)
				bIsEnabled = $LogConfig->bLoadingAssetsErrorLogsEnabled;
			else if (ELogsLevel::WARNING == LogsLevel)
				bIsEnabled = $LogConfig->bLoadingAssetsWarningLogsEnabled;
			else if (ELogsLevel::INFO == LogsLevel)
				bIsEnabled = $LogConfig->bLoadingAssetsInfoLogsEnabled;
			else if (ELogsLevel::DEBUG == LogsLevel)
				bIsEnabled = $LogConfig->bLoadingAssetsDebugLogsEnabled;
			else if (ELogsLevel::FULL == LogsLevel)
				bIsEnabled = $LogConfig->bLoadingAssetsFullLogsEnabled;
			break;
		}
	}
	return bIsEnabled;
}

