// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Utils/AbstractUtils.h"
#include "Misc/Enums/GameLoggingEnums.h"
#include "GameLogsUtils.generated.h"

/**
* @descr Class of Game Logs Utils
* @author problemniy
*/
UCLASS(CustomConstructor)
class SOMNIUMCN_MANAGEMENT_API UGameLogsUtils : public UAbstractUtils {

	GENERATED_BODY()

public:
	/**
	* @descr Checks logs for enabled according to game configuration
	* @param LogsGroup - Determines logs group that should be checked
	* @param LogsLevel - Determines logs level that should be checked
	* @return true if logs are available otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Logs Utils")
		static bool AreLogsEnabled(ELogsGroup LogsGroup, ELogsLevel LogsLevel);
	
};

