// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "WorldManagementService.generated.h"

/**
* @descr Bean class of World Management Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API UWorldManagementService : public UAbstractServiceBean {

	GENERATED_BODY()

public:
	/**
	* @descr Returns actor's active camera component
	* @return Reference to actor's active camera component (can be nullptr)
	*/
	UFUNCTION(BlueprintPure, Category = "World Management Service")
		UCameraComponent* GetActiveCameraComponent(AActor* Actor);
	
};

