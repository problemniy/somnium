// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerControllerManageService.h"
#include "Providers/ConstantsProvider.h"

/** Returns name of input action event */
FName UPlayerControllerManageService::GetInputActionEventName(const EInputActionEvent& InputActionEvent) {
	UInputEventNames* InputEventNames = UConstantsProvider::GetInputEventNames();
	FName Result;
	if (InputEventNames) {
		switch (InputActionEvent) {
		case EInputActionEvent::SKIP_INTRO:
				Result = InputEventNames->Action_SkipIntro;
				break;
		case EInputActionEvent::ANY_PRESSED:
				Result = InputEventNames->Action_AnyPressed;
				break;
		}
	}
	return Result;
}


