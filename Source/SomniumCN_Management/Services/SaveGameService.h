// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Data/SaveGame/GameSlotData.h"
#include "Data/SaveGame/GameSettingsData.h"
#include "Misc/Enums/GameSaveEnums.h"
#include "SaveGameService.generated.h"

/**
* @descr Bean class of Save Game Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API USaveGameService : public UAbstractServiceBean {

	GENERATED_BODY()

public:
	/**
	* @descr Returns available game slot data or the new if it's not yet exist
	* @param GameSlotIdentity - Identity of game save slot
	* @param bAllowToCreateNew - If true then creates a new game data if it's not existed
	* @return Reference to game slot data
	*/
	UFUNCTION(BlueprintPure, Category = "Save Game Service", meta = (AdvancedDisplay = "1"))
		UGameSlotData* ReceiveGameSlotData(const EGameSlotIdentity GameSlotIdentity, bool bAllowToCreateNew = true);

	/**
	* @descr Returns saved game data
	* @param DataClass - Expected class of save game data
	* @param GameSaveDataIdentity - Game save data identity
	* @param bAllowToCreateNew - If true then creates a new game data if it's not existed
	* @return Reference to saved game data
	*/
	UFUNCTION(BlueprintPure, Category = "Save Game Service", meta = (AdvancedDisplay = "2"))
		UAbstractSaveGameData* ReceiveSavedGameData(TSubclassOf<UAbstractSaveGameData> DataClass, const EGameSaveDataIdentity GameSaveDataIdentity, bool bAllowToCreateNew = true);

	/**
	* @descr Saves game slot data on hardware
	* @param GameData - Game data to save
	* @param GameSlotIdentity - Game data identity
	* @return true if saving was successful otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Save Game Service")
		bool SaveGameSlotData(UGameSlotData* GameSlotData);

	/**
	* @descr Saves game data on hardware
	* @param GameData - Game data to save
	* @param GameSlotIdentity - Game data identity
	* @return true if saving was successful otherwise false
	*/
	UFUNCTION(BlueprintCallable, Category = "Save Game Service")
		bool SaveGameData(UAbstractSaveGameData* GameData);

	/**
	* @descr Returns game slot name by it's identity
	* @param GameSlotIdentity - Identity of game save slot
	* @return Name of game save slot
	*/
	UFUNCTION(BlueprintPure, Category = "Save Game Service")
		FString GetSaveSlotName(const EGameSlotIdentity GameSlotIdentity);

	/**
	* @descr Returns save game data name by it's identity
	* @param GameSlotIdentity - Identity of game save slot
	* @return Name of game save slot
	*/
	UFUNCTION(BlueprintPure, Category = "Save Game Service")
		FString GetSaveDataName(const EGameSaveDataIdentity GameSlotIdentity);
	
};
