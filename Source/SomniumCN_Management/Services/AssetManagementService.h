// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Misc/Enums/GameWorldEnums.h"
#include "Engine/ObjectLibrary.h"
#include "AssetManagementService.generated.h"

/**
* @descr Bean class of Asset Management Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API UAssetManagementService : public UAbstractServiceBean {

	GENERATED_BODY()

private:
	// Returns object library by type if it's existed
	UObjectLibrary* GetAvailableObjectLibraryByType(const EAssetType AssetType);

public:
	/**
	* @descr Looking for asset data in object library by asset type and resource name
	* @param AssetType - Type of assets where should search
	* @param AssetName - Name of asset to search
	* @return Asset data struct
	*/
	FAssetData GetAssetData(const EAssetType AssetType, const FName AssetName);
	
public:
	/**
	* @descr Looking for package names in levels object library by level names
	* @param LevelNames - Names of levels
	* @param ResultPackages - Array which will contain found packages
	*/
	UFUNCTION(BlueprintCallable, Category = "Asset Management Service")
		void GetLevelPackages(const TArray<FName> LevelNames, TArray<FName>& ResultPackages);

	/**
	* @descr Looking for primary asset id's by primary asset type names
	* @param PrimaryAssetNames - Names of primary assets
	* @param PrimaryAssetTypeNames - Names of primary asset types
	* @param ResultPrimaryAssetIds - Array which will contain found parimary asset ids
	*/
	UFUNCTION(BlueprintCallable, Category = "Asset Management Service")
		void GetPrimaryAssetIdsByNames(const TArray<FName> PrimaryAssetNames, const TArray<FName> PrimaryAssetTypeNames, TArray<FPrimaryAssetId>& ResultPrimaryAssetIds);
	
	/**
	* @descr Looking for package name in object library by asset type and resource name
	* @param AssetType - Type of assets where should search
	* @param AssetName - Name of asset to search
	* @return Package name
	*/
	UFUNCTION(BlueprintCallable, Category = "Asset Management Service")
		FName GetAssetPackage(const EAssetType AssetType, const FName AssetName);

	/**
	* @descr Returns asset (loads it if needed) from object library by asset type and resource name
	* @param AssetType - Type of assets where should search
	* @param AssetName - Name of asset to search
	* @return Reference to loaded asset (will be nullptr if asset is not found)
	*/
	UFUNCTION(BlueprintCallable, Category = "Asset Management Service")
		UObject* GetAssetByName(const EAssetType AssetType, const FName AssetName);

};
