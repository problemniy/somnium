// Fill out your copyright notice in the Description page of Project Settings.

#include "AssetManagementService.h"
#include "EngineData/AssetManagers/GameAssetManager.h"
#include "Utils/AssetManagementUtils.h"
#include "Engine.h"

// Returns object library by type if it's existed
UObjectLibrary* UAssetManagementService::GetAvailableObjectLibraryByType(const EAssetType AssetType) {
	UObjectLibrary* Result = nullptr;
	if (GEngine) {
		UGameAssetManager* AssetManager = Cast<UGameAssetManager>(GEngine->AssetManager);
		if (AssetManager) {
			switch (AssetType) {
			case EAssetType::MAPS:
				Result = AssetManager->GetObjectLibraries()->Maps;
				break;
			case EAssetType::FONTS:
				Result = AssetManager->GetObjectLibraries()->Fonts;
				break;
			case EAssetType::WIDGETS:
				Result = AssetManager->GetObjectLibraries()->Widgets;
				break;
			case EAssetType::DTABLES:
				Result = AssetManager->GetObjectLibraries()->DataTables;
				break;
			}
		}
	}
	return Result;
}

/** Looking for asset data in object library by asset type and resource name */
FAssetData UAssetManagementService::GetAssetData(const EAssetType AssetType, const FName AssetName) {
	FAssetData AssetData;
	UObjectLibrary* ObjectLibrary = GetAvailableObjectLibraryByType(AssetType);
	if (ObjectLibrary) {
		TArray<FAssetData> DataList;
		ObjectLibrary->GetAssetDataList(DataList);
		for (FAssetData Data : DataList) {
			if (AssetName == Data.AssetName) {
				AssetData = Data;
				break;
			}
		}
	}
	return AssetData;
}

/** Looking for package names in levels object library by level names */
void UAssetManagementService::GetLevelPackages(const TArray<FName> LevelNames, TArray<FName>& ResultPackages) {
	UObjectLibrary* MapsLibrary = GetAvailableObjectLibraryByType(EAssetType::MAPS);
	if (MapsLibrary) {
		TArray<FAssetData> DataList;
		MapsLibrary->GetAssetDataList(DataList);
		for (FAssetData Data : DataList) {
			if (LevelNames.Contains(Data.AssetName)) {
				ResultPackages.Add(Data.PackageName);
			}
		}
	}
}

/** Looking for primary asset id's by primary asset type names */
void UAssetManagementService::GetPrimaryAssetIdsByNames(const TArray<FName> PrimaryAssetNames, const TArray<FName> PrimaryAssetTypeNames, TArray<FPrimaryAssetId>& ResultPrimaryAssetIds) {
	if (GEngine) {
		for (FName PrimaryAssetTypeName : PrimaryAssetTypeNames) {
			FPrimaryAssetType Type(PrimaryAssetTypeName);
			GEngine->AssetManager->GetPrimaryAssetIdList(Type, ResultPrimaryAssetIds);
		}
		UAssetManagementUtils::FilterAssetIdsByNames(PrimaryAssetNames, ResultPrimaryAssetIds);
	}
}

/** Looking for package name in object library by asset type and resource name */
FName UAssetManagementService::GetAssetPackage(const EAssetType AssetType, const FName AssetName) {
	FName PackageName;
	UObjectLibrary* ObjectLibrary = GetAvailableObjectLibraryByType(AssetType);
	if (ObjectLibrary) {
		TArray<FAssetData> DataList;
		ObjectLibrary->GetAssetDataList(DataList);
		for (FAssetData Data : DataList) {
			if (AssetName == Data.AssetName) {
				PackageName = Data.PackageName;
				break;
			}
		}
	}
	return PackageName;
}

/** Loads asset (if needed) from object library by asset type and resource name */
UObject* UAssetManagementService::GetAssetByName(const EAssetType AssetType, const FName AssetName) {
	UObject* LoadedAsset = nullptr;
	UObjectLibrary* ObjectLibrary = GetAvailableObjectLibraryByType(AssetType);
	if (ObjectLibrary) {
		TArray<FAssetData> DataList;
		ObjectLibrary->GetAssetDataList(DataList);
		for (FAssetData Data : DataList) {
			if (AssetName == Data.AssetName) {
				LoadedAsset = Data.GetAsset();
				break;
			}
		}
	}
	return LoadedAsset;
}


