// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Components/InputComponent.h"
#include "GameInstanceManagementService.generated.h"

/**
* @descr Bean class of Game Instance Management Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API UGameInstanceManagementService : public UAbstractServiceBean {

	GENERATED_BODY()
	
public:
	/**
	* @descr Registers action event in game instance data
	* @param EventName - Name of input action event
	* @param InputComponent - Input component which has binded input action event
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Instance Management Service")
		void RegActionEventOwner(const FName EventName, UInputComponent* InputComponent);

	/**
	* @descr Unregisters action event in game instance data
	* @param EventName - Name of input action event
	* @param ContextObject - Object which will be used for searching game instance
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Instance Management Service")
		void UnregActionEventOwner(const FName EventName, UObject* ContextObject);

	/**
	* @descr Returns action event position from bind actions collection by Input Component
	* @param EventName - Name of input action event
	* @param InputComponent - Input component which binded input action event
	* @return Position number (value -1 means not found)
	*/
	UFUNCTION(BlueprintPure, Category = "Game Instance Management Service")
		int32 GetActionEventPosition(const FName EventName, UInputComponent* InputComponent);

	/**
	* @descr Checks action event for registered in game instance data
	* @param EventName - Name of input action event
	* @param ContextObject - Object which will be used for searching game instance
	*/
	UFUNCTION(BlueprintPure, Category = "Game Instance Management Service")
		bool IsActionEventRegistered(const FName EventName, UObject* ContextObject);
	
	
};
