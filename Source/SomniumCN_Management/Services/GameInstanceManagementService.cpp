// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInstanceManagementService.h"
#include "Providers/GameInstanceDataProvider.h"
#include "Data/GameInstanceData/InputEventOwnersData.h"

/** Registers action event in game instance data */
void UGameInstanceManagementService::RegActionEventOwner(const FName EventName, UInputComponent* InputComponent) {
	UInputEventOwnersData* InputOwnersData = UGameInstanceDataProvider::GetInputEventOwnersData(InputComponent);
	if (InputOwnersData) {
		TPair<UInputComponent*, int32>* RegisteredComponent = InputOwnersData->ActionEventOwners.Find(EventName);
		if (!RegisteredComponent) {
			int32 Position = InputComponent->GetNumActionBindings();
			TPair<UInputComponent*, int32> ComponentToPosition(InputComponent, Position);
			InputOwnersData->ActionEventOwners.Add(EventName, ComponentToPosition);
		}
	}
}

/** Unregisters action event in game instance data */
void UGameInstanceManagementService::UnregActionEventOwner(const FName EventName, UObject* ContextObject) {
	UInputEventOwnersData* InputOwnersData = UGameInstanceDataProvider::GetInputEventOwnersData(ContextObject);
	if (InputOwnersData) {
		TPair<UInputComponent*, int32>* RegisteredComponent = InputOwnersData->ActionEventOwners.Find(EventName);
		if (RegisteredComponent) {
			bool bChecked = false;
			for (TPair<FName, TPair<UInputComponent*, int32>>& InputComponent : InputOwnersData->ActionEventOwners) {
				if (bChecked) {
					InputComponent.Value.Value -= 1;
				}
				else bChecked = InputComponent.Value.Key == RegisteredComponent->Key;
			}
			InputOwnersData->ActionEventOwners.Remove(EventName);
		}
	}
}

/** Returns action event position from bind actions collection by Input Component */
int32 UGameInstanceManagementService::GetActionEventPosition(const FName EventName, UInputComponent* InputComponent) {
	UInputEventOwnersData* InputOwnersData = UGameInstanceDataProvider::GetInputEventOwnersData(InputComponent);
	if (InputOwnersData) {
		TPair<UInputComponent*, int32>* RegisteredComponent = InputOwnersData->ActionEventOwners.Find(EventName);
		if (RegisteredComponent && InputComponent == RegisteredComponent->Key) {
			return RegisteredComponent->Value;
		}
	}
	return -1;
}

/** Checks action event for registered in game instance data */
bool UGameInstanceManagementService::IsActionEventRegistered(const FName EventName, UObject* ContextObject) {
	UInputEventOwnersData* InputOwnersData = UGameInstanceDataProvider::GetInputEventOwnersData(ContextObject);
	if (InputOwnersData) {
		return InputOwnersData->ActionEventOwners.Contains(EventName);
	}
	return false;
}


