// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSettingsService.h"
#include "Providers/ConstantsProvider.h"

FVector2D UGameSettingsService::GetMinMaxBrightnessRange() {
	UGameDefaultConfiguration* GameDefaultConfiguration = UConstantsProvider::GetGameDefaultConfiguration();
	return GameDefaultConfiguration ? GameDefaultConfiguration->BrightnessAvailableRange : FVector2D::ZeroVector;
}

FVector2D UGameSettingsService::GetAutoExposureMinRange() {
	UGameDefaultConfiguration* GameDefaultConfiguration = UConstantsProvider::GetGameDefaultConfiguration();
	return GameDefaultConfiguration ? GameDefaultConfiguration->AutoExposureMinRange : FVector2D::ZeroVector;
}

FVector2D UGameSettingsService::GetAutoExposureMaxRange() {
	UGameDefaultConfiguration* GameDefaultConfiguration = UConstantsProvider::GetGameDefaultConfiguration();
	return GameDefaultConfiguration ? GameDefaultConfiguration->AutoExposureMaxRange : FVector2D::ZeroVector;
}

/** Calculates camera brightness according to input value */
float UGameSettingsService::CalculateBrightness(const float PercentValue, const FVector2D MinMaxRange) {
	// because lighter brightness has lower value
	float RevertValue = 1 - PercentValue;
	// calculation
	float SliderPercent = RevertValue * 100;
	float Delta = ((MinMaxRange.Y - MinMaxRange.X) * SliderPercent) / 100;
	float ActualValue = MinMaxRange.X + Delta;
	return ActualValue;
}

/** Calculates brightness percent for slider (will be in range 0-1) */
float UGameSettingsService::CalculateBrightnessPercent(const float Brightness, const FVector2D MinMaxRange) {
	float CommonRange = MinMaxRange.Y - MinMaxRange.X;
	float InputBrightnessRange = Brightness - MinMaxRange.X;
	// calculation
	float DeltaPercent = (InputBrightnessRange * 100) / CommonRange;
	float Percent = (100 - DeltaPercent) / 100;
	return Percent;
}

/** Converts simple brightness value to eye adaptation brightness values */
FVector2D UGameSettingsService::ConvertToEyeAdaptationBrightness(const float Brightness) {
	FVector2D BrightnessMinMaxRange = GetMinMaxBrightnessRange();
	FVector2D OptimalMinAutoExposureRange = GetAutoExposureMinRange();
	FVector2D OptimalMaxAutoExposureRange = GetAutoExposureMaxRange();
	// calculation
	float RangeNumbers = BrightnessMinMaxRange.Y - BrightnessMinMaxRange.X;
	float StartNumber = Brightness - BrightnessMinMaxRange.X;
	float BrightnessPercent = (StartNumber * 100) / RangeNumbers;
	float MinExposureDelta = ((OptimalMinAutoExposureRange.Y - OptimalMinAutoExposureRange.X) * BrightnessPercent) / 100;
	float MaxExposureDelta = ((OptimalMaxAutoExposureRange.Y - OptimalMaxAutoExposureRange.X) * BrightnessPercent) / 100;
	float ResultX = OptimalMinAutoExposureRange.X + MinExposureDelta;
	float ResultY = OptimalMaxAutoExposureRange.X + MaxExposureDelta;
	return FVector2D(ResultX, ResultY);
}

/** Converts eye adaptation brightness values to simple brightness value */
float UGameSettingsService::ConvertToBrightness(const FVector2D MinMaxBrightness) {
	FVector2D OptimalMaxAutoExposureRange = GetAutoExposureMaxRange();
	FVector2D BrightnessMinMaxRange = GetMinMaxBrightnessRange();
	// calculation
	float RangeNumbers = OptimalMaxAutoExposureRange.Y - OptimalMaxAutoExposureRange.X;
	float StartNumber = MinMaxBrightness.Y - OptimalMaxAutoExposureRange.X;
	float BrightnessPercent = (StartNumber * 100) / RangeNumbers;
	float MaxExposureDelta = ((BrightnessMinMaxRange.Y - BrightnessMinMaxRange.X) * BrightnessPercent) / 100;
	float ResultBrightness = BrightnessMinMaxRange.X + MaxExposureDelta;
	return ResultBrightness;
}

/** Changes camera brightness */
void UGameSettingsService::ChangeCameraBrightness(UCameraComponent* CameraComponent, const FVector2D AutoExposureMinMaxBrightness) {
	FPostProcessSettings NewPostProcessSettings = CameraComponent->PostProcessSettings;
	// disable auto-exposure brightness for menu
	NewPostProcessSettings.AutoExposureMinBrightness = AutoExposureMinMaxBrightness.X;
	NewPostProcessSettings.AutoExposureMaxBrightness = AutoExposureMinMaxBrightness.Y;
	// allow to override post process settings
	NewPostProcessSettings.bOverride_AutoExposureMinBrightness = true;
	NewPostProcessSettings.bOverride_AutoExposureMaxBrightness = true;
	// override post process settings on active camera
	CameraComponent->PostProcessSettings = NewPostProcessSettings;
}

