// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Misc/Enums/GameControllerEnums.h"
#include "PlayerControllerManageService.generated.h"

/**
* @descr Bean class of Player Controller Management Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API UPlayerControllerManageService : public UAbstractServiceBean {

	GENERATED_BODY()
	
public:
	/**
	* @descr Returns name of input action event
	* @param InputActionEvent - Input action event specifier
	* @return Name of input action event
	*/
	UFUNCTION(BlueprintPure, Category = "Player Controller Management Service")
		FName GetInputActionEventName(const EInputActionEvent& InputActionEvent);
	
};
