// Fill out your copyright notice in the Description page of Project Settings.

#include "SaveGameService.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "Providers/ConstantsProvider.h"

/** Returns available game slot data or the new if it's not yet exist */
UGameSlotData* USaveGameService::ReceiveGameSlotData(const EGameSlotIdentity GameSlotIdentity, bool bAllowToCreateNew /*= true*/) {
	UGameSlotData* CreatedSlotData = Cast<UGameSlotData>(UGameplayStatics::CreateSaveGameObject(UGameSlotData::StaticClass()));
	// use default user index to search
	UGameSlotData* LoadedSlotData = Cast<UGameSlotData>(UGameplayStatics::LoadGameFromSlot(GetSaveSlotName(GameSlotIdentity), CreatedSlotData->UserIndex));
	// create a new game slot if it's needed`
	if (!LoadedSlotData && bAllowToCreateNew) {
		CreatedSlotData->SaveDataName = GetSaveSlotName(GameSlotIdentity);
		return CreatedSlotData;
	}
	return LoadedSlotData;
}

/** Saves game slot data on hardware */
bool USaveGameService::SaveGameSlotData(UGameSlotData* GameSlotData) {
	return UGameplayStatics::SaveGameToSlot(GameSlotData, GameSlotData->SaveDataName, GameSlotData->UserIndex);
}

/** Saves game data on hardware */
bool USaveGameService::SaveGameData(UAbstractSaveGameData* GameData) {
	return UGameplayStatics::SaveGameToSlot(GameData, GameData->SaveDataName, GameData->UserIndex);
}

/** Returns game slot name by it's identity */
FString USaveGameService::GetSaveSlotName(const EGameSlotIdentity GameSlotIdentity) {
	FString Result;
	switch(GameSlotIdentity) {
	case EGameSlotIdentity::SLOT_1:
		Result = UConstantsProvider::GetGameSaveConfigValues()->SaveSlotName_1;
		break;
	case EGameSlotIdentity::SLOT_2:
		Result = UConstantsProvider::GetGameSaveConfigValues()->SaveSlotName_2;
		break;
	case EGameSlotIdentity::SLOT_3:
		Result = UConstantsProvider::GetGameSaveConfigValues()->SaveSlotName_3;
		break;
	}
	return Result;
}

/** Returns save game data name by it's identity */
FString USaveGameService::GetSaveDataName(const EGameSaveDataIdentity GameSlotIdentity) {
	FString Result;
	switch (GameSlotIdentity) {
	case EGameSaveDataIdentity::GAME_SETTINGS:
		Result = UConstantsProvider::GetGameSaveConfigValues()->GameSettingsDataName;
		break;
	}
	return Result;
}

/** Returns saved game data */
UAbstractSaveGameData* USaveGameService::ReceiveSavedGameData(TSubclassOf<UAbstractSaveGameData> DataClass, const EGameSaveDataIdentity GameSlotIdentity, bool bAllowToCreateNew /*= true*/) {
	UAbstractSaveGameData* CreatedData = Cast<UAbstractSaveGameData>(UGameplayStatics::CreateSaveGameObject(DataClass.Get()));
	// use default user index to search
	UAbstractSaveGameData* ExistedData = Cast<UAbstractSaveGameData>(UGameplayStatics::LoadGameFromSlot(GetSaveDataName(GameSlotIdentity), CreatedData->UserIndex));
	if (!ExistedData && bAllowToCreateNew) {
		CreatedData->SaveDataName = GetSaveDataName(GameSlotIdentity);
		return CreatedData;
	}
	return ExistedData;
}


