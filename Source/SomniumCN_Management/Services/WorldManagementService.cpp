// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldManagementService.h"
#include "Engine/Classes/GameFramework/Actor.h"

/** Returns actor's active camera component */
UCameraComponent* UWorldManagementService::GetActiveCameraComponent(AActor* Actor) {
	UCameraComponent* ActiveCamera = nullptr;
	TInlineComponentArray<UCameraComponent*> Cameras;
	Actor->GetComponents<UCameraComponent>(Cameras);
	for (UCameraComponent* CameraComponent : Cameras) {
		if (CameraComponent->bIsActive) {
			ActiveCamera = CameraComponent;
			break;
		}
	}
	return ActiveCamera;
}


