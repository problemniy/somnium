// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abstract/Beans/AbstractServiceBean.h"
#include "Camera/CameraComponent.h"
#include "GameSettingsService.generated.h"

/**
* @descr Bean class of Game Settings Service
* @author problemniy
*/
UCLASS()
class SOMNIUMCN_MANAGEMENT_API UGameSettingsService : public UAbstractServiceBean {

	GENERATED_BODY()

private:
	FVector2D GetMinMaxBrightnessRange();
	FVector2D GetAutoExposureMinRange();
	FVector2D GetAutoExposureMaxRange();

public:
	/**
	* @descr Calculates camera brightness according to input value
	* @param PercentValue - Value to parse (the slider value is assumed, should be in range 0-1)
	* @param MinMaxRange - Available min/max range to calculate
	* @return Brightness value
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Settings Service")
		float CalculateBrightness(const float PercentValue, const FVector2D MinMaxRange);

	/**
	* @descr Calculates brightness percent for slider (will be in range 0-1)
	* @param Brightness - Brightness value to parse
	* @param MinMaxRange - Available min/max range to calculate ('X' should be <= input value to parse)
	* @return Brightness percent
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Settings Service")
		float CalculateBrightnessPercent(const float Brightness, const FVector2D MinMaxRange);

	/**
	* @descr Converts simple brightness value to eye adaptation brightness values
	* @param Brightness - Brighntess value to parse
	* @return Vector2D which determines 'Auto-exposure min' as X and 'Auto-exposure max' as Y
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Settings Service")
		FVector2D ConvertToEyeAdaptationBrightness(const float Brightness);

	/**
	* @descr Converts eye adaptation brightness values to simple brightness value
	* @param MinMaxBrightness - Auto exposure min/max values to parse
	* @return Brightness value
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Settings Service")
		float ConvertToBrightness(const FVector2D MinMaxBrightness);

	/**
	* @descr Changes camera brightness
	* @param CameraComponent - Camera component to get camera
	* @param AutoExposureMinMaxBrightness - Auto exposure min/max brightness values
	*/
	UFUNCTION(BlueprintCallable, Category = "Game Settings Service")
		void ChangeCameraBrightness(UCameraComponent* CameraComponent, const FVector2D AutoExposureMinMaxBrightness);
	
};

